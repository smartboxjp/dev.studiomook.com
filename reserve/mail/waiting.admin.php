
キャンセル待ちを受け付けました。

─────↓────────────────────────────────────────

---------------------------
今回キャンセル待ちする方の情報
---------------------------
【予約ステータス】 [view_status]
【撮影日】 [view_reserve_day]
【撮影時間】 [cate_course_time_from] 〜 [cate_course_time_to]
【代表者】[member_name]
【撮影メニュー】[cate_menu_name]
【撮影コース】 [cate_course_name]

----------------------------
来店情報
----------------------------
【当スタジオのご利用回数】 [visited_count]
【来店の人数】 [view_visit]

【受付日時】[datetime]

─────↑────────────────────────────────────────