<?php
    require_once $_SERVER['DOCUMENT_ROOT'].$DOCUMENT_ROOT."/app_include/connect.php";
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連

    require_once $_SERVER['DOCUMENT_ROOT'].$DOCUMENT_ROOT."/app_include/CommonEmail.php";
    $common_email = new CommonEmail(); //メール関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>
<?php
    //ログインチェック
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION["member_id"];

    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    
    if($reserve_id== "")
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }
    $datetime = date("Y/m/d H:i:s");


    //仮予約情報
    $arr_db_field = array("reserve_id", "member_id", "member_name_1", "member_name_2", "reserve_day", "cate_course_id", "cate_course_name", "cate_menu_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "visited_count", "visit_adult", "visit_child", "status");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM reserve where reserve_id='".$reserve_id."' and member_id='".$member_id."' " ;
    $sql .= " and flag_open=0" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }

        $yyyy = date("Y",strtotime($reserve_day));
        $mm = date("m",strtotime($reserve_day));
        $dd = date("d",strtotime($reserve_day));

        $view_reserve_day = $yyyy."年".$mm."月".$dd."日（".$common_connect->Fn_date_day($yyyy.$mm.$dd)."）";
    }
    else
    {
        $common_connect -> Fn_javascript_back("登録されているデータがありません。");
    }
    
    //更新
    $db_up = "update reserve set flag_open=1, up_date='".$datetime."' ";
    $db_up .= " where reserve_id='".$reserve_id."' and member_id='".$member_id."'" ;
    $common_dao->db_update($db_up);

    //会員情報
    $arr_db_field = array("member_name_1", "member_name_2", "tel", "member_email");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM member where member_id='".$member_id."' and flag_open=1" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }

    if($visited_count==0) { $visited_count = "はじめて";}
    $view_visit = "大人：".$visit_adult."人　";
    if($visit_child!="")
    {
        $view_visit .= "子供：".$visit_child."人　";
    }

    $temp_url = global_ssl;

    if($status==1)
    {
        //Thank youメール（ユーザー）
        if ($member_email != "")
        {
            $subject = "[Mr.MOOK]ご予約が確定しました";
            $body = "";
            $body = file_get_contents("./mail/reserve.user.php");
            $body = str_replace("[view_status]", $global_reserve_view[$status], $body);
            $body = str_replace("[cate_course_name]", $cate_course_name, $body);
            $body = str_replace("[view_reserve_day]", $view_reserve_day, $body);
            $body = str_replace("[cate_course_time_from]", substr($cate_course_time_from, 0, 5), $body);
            $body = str_replace("[cate_course_time_to]", substr($cate_course_time_to, 0, 5), $body);
            $body = str_replace("[member_name]", $member_name_1." ".$member_name_2, $body);
            $body = str_replace("[cate_menu_name]", $cate_menu_name, $body);
            $body = str_replace("[visited_count]", $visited_count, $body);
            $body = str_replace("[view_visit]", $view_visit, $body);
            $body = str_replace("[temp_url]", $temp_url, $body);


            $body = str_replace("[datetime]", $datetime, $body);
            $body = str_replace("[global_email_footer]", $global_email_footer, $body);
            $common_email-> Fn_send_utf($member_email."<".$member_email.">",$subject,$body,$global_mail_from,$global_send_mail);
        }
           
        //Thank youメール（管理者）
        if ($global_bcc_mail != "")
        {
            $subject = "[Mr.MOOK]ご予約が確定受付";
            $body = "";
            $body = file_get_contents("./mail/reserve.admin.php");
            $body = str_replace("[view_status]", $global_reserve_view[$status], $body);
            $body = str_replace("[cate_course_name]", $cate_course_name, $body);
            $body = str_replace("[view_reserve_day]", $view_reserve_day, $body);
            $body = str_replace("[cate_course_time_from]", substr($cate_course_time_from, 0, 5), $body);
            $body = str_replace("[cate_course_time_to]", substr($cate_course_time_to, 0, 5), $body);
            $body = str_replace("[member_name]", $member_name_1." ".$member_name_2, $body);
            $body = str_replace("[cate_menu_name]", $cate_menu_name, $body);
            $body = str_replace("[visited_count]", $visited_count, $body);
            $body = str_replace("[view_visit]", $view_visit, $body);
            $body = str_replace("[temp_url]", $temp_url, $body);


            $body = str_replace("[datetime]", $datetime, $body);
            $body = str_replace("[global_email_footer]", $global_email_footer, $body);
            $common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);
        }
    } elseif($status==2 || $status==3 || $status==4) {
        //Thank youメール（ユーザー）
        if ($member_email != "")
        {
            $subject = "[Mr.MOOK]キャンセル待ちをお受けいたしました";
            $body = "";
            $body = file_get_contents("./mail/waiting.user.php");
            $body = str_replace("[view_status]", $global_reserve_view[$status], $body);
            $body = str_replace("[cate_course_name]", $cate_course_name, $body);
            $body = str_replace("[view_reserve_day]", $view_reserve_day, $body);
            $body = str_replace("[cate_course_time_from]", substr($cate_course_time_from, 0, 5), $body);
            $body = str_replace("[cate_course_time_to]", substr($cate_course_time_to, 0, 5), $body);
            $body = str_replace("[member_name]", $member_name_1." ".$member_name_2, $body);
            $body = str_replace("[cate_menu_name]", $cate_menu_name, $body);
            $body = str_replace("[visited_count]", $visited_count, $body);
            $body = str_replace("[view_visit]", $view_visit, $body);
            $body = str_replace("[temp_url]", $temp_url, $body);


            $body = str_replace("[datetime]", $datetime, $body);
            $body = str_replace("[global_email_footer]", $global_email_footer, $body);
            $common_email-> Fn_send_utf($member_email."<".$member_email.">",$subject,$body,$global_mail_from,$global_send_mail);
        }
           
        //Thank youメール（管理者）
        if ($global_bcc_mail != "")
        {
            $subject = "[Mr.MOOK]キャンセル待ち受付";
            $body = "";
            $body = file_get_contents("./mail/waiting.admin.php");
            $body = str_replace("[view_status]", $global_reserve_view[$status], $body);
            $body = str_replace("[cate_course_name]", $cate_course_name, $body);
            $body = str_replace("[view_reserve_day]", $view_reserve_day, $body);
            $body = str_replace("[cate_course_time_from]", substr($cate_course_time_from, 0, 5), $body);
            $body = str_replace("[cate_course_time_to]", substr($cate_course_time_to, 0, 5), $body);
            $body = str_replace("[member_name]", $member_name_1." ".$member_name_2, $body);
            $body = str_replace("[cate_menu_name]", $cate_menu_name, $body);
            $body = str_replace("[visited_count]", $visited_count, $body);
            $body = str_replace("[view_visit]", $view_visit, $body);
            $body = str_replace("[temp_url]", $temp_url, $body);


            $body = str_replace("[datetime]", $datetime, $body);
            $body = str_replace("[global_email_footer]", $global_email_footer, $body);
            $common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);
        }
    }

    //$common_connect-> Fn_redirect("thanks.php");
?>
<script type="text/javascript">
<!--
$(function () {
    $('#form_confirm').click();
});
//-->
</script>
<form action="thanks.php" method="POST" name="form_write" style="display:none;">
  <?php $var = "reserve_id";?>
  <input type="hidden" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>">
  <?php $var = "form_confirm";?>
  <input type="submit" name="<?php echo $var;?>" id="<?php echo $var;?>" value="登録">
</form>
</body>
</html>