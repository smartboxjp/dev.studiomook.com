<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<?php
    //ログインチェック
    $common_connect -> Fn_member_check();
    $yyyymmdd = $common_connect->h($_GET["yyyymmdd"]);

    if(strlen($yyyymmdd)!=8)
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }

    $yyyy = date("Y",strtotime($yyyymmdd));
    $mm = date("m",strtotime($yyyymmdd));
    $dd = date("d",strtotime($yyyymmdd));
    //日付チェック
    if(!checkdate($mm, $dd, $yyyy))
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }

    //30分以内の仮予約削除(予約初期化)
    $del_time = date("Y-m-d H:i", strtotime('-31 minute'));
    $db_del = "Delete from reserve where flag_open=0 and regi_date<'".$del_time."' ";
    $common_dao->db_update($db_del);
    
    //コース名
    $arr_plan = array();
    $sql = "select p_".$dd." as plan, cate_course_name, cate_course_id ";
    $sql .= " from schedule s inner join ";
    $sql .= " cate_course c on s.p_".$dd."=c.cate_course_id  ";
    $sql .= " where flag_open=1 and yyyymm='".$yyyy.$mm."'  ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $cate_course_id = $db_result[0]["cate_course_id"];
        $cate_course_name = $db_result[0]["cate_course_name"];
    }

    //プランコース
    $arr_plan_course = array();
    $sql = "select p_".$dd." as plan, cate_course_time_id, cate_course_time_from, cate_course_time_to ";
    $sql .= " from schedule s inner join ";
    $sql .= " cate_course_time c on s.p_".$dd."=c.cate_course_id  ";
    $sql .= " where yyyymm='".$yyyy.$mm."'  ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_time[$db_result[$db_loop]["cate_course_time_id"]] = substr($db_result[$db_loop]["cate_course_time_from"], 0, 5)." 〜 ".substr($db_result[$db_loop]["cate_course_time_to"], 0, 5);
        }
    }

    //予約データ
    $arr_reserve = array();
    $sql = "select cate_course_time_id, MAX( STATUS ) AS max_status ";
    $sql .= " from reserve  ";
    $sql .= " where reserve_day='".$yyyy."-".$mm."-".$dd."'  ";
    $sql .= " and cate_course_id='".$cate_course_id."'  ";
    $sql .= " and status<'90' " ;
    $sql .= " group by cate_course_time_id ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_reserve[$db_result[$db_loop]["cate_course_time_id"]] = $db_result[$db_loop]["max_status"];
        }
    }

?>
<?php
    //予約可能かをチェック
    $add_day = 1;
    $today_ymd = date("Ymd"); //今日の日付
    $today_hi = date("Hi"); //今日の時間

    $today_ymdhhii = $today_ymd.$today_hi;
    $check_time = global_check_time; //更新基準時間
    $today_day = date("Y-m-d", strtotime($today_ymd));


    if(date("w", strtotime($today_ymd))==1 && date("Hi", strtotime($today_ymdhhii))<$check_time)
    {
        $loop_week = 3;
    }
    else
    {
        $loop_week = 4;
    }

    //月曜日の不具合？
    if(date("w", strtotime($today_ymd))==1)
    {
        $loop_week += 1;
    }

    //past
    $past_end = date("Ymd", strtotime($today_day." ".$add_day." day"));//予約不可日
    $notyet_start = date("Ymd", strtotime($today_day." Monday ".($loop_week)." week"));//予約不可日

    if(date("Ymd",strtotime($yyyymmdd))<=$past_end || date("Ymd",strtotime($yyyymmdd))>=$notyet_start)
    {
        $common_connect -> Fn_javascript_back("予約日を確認してください。");
    }
?>
<article>
<section>
<h1 id="pageTitle">ご予約 -希望時間選択-</h1>

<div class="stepArea">
<ul>
<li class="active">STEP1<span>日付選択</span></li>
<li class="active">STEP2<span>時間選択</span></li>
<li>STEP3<span>情報入力</span></li>
<li>STEP4<span>確認</span></li>
<li>STEP5<span>予約完了</span></li>
</ul>
</div>

<section class="timeArea">
<h2 class="tit"><span class="courseName"><? echo $cate_course_name;?>枠</span>を選択しました<span class="whiteBox"><? echo $yyyy;?>年<? echo $mm;?>月<? echo $dd;?>日（<? echo $common_connect->Fn_date_day($yyyy.$mm.$dd);?>）</span></h2>
<div class="timeAreaIn">
<p class="subTit"><span>ー STEP2 ー</span>次に、ご希望の時間枠を選択してください。</p>
<table>
<thead>
<tr>
<th>時間</th>
<th>予約</th>
</tr>
</thead>
<tbody>

<?php foreach($arr_cate_course_time as $key=>$value) { ?>
<tr>
<th><?php echo $value;?></th>
<td data-href="time_save.php?yyyymmdd=<?php echo $yyyymmdd;?>&cate_course_time_id=<?php echo $key;?>">
<?
    if($arr_reserve[$key]>=4){
        echo '<span class="icon cross"></span>';
    } elseif($arr_reserve[$key]==3){
        echo 'キャンセル待ち';
    } elseif($arr_reserve[$key]==2){
        echo 'キャンセル待ち';
    } elseif($arr_reserve[$key]==1){
        echo 'キャンセル待ち';
    } else{
        echo '<span class="icon circle"></span>';
    }

?>
<? /*
    if($arr_reserve[$key]>=4){
        echo $global_reserve_view[5];
    } elseif($arr_reserve[$key]==3){
        echo $global_reserve_view[4];
    } elseif($arr_reserve[$key]==2){
        echo $global_reserve_view[3];
    } elseif($arr_reserve[$key]==1){
        echo $global_reserve_view[2];
    } else{
        echo $global_reserve_view[1];
    }
*/
?>
</td>
</tr>
<?php } ?>

</tbody>
</table>
<p class="txt">○・・・予約可能<br>
× ・・・予約もキャンセル待ちも埋まりました<br>
キャンセル待ち・・・キャンセル待ちのみ可能</p>
</div>
</section>

<a href="calendar.php" class="backBtn">予約カレンダーに戻る</a>


</section>
</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
