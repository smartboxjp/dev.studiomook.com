<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<article>
<section id="reserveTop">
<h1 id="reserveTitle">ー 撮影のご予約 ー</h1>
<p>3月6日のご予約分より、web予約がスタートしました！<br>
ご予約は1ヶ月先までお取り頂けます。</p>
<div class="purpleBox">
毎週月曜日の昼12:30に<br>
新しいご予約枠が、1週間分ずつ公開されます。<br>
最新公開中の予約枠と空き状況は、<br>
予約カレンダーからご確認頂けます。
</div>
</section>
<section class="reserveArea">
<h2 class="reservationflowTit">ご予約の流れ</h2>
<div class="reserveBox">
<div>
<p class="num">1</p>
<p class="tit">会員登録をお願い致します。</p>
<p class="txt">ご予約には会員登録が必要です！<br>
新規会員登録より必要事項を入力して、ユーザー名とパスワードを取得してください。</p>
<a href="/registration/" class="registrationBtn">新規会員登録はこちら</a>
</div>
</div>
<div class="reserveBox">
<div>
<p class="num">2</p>
<p class="tit">カレンダーで空き状況をご確認ください。</p>
</div>
</div>
<div class="reserveBox">
<div>
<p class="num">3</p>
<p class="tit">ご希望の日付・時間をお選びください。 </p>
<p class="txt">ショート枠とスタンダード枠の違いにご注意ください！あらかじめご希望の<a href="/menu/" class="cBlue">撮影メニュー</a>ページにてご確認頂くとスムーズです。</p>
</div>
</div>
<div class="reserveBox">
<div>
<p class="num">4</p>
<p class="tit">撮影の内容をフォームに入力してください。 </p>
<p class="txt">メインで撮影するお子様が2人以上の場合は、1枠の中で、それぞれのお子様の撮影内容を入力してください。</p>
</div>
</div>
<div class="reserveBox">
<div>
<p class="num">5</p>
<p class="tit">「 予約確定 」ボタンを押して完了です！</p>
</div>
</div>
<div class="reserveBox none">
<div>
<p class="num">6</p>
<p class="tit">マイページで予約状況をご確認ください。</p>
</div>
</div>
<p class="subTxt">ご予約完了後は、必ず当サイトのマイページにて予約状況を確認してください！<br>
<span class="cRed">万が一、マイページに予約状況が反映されていない場合は、お手数ですがお電話にてご連絡くださいます様お願い致します。</span></p>
<a href="/reserve/calendar.php" class="submitBtn">予約カレンダーで空き状況を確認する</a>
</section>
</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
