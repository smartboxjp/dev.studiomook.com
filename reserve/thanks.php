<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<?
    //ログインチェック
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION["member_id"];

    //会員情報
    $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "tel", "member_email", "post_num", "address_1", "address_2", "address_3");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM member where member_id='".$member_id."' and flag_open=1" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }
?>
<article>
<section>
<h1 id="pageTitle">ご予約 ー 完了 ー</h1>

<div class="stepArea">
<ul>
<li class="active">STEP1<span>日付選択</span></li>
<li class="active">STEP2<span>時間選択</span></li>
<li class="active">STEP3<span>情報入力</span></li>
<li class="active">STEP4<span>確認</span></li>
<li class="active">STEP5<span>予約完了</span></li>
</ul>
</div>

<section class="descriptionArea thankyou">
<h2 class="tit">ご予約が完了しました！</h2>
<div class="formTxt">
<p class="mb10">この度は、ご予約頂き誠にありがとうございました。スタッフ一同、撮影当日を楽しみにお待ちしております！</p>
<p>ご予約頂いたメールアドレス宛に内容確認メールを自動送信致しました。</p>
<p class="subTxt">※携帯メールをご利用の場合、スタジオからのメールが届かない場合があります。お客様のメール設定でパソコンからのメールが受信拒否になっている可能性がありますのでご確認頂きますようお願い致します。</p>
</div>
</section>

<div class="descriptionArea">
<p class="bold tCenter">マイページからも予約内容をご確認頂けます。</p>
<a href="/mypage/" class="mypageBtn">マイページで確認する</a>
<p class="formTxt mt10"><span class="cRed">万が一、マイページに予約内容が反映されていない場合は、お手数ですがお電話にてご連絡くださいます様お願い致します。</span></p>
</div>

<section class="formEnterArea bbBlue">
<h3 class="tit"><span>ご登録中の代表者様情報の確認をお願い致します！</span></h3>
<table>
<tr>
<th>お名前</th>
<td><? echo $member_name_1;?> <? echo $member_name_2;?></td>
</tr>
<tr>
<th>ふりがな</th>
<td><? echo $member_name_kana;?></td>
</tr>
<tr>
<th>電話番号</th>
<td><? echo $tel;?></td>
</tr>
<tr>
<th>E-Mail</th>
<td><? echo $member_email;?></td>
</tr>
<tr>
<th>住所</th>
<td>〒<? echo $post_num;?><br>
<? echo $address_1;?><br>
<? echo $address_2;?><br>
<? echo $address_3;?></td>
</tr>
</table>
<p class="formTxt">※お客様情報（特にご連絡先）に変更はありませんでしょうか？変更がある場合は、マイページの「<a href="/mypage/information/" class="cBlue">登録情報の変更</a>」より更新をお願い致します。</p>
</section>


</section>
</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
