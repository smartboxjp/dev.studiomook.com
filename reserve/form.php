<?php
    require_once $_SERVER['DOCUMENT_ROOT'].$DOCUMENT_ROOT."/app_include/connect.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    $max_count = 5;
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<?php
    //ログインチェック
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION["member_id"];

    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    
    if($reserve_id == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }

    //仮予約情報
    $arr_db_field = array("reserve_id", "member_id", "member_name_1", "member_name_2", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "visited_count", "visit_adult", "reserve_count", "visit_child", "status");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM reserve where reserve_id='".$reserve_id."' and member_id='".$member_id."' and flag_open=0" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }
    if($member_name_1=="")
    {
        $common_connect -> Fn_javascript_back("予約情報が正しくありません。");
    }

    $yyyy = date("Y",strtotime($reserve_day));
    $mm = date("m",strtotime($reserve_day));
    $dd = date("d",strtotime($reserve_day));

    //メニュー
    $sql = " select cate_menu_id, cate_menu_name FROM cate_menu ";
    $sql .= " where flag_open=1 and cate_course_id='".$cate_course_id."' order by view_level" ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_menu_id[$db_result[$db_loop]["cate_menu_id"]] = $db_result[$db_loop]["cate_menu_name"];
        }
    }

    $arr_sex = array();
    $arr_sex[] = "男";
    $arr_sex[] = "女";

    $arr_birth_yyyy = array();
    for($loop_yyyy=(date("Y")+1);$loop_yyyy>1930;$loop_yyyy--) {
        $arr_birth_yyyy[] = $loop_yyyy;
    }

    $arr_birth_mm = array();
    for($loop_mm=1;$loop_mm<13;$loop_mm++) {
        $arr_birth_mm[] = $loop_mm;
    }

    $arr_birth_dd = array();
    for($loop_dd=1;$loop_dd<32;$loop_dd++) {
        $arr_birth_dd[] = $loop_dd;
    }
?>

<script type="text/javascript">
    $(function() {
        
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "transparent";
            bgcolor_err = "#FFCCCC";
            background = "background-color";
            
            err_check_count += check_input_sel("reserve_count");
            err_check_count += check_input_sel("visited_count");
            err_check_count += check_input_sel("visit_adult");
            err_check_count += check_input_sel("visit_child");


            max_i = $("select[name=reserve_count]").children(':selected').val();

            for (var loop=1; loop<=max_i; loop++)
            {
                $("#cate_menu_"+loop).css(background,bgcolor_default);
                if($("input[name='cate_menu_id["+loop+"]']:checked").val()==undefined)
                {
                    err_check_count += 1;
                    $("#cate_menu_"+loop).css(background,bgcolor_err);
                }
                err_check_count += check_input("reserve_sub_name_"+loop);
                err_check_count += check_input("reserve_sub_kana_"+loop);
                err_check_count += check_input("sex_"+loop);
                err_check_count += check_input_birth("reserve_sub_birth_"+loop, "reserve_sub_birth_yyyy_"+loop, "reserve_sub_birth_mm_"+loop, "reserve_sub_birth_dd_"+loop);
            }
            
            if(err_check_count!=0)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {
                //$('#form_confirm').submit();
                $('#form_confirm', "body").submit();
                return true;
            }
            
            
        });
                
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);

            if($('#'+$str).val().replace(/　/g," ").match(/^\s+$/))
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                $("#"+$str).focus();
                
                return 1;
            }
            else if($('#'+$str).val()=="")
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }

        function check_input_birth($str, $str1, $str2, $str3) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str1).css(background,bgcolor_default);
            $("#"+$str2).css(background,bgcolor_default);
            $("#"+$str3).css(background,bgcolor_default);


            if(!check_date($('#'+$str1).val(), $('#'+$str2).val(), $('#'+$str3).val()))
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str1).css(background,bgcolor_err);
                $("#"+$str2).css(background,bgcolor_err);
                $("#"+$str3).css(background,bgcolor_err);
                $("#"+$str1).focus();
                
                return 1;
            }

            return 0;
        }
                
        function check_input_sel($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);

            if($('#'+$str).val().replace(/　/g," ").match(/^\s+$/))
            {
                err ="<span class='error'>正しく選択してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                $("#"+$str).focus();
                
                return 1;
            }
            else if($('#'+$str).val()=="")
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }

        function check_date(vYear, vMonth, vDay) {
            vMonth = (vMonth-1);
        　// 月,日の妥当性チェック
        　if(vMonth >= 0 && vMonth <= 11 && vDay >= 1 && vDay <= 31){
        　　var vDt = new Date(vYear, vMonth, vDay);
        　　if(isNaN(vDt)){
        　　　return false;
        　　}else if(vDt.getFullYear() == vYear && vDt.getMonth() == vMonth && vDt.getDate() == vDay){　　
        　　　return true;
        　　}else{
        　　　return false;
        　　}
        　}else{
        　　return false;
        　}
        }
       
    });


    $(function(){
        <?
        for($loop=2 ; $loop<=$max_count ; $loop++) {
            //修正の場合表示
            if($loop>$reserve_count) {
        ?>
        $('.sel_<? echo $loop;?>').css("display","none");
        <?
            }
        }
        ?>
        $('#reserve_count').change(function(){
            <? for($loop=2 ; $loop<=$max_count ; $loop++) { ?>
            $('.sel_<? echo $loop;?>').css("display","none");
            <? } ?>
            max_i = $("select[name=reserve_count]").children(':selected').val();
            for (var i=1; i<=max_i; i++)
            {
                $('.sel_'+i).css("display","block");
            }
        });
    })
</script>

<article>
<section>
<h1 id="pageTitle">ご予約 -撮影内容の情報入力-</h1>

<div class="stepArea">
<ul>
<li class="active">STEP1<span>日付選択</span></li>
<li class="active">STEP2<span>時間選択</span></li>
<li class="active">STEP3<span>情報入力</span></li>
<li>STEP4<span>確認</span></li>
<li>STEP5<span>予約完了</span></li>
</ul>
</div>

<section class="importantArea">
<? if($status==2 or $status==3 or $status==4){ ?>
<h2 class="tit">ご希望の予約枠はキャンセル待ちになりました。</h2>
<p class="txtWhite">キャンセル待ちをご予約の場合も同様にお進みください</p>
<?php } elseif($status==1) { ?>
<h2 class="tit">ご希望の予約枠をキープしました！</h2>
<?php } ?>
<p class="txt"><span>＜重要＞<br>ご予約はまだ完了していません。</span><br>
選択したご予約日時は30分間保持されています。<br>
30分間を経過、または別画面へ移動してしまった場合、自動的に予約はキャンセルされますのでご注意ください。</p>
<p class="subTxt">ご予約の日時・枠がお間違いないか確認の上、STEP3「撮影内容の情報」を入力してください。</p>
</section>

<section class="formCheckArea">
<h2 class="tit">ご予約の撮影日・時間・コース</h2>
<div class="formCheckAreaIn">
<table>
<tr>
<th>撮影日</th>
<td><? echo $yyyy;?>年<? echo $mm;?>月<? echo $dd;?>日（<? echo $common_connect->Fn_date_day($yyyy.$mm.$dd);?>）</td>
</tr>
<tr>
<th>撮影時間</th>
<td><? echo substr($cate_course_time_from, 0, 5);?> 〜 <? echo substr($cate_course_time_to, 0, 5);?></td>
</tr>
<tr>
<th>撮影コース</th>
<td><? echo $cate_course_name;?></td>
</tr>
</table>
</div>
</section>


<form action="./check.php" method="POST" name="form_write" id="form_regist">
<? $var = "reserve_id";?>
<input type="hidden" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>">
<section class="formEnterArea">
<p class="pTit">STEP3<span>撮影内容の情報を入力してください。</span></p>
<h3 class="tit"><span>今回撮影する方の情報</span></h3>
<dl>
<dt>主役の人数</dt>
<dd>
<div class="selectBox">
<? $var = "reserve_count";?>
<select name="<? echo $var;?>" id="<? echo $var;?>">
<? for($loop=1 ; $loop<=$max_count; $loop++) { ?>
<option value="<? echo $loop;?>" <? if($$var==$loop) { echo " selected ";}?>><? if($loop==$max_count) {echo $loop."〜";} else {echo $loop;}?></option>
<? } ?>
</select> 人
<label id="err_<?=$var;?>"></label>
</div>
</dd>
</dl>
<p class="formTxt">メインで撮影する方が2人以上の場合は、このご予約枠の中で順番にそれぞれの撮影内容を入力してください。</p>
</section>

<section class="formEnterArea">
<!--▼主役-->
<?
    for($loop=1 ; $loop<=$max_count; $loop++) { 

        $reserve_sub_name = "";
        $reserve_sub_kana = "";
        $sex = "";
        $reserve_sub_birth = "";
        $birth_yyyy = "";
        $birth_mm = "";
        $birth_dd = "";
        $cate_menu_id = "";
        $cate_menu_name = "";

        $sql = " select reserve_sub_name, reserve_sub_kana, sex, reserve_sub_birth, cate_menu_id, cate_menu_name FROM reserve_sub ";
        $sql .= " where reserve_id='".$reserve_id."' and reserve_sub_id='".$loop."' " ; 
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $reserve_sub_name = $db_result[0]["reserve_sub_name"];
            $reserve_sub_kana = $db_result[0]["reserve_sub_kana"];
            $sex = $db_result[0]["sex"];
            $reserve_sub_birth = $db_result[0]["reserve_sub_birth"];
            $birth_yyyy = date("Y",strtotime($reserve_sub_birth));
            $birth_mm = date("m",strtotime($reserve_sub_birth));
            $birth_dd = date("d",strtotime($reserve_sub_birth));
            $cate_menu_id = $db_result[0]["cate_menu_id"];
            $cate_menu_name = $db_result[0]["cate_menu_name"];
        }
?>
<div class="modelEnter sel_<? echo $loop;?>">
<dl>
<dt>主役<? echo $loop;?>のお名前</dt>
<dd>
    <? $var = "reserve_sub_name[$loop]";?>
    <input name="<? echo $var;?>" id="reserve_sub_name_<? echo $loop;?>" value="<? echo $reserve_sub_name;?>" type="text" placeholder="木村　花子">
    <label id="err_reserve_sub_name_<? echo $loop;?>"></label>
</dd>
<dt>主役<? echo $loop;?>のふりがな</dt>
<dd>
    <? $var = "reserve_sub_kana[$loop]";?>
    <input name="<? echo $var;?>" id="reserve_sub_kana_<? echo $loop;?>" value="<? echo $reserve_sub_kana;?>" type="text" placeholder="きむら　はなこ">
    <label id="err_reserve_sub_kana_<? echo $loop;?>"></label>
</dd>
<dt>主役<? echo $loop;?>の性別</dt>
<dd>
<div class="selectBox">
    <? $var = "sex[$loop]";?>
    <select name="<? echo $var;?>" id="sex_<? echo $loop;?>">
        <option value="">選択</option>
        <? foreach($arr_sex as $key=>$value) { ?>
        <option value="<? echo $value;?>" <? if($sex==$key && $sex!=""){ echo " selected ";}?>><? echo $value;?></option>
        <? } ?>
    </select>
    <label id="err_sex_<? echo $loop;?>"></label>
</div>
</dd>
<dt>主役<? echo $loop;?>の生年月日</dt>
<dd>
<div class="selectBox">
    <span class="selectYear">
    <? $var = "reserve_sub_birth_yyyy[$loop]";?>
    <select name="<? echo $var;?>" id="reserve_sub_birth_yyyy_<? echo $loop;?>">
    <option value="">選択</option>
    <? foreach($arr_birth_yyyy as $key=>$value) { ?>
    <option value="<? echo $value;?>" <? if($birth_yyyy==$value){ echo " selected ";}?>><? echo $value;?></option>
    <? } ?>
    </select> 年
    </span>
    <span class="selectMonth">
    <? $var = "reserve_sub_birth_mm[$loop]";?>
    <select name="<? echo $var;?>" id="reserve_sub_birth_mm_<? echo $loop;?>">
    <option value="">選択</option>
    <? foreach($arr_birth_mm as $key=>$value) { ?>
    <option value="<? echo $value;?>" <? if($birth_mm==$value){ echo " selected ";}?>><? echo $value;?></option>
    <? } ?>
    </select> 月
    </span>
    <span class="selectDay">
    <? $var = "reserve_sub_birth_dd[$loop]";?>
    <select name="<? echo $var;?>" id="reserve_sub_birth_dd_<? echo $loop;?>">
    <option value="">選択</option>
    <? foreach($arr_birth_dd as $key=>$value) { ?>
    <option value="<? echo $value;?>" <? if($birth_dd==$value){ echo " selected ";}?>><? echo $value;?></option>
    <? } ?>
    </select> 日
    </span>
    <label id="err_reserve_sub_birth_<? echo $loop;?>"></label>
</div>
</dd>
</dl>
<p class="formTxt">以下の選択肢の中から、ご希望の撮影メニューをお選びください。</p>
<dl>
<dt>主役<? echo $loop;?>の撮影メニュー</dt>
<dd>
<ul class="radioUl" id="cate_menu_<? echo $loop;?>">
<? $var = "cate_menu_id[$loop]";?>
<? foreach($arr_cate_menu_id as $key=>$value) { ?>
<li><label><input name="<? echo $var;?>" type="radio" class="radio" value="<? echo $key;?>" <? if($cate_menu_id==$key){ echo " checked ";}?>><? echo $value;?></label></li>
<? } ?>
</ul>
</dd>
</dl>
</div>
<!--▲主役-->
<? } ?>



</section>

<section class="formEnterArea bbBlue">
<h3 class="tit"><span>来店情報</span></h3>
<dl>
<dt>当スタジオのご利用回数</dt>
<dd>
<div class="selectBox">
<?
    $var = "visited_count";
    $visited_count_max = 5;
?>
<select name="<? echo $var;?>" id="<? echo $var;?>">
<option value="">選択</option>
<? for($loop=1 ; $loop<=$visited_count_max; $loop++) { ?>
<option value="<? echo $loop;?>" <? if($$var==$loop) { echo " selected ";}?>><? if($loop==$visited_count_max) {echo $loop."〜";} else {echo $loop;}?></option>
<? } ?>
</select> 回目
<label id="err_<?=$var;?>"></label>
</div>
</dd>
<dt>来店の人数</dt>
<dd>
<div class="selectBox mb10">
大人
<?
    $var = "visit_adult";
    $visit_adult_max = 10;
?>
<select name="<? echo $var;?>" id="<? echo $var;?>">
<option value="">選択</option>
<? for($loop=1 ; $loop<=$visit_adult_max; $loop++) { ?>
<option value="<? echo $loop;?>" <? if($$var==$loop && $$var!="") { echo " selected ";}?>><? if($loop==$visit_adult_max) {echo $loop."〜";} else {echo $loop;}?></option>
<? } ?>
</select> 人
<label id="err_<?=$var;?>"></label>
</div>
<div class="selectBox">
子供
<?
    $var = "visit_child";
    $visit_child_max = 10;
    //子供初期値が0のため大人人数で判断　$visit_adult!=0
?>
<select name="<? echo $var;?>" id="<? echo $var;?>">
<option value="">選択</option>
<? for($loop=0 ; $loop<=$visit_child_max; $loop++) { ?>
<option value="<? echo $loop;?>" <? if($$var==$loop && $visit_adult!=0) { echo " selected ";}?>><? if($loop==$visit_child_max) {echo $loop."〜";} else {echo $loop;}?></option>
<? } ?>
</select> 人

<label id="err_<?=$var;?>"></label>
</div>
</dd>
</dl>
</section>

<section class="message01 bbBlue">
<h4 class="tit">ー　大切なお願い　ー</h4>
<p class="subTit">お人家族で複数枠のご予約はご遠慮頂いております。</p>
<p class="txt">当スタジオは小さなスタジオですので、1日に撮影できるのは、2〜3組のお客様となっています。1組でも多くのお客様にご利用頂きたいと思っておりますので、ご理解の程よろしくお願い致します。</p>
<p class="subTxt">※ 万が一、複数予約が確認された場合、すべてのご予約が無効となりますのでご注意ください。<br>
・キャンセル待ちのご登録は複数可ですが、1家族につき3件までとさせていただいております。<br>
・ご兄妹2人とも主役での撮影をご希望で、別日でお撮りしたい場合はスタジオまでお問合せ下さい。</p>
</section>


<? $var = "form_confirm";?>
<input name="<?php echo $var;?>" id="<?php echo $var;?>" type="submit" value="ご予約内容の確認画面へ進む" class="submitBtn">

</form>



</section>
</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
