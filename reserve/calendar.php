<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<?php
    //コース
    $sql = " select cate_course_id, cate_course_name FROM cate_course where flag_open=1 order by view_level" ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_id[$db_result[$db_loop]["cate_course_id"]] = $db_result[$db_loop]["cate_course_name"];
        }
    }

    $sql = "select cate_course_id, cate_course_name, ";
    $sql .= " (select count(cate_course_time_id) from cate_course_time where cate_course_time.cate_course_id=cate_course.cate_course_id
 and flag_open=1) as cate_course_time_count ";
    $sql .= " FROM cate_course where flag_open=1 order by view_level";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_time_id[$db_result[$db_loop]["cate_course_id"]] = $db_result[$db_loop]["cate_course_time_count"];
        }
    }

    //30分以内の仮予約削除(予約初期化)
    $del_time = date("Y-m-d H:i", strtotime('-31 minute'));
    $db_del = "Delete from reserve where flag_open=0 and regi_date<'".$del_time."' ";
    $common_dao->db_update($db_del);
?>
<article>
<section>
<h1 id="pageTitle">ご予約 -カレンダー日付選択-</h1>

<div class="stepArea">
<ul>
<li class="active">STEP1<span>日付選択</span></li>
<li>STEP2<span>時間選択</span></li>
<li>STEP3<span>情報入力</span></li>
<li>STEP4<span>確認</span></li>
<li>STEP5<span>予約完了</span></li>
</ul>
</div>

<section class="attentionArea">
<h2 class="tit"><img src="/common/img/reserve/tit_attention.png" alt="カレンダーに進む前に！"></h2>
<p class="txt mb10">カレンダーに表記されている「ショートの日」「スタンダードの日」「今月のキャンペーン」の違いにご注意下さい。</p>
<p class="txt mb10"><span class="bold">ショートの日</span>（ショート枠のみの撮影日/1日3枠）<br>
<span class="bold">［撮影内容］</span>撮影時間約20分・1シーンのみ・￥25,000〜</p>
<p class="txt mb10"><span class="bold">スタンダードの日</span>（スタンダード枠のみの撮影日/1日2枠）<br>
<span class="bold">［撮影内容］</span>撮影時間約40分・3シーンまで・￥35,000〜</p>
<p class="txt mb30"><span class="bold">今月のキャンペーン</span>（1日6枠）<br>
<span class="bold">［撮影内容］</span>キャンペーンによって変わります</p>
<table>
<table>
<thead>
<tr>
<th colspan="10">撮影メニュー別 ショート/スタンダード枠早見表</th>
</tr>
</thead>
<tbody>
<tr>
<th></th>
<th>ニュー<br>ボーン</th>
<th>百日</th>
<th>ハーフ<br>バースディ</th>
<th>誕生日</th>
<th>入園入学</th>
<th>七五三</th>
<th>マタニ<br>ティ</th>
<th>還暦</th>
</tr>
<tr>
<th class="side">ショート</th>
<td>ー</td></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td>ー</td></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
</tr>
<tr>
<th class="side">スタンダード</th>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
<td><img src="/common/img/reserve/ico_ok.png" alt="予約可"></td>
</tr>
</tbody>
</table>
<p class="subTit">＜事前に各撮影メニューをご確認ください＞</p>
<p class="txt">撮影メニューによっては、ショート枠ではお撮り出来ない撮影もございます。<br>
ショート枠とスタンダード枠の違いに関しましては、料金等の違いもございますので、必ず<a href="/menu/" class="cBlue">各撮影メニュー</a>をご覧頂いた上でご予約ください。
また、メインで撮影される方がお2人以上の場合は、必ずスタンダードの日を選択して下さい。</p>
</section>

<?php
    $add_day = 1;
    $yyyymmdd = date("Y-m-01");
    $today_ymd = date("Ymd"); //今日の日付
    $today_hi = date("Hi"); //今日の時間

    //（テスト用）
    if($_GET["today_ymd"]!="")
    {
        $today_ymd = $_GET["today_ymd"];
    }
    if($_GET["today_hi"]!="")
    {
        $today_hi = $_GET["today_hi"];
    }
    

    $today_ymdhhii = $today_ymd.$today_hi;
    $check_time = global_check_time; //更新基準時間
    $today_day = date("Y-m-d", strtotime($today_ymd));


    if(date("w", strtotime($today_ymd))==1 && date("Hi", strtotime($today_ymdhhii))<$check_time)
    {
        $loop_week = 3;
    }
    else
    {
        $loop_week = 4;
    }

    //月曜日の不具合？
    if(date("w", strtotime($today_ymd))==1)
    {
        $loop_week += 1;
    }

    //past
    $past_end = date("Ymd", strtotime($today_day." ".$add_day." day"));

    $green_start = date("Ymd", strtotime($today_day." Monday ".($loop_week-1)." week"));
    $green_end = date("Ymd", strtotime($today_day." Sunday ".($loop_week)." week"));
    $notyet_start = date("Ymd", strtotime($today_day." Monday ".($loop_week)." week"));

    //$addday_ymd = date("Ym01", strtotime($today_ymd." ".(30+$add_day)." day"));
    $addday_ymd = date("Ym01", strtotime($green_end));

    $date1=strtotime($today_ymd);
    $date2=strtotime($addday_ymd);
    $month1=date("Y",$date1)*12+date("m",$date1." 00:00");
    $month2=date("Y",$date2)*12+date("m",$date2." 00:00");
    $diff = $month2 - $month1;
    $max_month = $diff+1;
?>
<section class="calendarArea">
<h2 class="reservationStatusTit">ご予約空き状況</h2>
<dl class="guideboard">
<dt>毎週月曜日の昼<? echo substr($check_time, 0, 2);?>:<? echo substr($check_time, 2);?>更新！ <br>
1ヶ月先の1週間分の予約が公開されます。 </dt>
<dd>次回の予約開始日は<span><?echo date("n月j日",strtotime( $today_day." Monday +".($loop_week-4)." week "))?></span>です</dd>
</dl>


<?
//最新公開枠表示
$check_sun = 0;
$check_mon = 0;
$check_day = 0;
for ($i=0 ; $i < $max_month ; $i++) {
    
    $sql  = "SELECT '".$yyyymmdd."' + INTERVAL $i MONTH as yyyymm";
    $result_month = $common_dao->db_query_bind ($sql);
    $yyyy = date("Y", strtotime($result_month[0]["yyyymm"]));
    $mm = date("m", strtotime($result_month[0]["yyyymm"]));
    
    //最初の曜日
    $firstTime = strtotime($yyyy . "-" . $mm . "-01"); 
    $firstWeek = date("w", $firstTime);
    if($firstWeek==0) { $firstWeek=7; } //■月曜日から始まる
    
    //最後の日
    for ($ld = 28; checkdate($mm,$ld,$yyyy); $ld ++); 
    $lastDay = $ld - 1;
    
    //最後の曜日
    $lastTime = strtotime($yyyy . "-" . $mm . "-" . $lastDay); 
    $lastWeek = date("w", $lastTime); 
    if($lastWeek==0) { $lastWeek=7; } //■月曜日から始まる
                
    //arrへ保存
    $arrSCH = array(array(),array(),array(),array(),array(),array(),array()); 
    
    //該当する最初の曜日になる前にはブランク
    for($j = 1; $j < $firstWeek; $j ++) {
        $arrSCH[$j][] = ""; 
    }
    
    $maxRow = 0; 
    for($schDay = 1; $schDay <= $lastDay; $schDay ++) { 
        $checkday = mktime(0, 0, 0, $mm, $schDay, $yyyy); 
        $week = date("w", $checkday);
        if($week==0) { $week=7; } //■月曜日から始まる
        $arrSCH[$week][] = $schDay; 
    
        if(count($arrSCH[$week]) > $maxRow) $maxRow = count($arrSCH[$week]); 
    } 


    //日毎のプラン
    //初期化
    $p = array();
    $sql = "select  ";
    for($loop=1 ; $loop<=$lastDay ; $loop++) {
        $sql .= "p_".substr($loop+100, 1)." , ";
    }
    $sql .= " up_date from schedule ";
    $sql .= " where yyyymm='".$yyyy.substr($mm+100, 1)."'";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($loop=1 ; $loop<=$lastDay ; $loop++) {
            $p[substr($loop+100, 1)] = $db_result[0]["p_".substr($loop+100, 1)];
        }
    }
?>
<div class="calendarBox">
<table class="main">
<thead>
<tr>
<th class="tit" colspan="7"><?php echo $yyyy;?>年<?php echo $mm;?>月<a href="javascript:document.location.reload()"><img src="/common/img/reserve/btn_reload.png" alt="更新"></a></th>
</tr>
<tr>
<th>月</th>
<th>火</th>
<th>水</th>
<th>木</th>
<th>金</th>
<th class="sat">土</th>
<th class="sun">日</th>
</tr>
</thead>
<tbody>
            
<?



for($row = 0; $row < $maxRow; $row ++) { 
    echo "<tr>"; 
    for($col = 1; $col < 8; $col ++) { 
        $day = $arrSCH[$col][$row]; 
        $dd = substr(($day+100),1);

        $cal_class = "";
        $check_link = false;
        if(checkdate($mm, $dd, $yyyy))
        {
            if(date("Ymd",strtotime($yyyy . "-" . $mm . "-" . $day))<=$past_end)
            {
                $cal_class .= "past ";
            }
            else
            {
                $check_day++;
                $check_link = true;
            }

            //公開された次の週はクリック出来ない
            if(date("Ymd",strtotime($yyyy . "-" . $mm . "-" . $day))>=$notyet_start)
            {
                $cal_class .= "notyet ";
                $check_link = false;
            }

            if($col == 1)
            {
                if(date("Ymd",strtotime($yyyy . "-" . $mm . "-" . $day))>date("Ymd", strtotime($today_ymd." ".$add_day." day")))
                {
                    $check_mon++;
                }
            }
            if(date("Ymd",strtotime($yyyy."-".$mm."-".$day))>=$green_start && date("Ymd",strtotime($yyyy."-".$mm."-".$day))<=$green_end)
            {
                $cal_class .= "open ";
            }
        }

        if($check_link)
        {
            if($arr_cate_course_time_id[$p[$dd]]>0)
            {
                $cal_class .= "clickable ";
            }
        }

        if($col == 7)
        {
            $cal_class .= "sun ";
            if(date("Ymd",strtotime($yyyy . "-" . $mm . "-" . $day))>date("Ymd", strtotime($today_ymd." ".$add_day." day")))
            {
                $check_sun++;
            }
        }
        elseif($col == 6)
        {
            $cal_class .= "sat ";
        }
        if($arr_cate_course_time_id[$p[$dd]]<1)
        {
            $cal_class .= "none ";
        }

        //予約データ
        $reserve_count_1 = 0;//予約可能
        $reserve_count_2 = 0;//キャンセル待ちのみ可能
        $reserve_count_3 = 0;//ご予約埋まりました
        $sql = "SELECT cate_course_time_id, ";
        $sql .= " (select max(status) as max_status from reserve ";
        $sql .= " where reserve_day='".$yyyy."-".$mm."-".$dd."' and (status<=4 or status=100) and ";
        $sql .= " reserve.cate_course_time_id=cate_course_time.cate_course_time_id) as max_status
     FROM cate_course_time  ";
        $sql .= " where cate_course_id='".$p[$dd]."' and flag_open=1 ";
        //echo $sql;

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
                //予約可能が一個もあれば○
                if($db_result[$db_loop]["max_status"]<1 )
                {
                    $reserve_count_1++;
                } elseif($db_result[$db_loop]["max_status"]==1 || $db_result[$db_loop]["max_status"]==2 || $db_result[$db_loop]["max_status"]==3)
                {
                    $reserve_count_2++;
                } elseif($db_result[$db_loop]["max_status"]==4 || $db_result[$db_loop]["max_status"]==100)
                {
                    $reserve_count_3++;
                }
            }
        }
        
        if($reserve_count_1>0) {
            $check_circle = "circle"; //予約可能
        } elseif($reserve_count_2>0) {
            $check_circle = "triangle"; //キャンセル待ちのみ可能
        } elseif($reserve_count_3>0) {
            $check_circle = "cross"; //ご予約埋まりました
            $cal_class = str_replace("clickable", "", $cal_class); //リンクなし
        }

        $check_loginbox = false;
        echo "<td ";
        //リンク
        if($check_link)
        {
            if($day)
            {
                if($arr_cate_course_time_id[$p[$dd]]>0)
                {
                    if($check_circle == "cross") {
                        //Xの場合リンクなし
                    } elseif($_SESSION["member_id"] == "") {
                        $check_loginbox = true;
                        $cal_class .= "nologin ";
                    } else {
                        echo " data-href=\"time.php?yyyymmdd=".$yyyy.$mm.$dd."\"  ";
                    }

                }
            }
        }

        echo " class=\"".$cal_class."\">";
        //ログインしなければ
        if($check_loginbox==true && $check_circle != "cross") {
            echo " <a class=\"iframe cboxElement\" href=\"/mypage/login/?return_url=/reserve/time.php?yyyymmdd=".$yyyy.$mm.$dd."\"> ";
        }

        if($day)
        {
            echo "<span class=\"num\">".$day."</span>";
            if($arr_cate_course_time_id[$p[$dd]]>0)
            {
        ?>
            <span class="course"><? echo $arr_cate_course_id[$p[$dd]];?>（<? echo $arr_cate_course_time_id[$p[$dd]];?>枠）</span>
            <span class="icon <? echo $check_circle;?>"></span>
        <?
            }
            else
            {
        ?>
            <span class="course"><? if($p[$dd]!=5) { echo $arr_cate_course_id[$p[$dd]]; } //撮影なしは表示させない?></span>
            <span class="course none">ー</span>
        <?
            }
        }
        //ログインしなければ
        if($check_loginbox && $check_circle != "cross") {
            echo "</a>";
        }
        
        echo "</td>"; 
    } 
    echo "</tr>"; 
} 
?>
</tbody>
</table>
<table class="sub">
<tr>
<th>○</th>
<td>予約可能</td>
<th>△</th>
<td>キャンセル待ちのみ可能</td>
</tr>
<tr>
<th>ー</th>
<td>撮影なし</td>
<th>×</th>
<td>ご予約埋まりました</td>
</tr>
<? if(date("Ym",$date2)==$yyyy.$mm) { ?>
<tr>
<th><span class="open"></span></th>
<td colspan="3">最新公開枠</td>
</tr>
<tr>
<th><span class="notyet"></span></th>
<td colspan="3">未公開枠（毎週月曜日の昼12:30更新）</td>
</tr>
<? } ?>
</table>
</div>
<?php
}
?>


</section>

</section>
</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
