<?php
    require_once $_SERVER['DOCUMENT_ROOT'].$DOCUMENT_ROOT."/app_include/connect.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<?php
    //ログインチェック
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION["member_id"];

    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    if($reserve_id == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }

    //仮予約情報
    $arr_db_field = array("reserve_id", "member_id", "member_name_1", "member_name_2", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM reserve where reserve_id='".$reserve_id."' and member_id='".$member_id."' and flag_open=0" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }
    if($member_name_1=="")
    {
        $common_connect -> Fn_javascript_back("予約情報が正しくありません。");
    }

    $yyyy = date("Y",strtotime($reserve_day));
    $mm = date("m",strtotime($reserve_day));
    $dd = date("d",strtotime($reserve_day));

    //メニュー
    if($common_connect->h($_POST ["cate_menu_id"][1])!="")
    {
        $sql = " select cate_menu_id, cate_menu_name FROM cate_menu ";
        $sql .= " where flag_open=1 and cate_menu_id='".$_POST ["cate_menu_id"][1]."' order by view_level" ; 
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
            {
                $get_cate_menu_id[$db_loop] = $db_result[$db_loop]["cate_menu_id"];
                $get_cate_menu_name[$db_loop] = $db_result[$db_loop]["cate_menu_name"];
                $arr_cate_menu_id[$db_result[$db_loop]["cate_menu_id"]] = $db_result[$db_loop]["cate_menu_name"];
            }
        }
    }

    $datetime = date("Y/m/d H:i:s");

    //仮予約main update
    //array
    $arr_db_field = array("reserve_count", "visited_count", "visit_adult", "visit_child");
    $db_insert = "update reserve set ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= $val."='".$$val."', ";
    }
    
    $db_insert .= " cate_menu_id='".$get_cate_menu_id[0]."', ";
    $db_insert .= " cate_menu_name='".$get_cate_menu_name[0]."', ";
    $db_insert .= " up_date='".$datetime."' ";
    $db_insert .= " where reserve_id='".$reserve_id."' and member_id='".$member_id."' and flag_open=0";
    $db_result = $common_dao->db_update($db_insert);
    
    //仮予約subリセット登録
    $db_del .= " Delete from reserve_sub where reserve_id='".$reserve_id."' ";
    $db_result = $common_dao->db_update($db_del);

    for($loop=1 ; $loop<=$reserve_count; $loop++) {
        $reserve_sub_name = $common_connect->h($_POST["reserve_sub_name"][$loop]);
        $reserve_sub_kana = $common_connect->h($_POST["reserve_sub_kana"][$loop]);
        $sex = $common_connect->h($_POST["sex"][$loop]);
        //日付チェック
        $reserve_sub_birth = "";
        if(checkdate($common_connect->h($_POST["reserve_sub_birth_mm"][$loop]), $common_connect->h($_POST["reserve_sub_birth_dd"][$loop]), $common_connect->h($_POST["reserve_sub_birth_yyyy"][$loop]))) {
            $reserve_sub_birth = $common_connect->h($_POST["reserve_sub_birth_yyyy"][$loop])."-".$common_connect->h($_POST["reserve_sub_birth_mm"][$loop])."-".$common_connect->h($_POST["reserve_sub_birth_dd"][$loop]);
        }
        $cate_menu_id = $common_connect->h($_POST["cate_menu_id"][$loop]);
        $cate_menu_name = $arr_cate_menu_id[$cate_menu_id];

        $arr_db_field = array("reserve_id", "reserve_sub_name", "reserve_sub_kana", "sex", "reserve_sub_birth", "cate_menu_id", "cate_menu_name");

        $db_insert = "insert into reserve_sub ( ";
        $db_insert .= " reserve_sub_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " regi_date, up_date ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '".$loop."', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '$datetime', '$datetime')";
        $db_result = $common_dao->db_update($db_insert);
    }

?>
<article>
<section>
<h1 id="pageTitle">ご予約 -撮影内容の確認-</h1>

<div class="stepArea">
<ul>
<li class="active">STEP1<span>日付選択</span></li>
<li class="active">STEP2<span>時間選択</span></li>
<li class="active">STEP3<span>情報入力</span></li>
<li class="active">STEP4<span>確認</span></li>
<li>STEP5<span>予約完了</span></li>
</ul>
</div>

<section class="descriptionArea">
<h2 class="tit"><? echo $member_name_1;?> <? echo $member_name_2;?> 様のご予約内容</h2>
<p>入力内容にお間違いがないか確認のうえ「確定する」ボタンを押して下さい。</p>
</section>


<form action="./check_save.php" method="POST">
<? $var = "reserve_id";?>
<input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
<section class="formCheckArea">
<h2 class="tit">ご予約の撮影日・時間・コース</h2>
<div class="formCheckAreaIn">
<table>
<tr>
<th>撮影日</th>
<td><? echo $yyyy;?>年<? echo $mm;?>月<? echo $dd;?>日（<? echo $common_connect->Fn_date_day($yyyy.$mm.$dd);?>）</td>
</tr>
<tr>
<th>撮影時間</th>
<td><? echo substr($cate_course_time_from, 0, 5);?> 〜 <? echo substr($cate_course_time_to, 0, 5);?></td>
</tr>
<tr>
<th>撮影コース</th>
<td><? echo $cate_course_name;?></td>
</tr>
</table>
</div>
</section>

<section class="formCheckArea">
<h2 class="tit">撮影内容</h2>
<div class="formCheckAreaIn">
<table>
<tr>
<th>主役の人数</th>
<td>
<? $var = "reserve_count";?>
<input type="hidden" name="<? echo $var;?>"><? echo $$var;?>人
</td>
</tr>
<tr>
<th>来店人數</th>
<td>
<? $var = "visit_adult";?>
大人
<input type="hidden" name="<? echo $var;?>"><? echo $$var;?>人　
<? $var = "visit_child";?>
子供
<input type="hidden" name="<? echo $var;?>"><? echo $$var;?>人 
</td>
</tr>
<tr>
<th>スタジオご利用回数</th>
<td>
<? $var = "visited_count";?>
<input type="hidden" name="<? echo $var;?>"><? echo $$var;?>回目
</td>
</tr>
<? for($loop=1 ; $loop<=$reserve_count; $loop++) { ?>
<tr>
<th>主役<? echo $loop;?>のお名前</th>
<td>
    <? $var = "reserve_sub_name[$loop]";?>
    <? $$var = $common_connect->h($_POST["reserve_sub_name"][$loop]);?>
    <input type="hidden" name="<? echo $var;?>"><? echo $$var;?>
</td>
</tr>
<tr>
<th>主役<? echo $loop;?>のふりがな</th>
<td>
    <? $var = "reserve_sub_kana[$loop]";?>
    <? $$var = $common_connect->h($_POST["reserve_sub_kana"][$loop]);?>
    <input type="hidden" name="<? echo $var;?>"><? echo $$var;?>
</td>
</tr>
<tr>
<th>主役<? echo $loop;?>の性別</th>
<td>
    <? $var = "sex[$loop]";?>
    <? $$var = $common_connect->h($_POST["sex"][$loop]);?>
    <input type="hidden" name="<? echo $var;?>"><? echo $$var;?>
</td>
</tr>
<tr>
<th>主役<? echo $loop;?>の生年月日</th>
<td>
    <?
    //日付チェック
    if(checkdate($common_connect->h($_POST["reserve_sub_birth_mm"][$loop]), $common_connect->h($_POST["reserve_sub_birth_dd"][$loop]), $common_connect->h($_POST["reserve_sub_birth_yyyy"][$loop]))) { ?>
    <? $var = "reserve_sub_birth_yyyy[$loop]";?>
    <? $$var = $common_connect->h($_POST["reserve_sub_birth_yyyy"][$loop]);?>
    <input type="hidden" name="<? echo $var;?>"><? echo $$var;?>年 
    <? $var = "reserve_sub_birth_mm[$loop]";?>
    <? $$var = $common_connect->h($_POST["reserve_sub_birth_mm"][$loop]);?>
    <input type="hidden" name="<? echo $var;?>"><? echo $$var;?>月 
    <? $var = "reserve_sub_birth_dd[$loop]";?>
    <? $$var = $common_connect->h($_POST["reserve_sub_birth_dd"][$loop]);?>
    <input type="hidden" name="<? echo $var;?>"><? echo $$var;?>日
    <? } ?>
</td>
</tr>
<tr>
<th>主役<? echo $loop;?>の撮影メニュー</th>
<td>
    <? $var = "cate_menu_id[$loop]";?>
    <? $$var = $common_connect->h($_POST["cate_menu_id"][$loop]);?>
    <input type="hidden" name="<? echo $var;?>"><? echo $arr_cate_menu_id[$$var];?>
</td>
</tr>
<? } ?>

</table>
</div>
</section>

<div class="descriptionArea">
<p class="formTxt">入力頂いたお客様の登録情報に関しましては、<a href="/about/privacy.php" class="cBlue" target="_blank">個人情報保護方針・ご利用規約</a>の同意をお願い致します。</p>
<input type="submit" value="利用規約に同意して、予約を確定する" class="submitBtn">
</form>

<script type="text/javascript">
<!--
$('#form_confirm').click(function() {

    //$('#form_confirm').submit();
    $('#form_confirm', "body").submit();
    //return true;

});

//-->
</script>
<form action="form.php" method="POST" name="form_write">
  <?php $var = "reserve_id";?>
  <input type="hidden" name="<?php echo $var;?>" id="<?php echo $var;?>" value="<?php echo $$var;?>">
  <?php $var = "form_confirm";?>
  <input type="submit" name="<?php echo $var;?>" id="<?php echo $var;?>" value="入力内容を編集に戻る" class="backBtn">
</form>

<p class="bNotes">「予約を確定する」ボタンを押すまで、ご予約は完了していません。30分間を経過してしまった場合、自動的に予約がキャンセルとなりますのでお気をつけ下さい！</p>
</div>

</section>
</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
