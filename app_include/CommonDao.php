<?php
class CommonDao{

  protected $mysqli = null;
	protected $db;

  //コンストラクタ
  function __construct(){	
    $this->connect();
  }

  //デストラクタ
  function __destruct(){
    $this->disconnect();	
  }

  //MySQLサーバへ接続
  protected function connect(){
		try {
			$option = array(
				PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
				PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
				PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
				PDO::ATTR_EMULATE_PREPARES => false,
				PDO::ATTR_STRINGIFY_FETCHES => false
			);
			$this->db = new PDO("mysql:dbname=".DB_NAME.";host=".DB_SERVER,DB_USER,DB_PASSWORD, $option);
			$this->db->query("SET NAMES utf8");
		} catch (PDOException $e) {
			exit('データベース接続失敗。'.$e->getMessage());
		}
		/*
    if(is_null($this->mysqli)){
      $this->mysqli = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
      $this->mysqli->query("SET NAMES utf8");
      if(mysqli_connect_errno()){
        die("MySQLサーバ接続に失敗しました<br> 理由：" . mysqli_connect_error());
      }
    }
		*/
  }

  //MySQLサーバと切断
  protected function disconnect(){
    is_null($this->mysqli) or $this->mysqli->close();
  }

  public function transaction_start(){
    is_null($this->mysqli) and $this->connect();
    if( !$this->mysqli->query("set autocommit = 0")){
      return "トランザクション開始に失敗しました(1)" . $this->mysqli->error . "\n";
    }else{
    	if(!$this->mysqli->query("begin")){
    		return "トランザクション開始に失敗しました(2)" . $this->mysqli->error . "\n";
    	}else{
    		return true;
    	}
    }
  }

  public function transaction_end(){
    if( !$this->mysqli->query("commit")){
      $this->mysqli->query("rollback");
      return "コミットに失敗しました" . $this->mysqli->error . "\n";
    }
    return true;
  }

  public function real_escape_string($str){
    if(is_null($this->mysqli)){
        $this->mysqli = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);
        $this->mysqli->query("SET NAMES utf8");
        $result = $this->mysqli->real_escape_string($str);
    }
    else
    {
        $result = $this->mysqli->real_escape_string($str);
    }
    return $result;
  }
	
  public function transaction_rollback(){
    $this->mysqli->query("rollback");
  }

  //DB Query
  /*
  public function db_query($sql){
    is_null($this->mysqli) and $this->connect();
    $result = $this->mysqli->query($sql);
    $db_result = array();
		
	if($result){
		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$db_result[] = $row;
		}
		$result->close();
	}else{
		$db_result = null;
	}
    return $db_result;
  }
  */
	
  public function db_query_bind($sql, $arr_bind=array()){

		$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	
		// エラー時に例外を発生させる（任意）
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
		$sth = $this->db->prepare($sql);
		
		// 3. 型を指定してパラメータにバインドする
		if($arr_bind)
		{
			foreach($arr_bind as $key=>$value)
			{
				//echo $key.":".$value."<hr />";
				$sth->bindValue(":".$key, $value, PDO::PARAM_STR);
			}
		}
		
		// SQL の実行
		$db_result = array();
		$sth->execute();
		$sth->setFetchMode(PDO::FETCH_ASSOC);
    //$db_result = $sth->fetch();
		if($sth) {
			while ($result = $sth->fetch()){     //クラスのインスタンスを直接取得
					$db_result[] = $result;
			}
		}else{
			$db_result = null;
		}
		//$this->disconnect();
		
    return $db_result;
  }
	
  //DB Query with Transaction
  public function db_query_ts($sql){
    $result = $this->mysqli->query($sql);
    $db_result = array();

    if($result){
	    while($row = $result->fetch_array(MYSQLI_ASSOC)){
	      $db_result[] = $row;
	    }
	    $result->close();
    }
    return $db_result;
  }

  //DB Update
  public function db_update($sql, $arr_bind){
		
		$this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	
		// エラー時に例外を発生させる（任意）
		$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
		$sth = $this->db->prepare($sql);
		
		// 3. 型を指定してパラメータにバインドする
		if($arr_bind)
		{
			foreach($arr_bind as $key=>$value)
			{
				$sth->bindValue(":".$key, $value, PDO::PARAM_STR);
			}
		}
		
		// SQL の実行
		$db_result = array();
		$sth->execute();
		//$sth->setFetchMode(PDO::FETCH_ASSOC);
		
		$this->disconnect();
		
  }

  //DB Update with Transaction
  public function db_update_ts($sql){
    $result = $this->mysqli->query($sql);
    if( !$this->mysqli->query($sql)){
      return "更新に失敗しました" . $this->mysqli->error . "\n";
    }
    return true;
  }

}

?>
