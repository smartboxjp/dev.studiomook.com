<?php 
session_start();
$global_path = $_SERVER['DOCUMENT_ROOT']."/";
require_once $global_path."/app_include/connect_db.php";

require_once $global_path."/app_include/CommonConnect.php";
require_once $global_path."/app_include/CommonDao.php";
	
$global_send_mail = "support@studiomook.com";//メールマガ
$global_mail_from = "Mr.MOOK";
$global_img_temp_dir = "PHOTO_TEMP/";

define(global_reserve_dir, "app_photo/reserve/");
define(global_member_dir, "app_photo/member/");
define(global_name, "Mr.MOOK");
define(global_copyright, "studiomook.com");
define(global_check_time, "1230");//更新基準時間

$global_reserve_view[5] = "予約もキャンセル待ちも埋まりました";
$global_reserve_view[4] = "キャンセル待ち３";
$global_reserve_view[3] = "キャンセル待ち２";
$global_reserve_view[2] = "キャンセル待ち１";
$global_reserve_view[1] = "予約確定";


$global_reserve_admin_view[1] = "予約確定";
$global_reserve_admin_view[2] = "キャンセル待ち１";
$global_reserve_admin_view[3] = "キャンセル待ち２";
$global_reserve_admin_view[4] = "キャンセル待ち３";
$global_reserve_admin_view[99] = "キャンセル";
$global_reserve_admin_view[100] = "撮影完了";

if($_SERVER['HTTP_HOST']=="dev.studiomook.com")
{
    //テストサーバー
    $global_service_url = "http://dev.studiomook.com";
    $global_bcc_mail = "support@studiomook.com";
    define('global_no_ssl', "http://dev.studiomook.com");
    define('global_ssl', "http://dev.studiomook.com");
}
else
{
    //本番
    $global_service_url = "https://studiomook.com";
    $global_bcc_mail = "support@studiomook.com";
    define('global_no_ssl', "http://studiomook.com");
    define('global_ssl', "https://studiomook.com");
}

$global_email_footer = <<<EOF
※本メールは送信専用です。ご返信いただいても回答致しかねますので、ご了承ください。ご不明な点はお電話にてお願い致します。
※このメールにお心当たりのない方へ
他の方が誤って貴方のメールアドレスで登録したものと思われます。
メール配信を停止する手続きをいたしますので、お手数ですが件名を「誤った登録」にして頂き、会員登録専用アドレス（support@studiomook.com）までご連絡下さい。

──────────────────────────────────────────────

フォトスタジオ Mr.MOOK / ミスタームック
住所 / 〒901-2134 沖縄県浦添市港川2-12-8
TEL / 098-911-6202
住所 / 営業時間　9:30 am 〜 18:00 pm
定休日 / 月・土曜日
駐車場 / スタジオ隣りに2台分（近隣にコインPもあります）

──────────────────────────────────────────────
EOF;
?>