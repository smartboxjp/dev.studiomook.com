
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<article class="white">
<div id="mainSlide">
<div class="mainSlideIn">
<ul class="slider">
<li><img src="/common/img/index/img_main03.jpg" alt="Web予約始まりました!!"></li>
<li><img src="/common/img/index/img_main01_0206.jpg" alt="Web予約始まりました!!"></li>
<li><img src="/common/img/index/img_main02.jpg" alt="Web予約始まりました!!"></li>
</ul>
</div>
</div>

<section id="infoArea">
<h2 class="indexTit">お知らせ</h2>
<div class="infoBox">
<p class="infoImg"><img src="/common/img/index/tit_info.png" alt="Stuidio Info"></p>
<p class="infotxtMain">ミスタームックの撮影のご予約方法が、<br>
<span class="cRed">電話予約</span>から<span class="cRed">web予約</span>に変わりました！ </p>
<p class="infotxtSub">ご予約をスムーズに行って頂くために、撮影のご予約方法と流れをご確認ください。</p>
<a href="/reserve/" class="greenBtn">撮影のご予約方法と流れを確認する</a>
</div>
<div class="purpleBox">
これまで1日2組限定で撮影をご案内していましたが、2017年3月のご予約より「ショート枠」と「スタンダード枠」がお選び頂けるようになり、撮影枠が増えました！<br>
これまでの料金体制も変更になり、撮影メニューごとにお得なセットをご用意致しましたので、リピーターのお客様も新規のお客様も、ご希望の<a href="/menu/" class="cBlue">撮影メニュー</a>から詳細をご覧ください。
</div>
<section class="infoList">
<h3 class="infoListTit">STUDIO INFORMATION</h3>
<dl>
<dt>2017/02/01<span>お知らせ</span></dt>
<dd>第一回ご予約受付は、2月6日(月)のお昼12:30にスタート致します。詳しくは予約カレンダーをご確認ください。</dd>
<dt>2017/02/01<span>お知らせ</span></dt>
<dd>3月2日〜5日の間、スタジオ内改装のため休業となります。商品のお受け取りをご希望のお客様は、大変お手数ですが、すれ違いを避ける為、ご来店前に一度スタジオへお電話をお願い致します！</dd>
<dt>2017/02/01<span>お知らせ</span></dt>
<dd>ミスタームックのwebサイトがリニューアルしました^^</dd>
</dl>
</section>
</section>

<section id="photomenuArea">
<div class="photomenuAreaIn">
<h2 class="indexTit">撮影メニュー</h2>
<ul class="menuTop hover">
<li><a href="/menu/birthday.php"><img src="/common/img/index/img_birthday.jpg" width="238" alt="バースディ記念"><p><span>バースディ記念</span>1年の成長を改めて実感できる特別な日。過ぎ行く日々の、大切な記念日を写真に。</p></a></li>
<li><a href="/menu/100days.php"><img src="/common/img/index/img_100days.jpg" width="238" alt="百日記念"><p><span>百日記念</span>赤ちゃんの成長に合わせた仕草やポーズで自然な姿を思い出に。</p></a></li>
<li><a href="/menu/celebration753.php"><img src="/common/img/index/img_753.jpg" width="238" alt="七五三"><p><span>七五三</span>それぞれの年の大事な節目だから、特別な「今」をその子らしく。</p></a></li>
</ul>
<ul class="menuBottom hover">
<li><a href="/menu/newborn.php"><img src="/common/img/index/img_newborn.jpg" width="200" alt="ニューボーン">ニューボーン</a></li>
<li><a href="/menu/halfbirthday.php"><img src="/common/img/index/img_halfbirthday.jpg" width="200" alt="ハーフバースディ">ハーフバースディ</a></li>
<li><a href="/menu/entrance.php"><img src="/common/img/index/img_entrance.jpg" width="200" alt="入園・入学記念">入園・入学記念</a></li>
<li><a href="/menu/family.php"><img src="/common/img/index/img_family.jpg" width="200" alt="家族写真">家族写真</a></li>
</ul>
<a href="/menu/" class="greenBtn">すべての撮影メニューを一覧でみる</a>
</div>
</section>

<section id="studioArea">
<h2 class="indexTit">スタジオ紹介</h2>
<p>ミスタームックは、沖縄の港川外人住宅街にある一軒家の小さな写真スタジオです。<br>
1組1組丁寧に撮影を行っている為、1日に撮影できるのは、2組から3組のお客様です。<br>
日々の思い出や瞬間がいつか大切な一枚になる、そんなことを感じてもらえるよう、自然光の下で、その時その瞬間にしかない仕草や表情を大切に撮影を行っています。</p>
<p class="tit"><img src="/common/img/index/tit_howstudio.png" alt="動画でみる撮影の様子"></p>
<ul class="movieList hover">
<li><a href="https://player.vimeo.com/video/155281486?autoplay=1" class="vimeo"><img src="/common/img/index/img_movie753.jpg" width="338" alt="七五三の撮影風景"></a></li>
<li><a href="https://player.vimeo.com/video/155281512?autoplay=1" class="vimeo"><img src="/common/img/index/img_moviefamily.jpg" width="338" alt="兄弟・家族の撮影風景"></a></li>
</ul>
</section>

<section id="productArea">
<div class="productAreaIn">
<h2 class="indexTit">商品紹介</h2>
<p class="txt">大切な写真を、どのようなカタチで残していくか。ミスタームックでは様々な種類の商品を取り揃えています。<br>
「 写真こそインテリアに 」をコンセプトに、撮影前から丁寧に打合せをさせて頂いております。<br>
「飾る」そして「残していく」それぞれの各種商品を、ひとつひとつ大切にお作りしてお届け致します。</p>
<p class="img"><img src="/common/img/index/img_product.jpg" alt="商品紹介"></p>
<!--<a href="/product/" class="greenBtn">すべての各種商品を一覧でみる</a>-->
</div>
</section>

<div class="whiteBg">
<section class="reserveBottom">
<div class="reserveCircle">
<p><img src="/common/img/share/bg_reserve.png" alt="ご予約はWebから"></p>
</div>
<div class="txt">webからのご予約がはじめての方は、まずは「 <a href="/reserve/" class="cBlue">撮影のご予約方法と流れ</a> 」をご確認ください
<a href="/reserve/" class="yellowBtn"><span>撮影のご予約はこちら</span></a>
</div>
</section>
</div>

<section id="accessArea">
<div class="accessAreaIn">
<h2 class="indexTit">店舗情報</h2>
<div class="addressBox">
<p class="img"><img src="/common/img/index/img_studio.jpg" width="330" alt="フォトスタジオ Mr.MOOK / ミスタームック"></p>
<p class="tit">フォトスタジオ Mr.MOOK / ミスタームック</p>
<p class="txt">住所 / 〒901-2134  沖縄県浦添市港川2-12-8<br>
TEL / 098-911-6202<br>
住所 / 営業時間　9:30 am 〜 18:00 pm<br>
定休日 / 月・土曜日<br>
駐車場 / スタジオ隣りに2台分（近隣にコインPもあります）</p>
<p class="hover"><a href="https://www.facebook.com/studiomook/" target="_blank"><img src="/common/img/index/btn_facebook.png" alt="FACEBOOK"></a></p>
</div>
<div class="gMap">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3577.9532304196514!2d127.71439831503122!3d26.26318128341166!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x34e56b7bb9965fb9%3A0xaaf2c9584a6de6c!2z44CSOTAxLTIxMzQg5rKW57iE55yM5rWm5re75biC5riv5bed77yS5LiB55uu77yR77yS4oiS77yY!5e0!3m2!1sja!2sjp!4v1485321203912" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
</div>
</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php");?>
