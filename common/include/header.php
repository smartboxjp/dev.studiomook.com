<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/common.php"); ?>

<!DOCTYPE HTML>
<html>
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
<meta charset="UTF-8">
<?php if($member){ ?>
<meta name="robots" content="noindex,nofollow">
<?php } ?>
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=0">
<meta name="description" content="<?php echo $meta_description; ?>">
<title><?php echo $meta_title; ?></title>
<link rel="stylesheet" href="/common/css/default.css">
<link rel="stylesheet" href="/common/css/sp.css" media="only screen and (max-width:780px)">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/common/js/jquery.easing.1.3.js"></script>
<script src="/common/js/jquery.bxslider.js"></script>
<script src="/common/js/script.js"></script>
<script src="/common/js/analytics.js"></script>
<script src="/common/js/colorbox/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="/common/js/colorbox/colorbox.css">
<!--▼favicon-->
<link href="https://fonts.googleapis.com/css?family=Crimson+Text" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link rel="apple-touch-icon" sizes="180x180" href="/common/img/share/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/common/img/share/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/common/img/share/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/common/img/share/favicon/manifest.json">
<link rel="mask-icon" href="/common/img/share/favicon/safari-pinned-tab.svg" color="#26afaf">
<meta name="theme-color" content="#ffffff">
<!--▲favicon-->
<!--▼OGP-->
<meta property="og:title" content="<?php echo $meta_title; ?>" />
<meta property="og:type" content="<?php echo $meta_type; ?>" />
<meta property="og:url" content="https://studiomook.com<?php echo $_SERVER['REQUEST_URI']; ?>" />
<meta property="og:image" content="https://studiomook.com/common/img/<?php echo $meta_image; ?>" />
<meta property="og:site_name" content="沖縄フォトスタジオ Mr.MOOK（ミスタームック）" />
<meta property="og:description" content="<?php echo $meta_description; ?>" />
<meta property="fb:app_id" content="1333385380055407" />
<meta name="twitter:card" content="photo" />
<!--▲OGP-->

</head>

<body<?php if($loginpage == ture){ echo ' id="popup"';}?>>
<?php session_start(); ?>
<?php if(!$loginpage){ ?>

<div id="wrapper">
<header>
<div class="headerIn">
<div id="logo">
<p class="logoImg"><a href="/"><img src="/common/img/header/logo_img.png" alt="フォトスタジオミスタームック"></a></p>
<p class="logoTxt"><a href="/"><img src="/common/img/header/logo_txt.png" alt="PHOTO STUDIO Mr.MOOK"></a></p>
</div>
<nav>

<!--▼PC-->
<div id="navPc" class="hover">
<ul class="btn">
<li><a href="/reserve/"><img src="/common/img/header/btn_reserve.png" alt="撮影のご予約はこちら"></a></li>
<li>
    <?php
        if($_SESSION['member_name']!="") { 
            $url_mypage = "/mypage/";
            $url_mypage_class = "";
        } else {
            $url_mypage = "/mypage/login/?return_url=/mypage/";
            $url_mypage_class = "iframe";
        }
    ?>
    <a href="<? echo $url_mypage;?>" class="<? echo $url_mypage_class;?>"><img src="/common/img/header/btn_mypage.png" alt="マイページで予約確認"></a>
</li>
</ul>
<ul class="nav">
<!--
<li><a href="/qa/"><img src="/common/img/header/nav_qa.png" alt="よくある質問"></a></li>
<li><a href="/flow/"><img src="/common/img/header/nav_flow.png" alt="撮影の流れ"></a></li>
<li><a href="/product/"><img src="/common/img/header/nav_product.png" alt="各種商品"></a></li>
-->
<li><a href="/menu/"><img src="/common/img/header/nav_menu.png" alt="撮影メニュー"></a></li>
<!--
<li><a href="/studio/"><img src="/common/img/header/nav_studio.png" alt="スタジオの紹介"></a></li>
-->
</ul>
</div>
<!--▲PC-->

<!--▼SP-->
<div id="navSp">
<p class="menuBtn"><img src="/common/img/header/btn_menusp.png" alt="MENU"></p>
</div>
<!--▲SP-->
</nav>
</div><!--headerIn-->

<div id="menuSp">
<div class="naviArea">
<ul>
<!--
<li><a href="/studio/"><span>STUDIO</span>スタジオの紹介</a></li>
-->
<li><a href="/menu/"><span>MENU</span>撮影メニュー</a></li>
<!--<li><a href="/product/"><span>PRODUCT</span>各種商品</a></li>
<li><a href="/flow/"><span>FLOW</span>撮影の流れ</a></li>
<li><a href="/qa/"><span>Q&amp;A</span>よくある質問</a></li>-->
</ul>
</div>
</div>

</header>

<?php if($_SESSION['member_name']!="") { ?>
<!--▼ログイン中に表示-->
<div id="loginBar">
<p>
<a href="/mypage/" class="name"><?php echo $_SESSION['member_name'] . "様"; ?></a>
<a href="/mypage/login/logout.php">ログアウト</a>
</p>
</div>
<!--▲ログイン中に表示-->
<?php } ?>

<?php } ?>