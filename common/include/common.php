<?php 
	$url = explode('/', $_SERVER["REQUEST_URI"]);//urlを'/'で分割
	$category_file = $url[1];//カテゴリーファイル
	$url_parts = pathinfo($_SERVER["SCRIPT_NAME"]);//urlを名称で分割
	$page_file = $url_parts['filename'];//最終ファイル名の拡張子抜き

	if($url[2] == login){
		$loginpage = ture;
	}
	if($url[1] == ""){
		$home = ture;
	}

	//各ページタイトル&OGP

	$meta_image = 'index/img_main01.jpg';
	$meta_description = 'ミスタームックは、沖縄の港川外人住宅街にある一軒家の小さな写真スタジオです。1組1組丁寧に撮影を行っている為、1日に撮影できるのは、2組から3組のお客様です。';
	$meta_type = archive;    	
	if($url[1] == 'menu'){
		$category_title = '撮影メニュー';
		$meta_description = '一組一組大切に撮影したいという思いから、スタジオで過ごす時間も楽しんで頂くために、完全貸切での撮影をご案内しております。';
    	if($page_file == 'newborn'){ 
    		$title = 'ニューボーン';
    		$meta_description = '「ニューボーンフォト」とは生後15日ぐらいまでの、生まれて間もない赤ちゃんの撮影です。生後2週間までは、ママのおなかの中にいた時の様なまるまった体勢をしているため、神秘的な写真を残すことができます。';
    		$slag = $page_file;
    		$meta_image = $url[1] . '/' . $page_file . '/img_main.jpg';
    	}elseif($page_file == '100days'){ 
    		$title = '百日記念';
    		$meta_description = '赤ちゃんが誕生してから、目まぐるしい毎日の中でも、ちょっとした仕草や笑顔、泣き顔までも愛おしいと思う、そんな赤ちゃん時期だけの特別な時間は、きっとずっと大切にしたい瞬間。';
    		$slag = $page_file;
    		$meta_image = $url[1] . '/' . $page_file . '/img_main.jpg';
    	}elseif($page_file == 'halfbirthday'){ 
    		$title = 'ハーフバースディ';
    		$meta_description = 'ハーフバースディを迎えたお子さまは、今まで寝ているだけだった仕草から、寝返りやおすわりができるようになって、赤ちゃん時期からちょこっと卒業し始めているような、そんな成長が嬉しかったり名残惜しかったり。';
    		$slag = $page_file;
    		$meta_image = $url[1] . '/' . $page_file . '/img_main.jpg';
    	}elseif($page_file == 'birthday'){ 
    		$title = 'バースディ';
    		$meta_description = '何気ない表情や仕草の中に、「その子らしさ」がたくさん溢れているから、笑顔だけにこだわらず、写真スタジオで記念写真を撮るという概念から少しだけ離れたナチュラルスタイルでの撮影をご提案させて頂いています。';
    		$slag = $page_file;
    		$meta_image = $url[1] . '/' . $page_file . '/img_main.jpg';
    	}elseif($page_file == 'celebration753'){ 
    		$title = '七五三';
    		$meta_description = '七五三のお祝いを迎えた、それぞれの年齢が魅せる成長や個性。「あの時はこうだったね〜」と思い出を振り返る時、今と過去を結びつけてくれる一枚でありますように';
    		$slag = $page_file;
    		$meta_image = $url[1] . '/' . $page_file . '/img_main.jpg';
    	}elseif($page_file == 'entrance'){ 
    		$title = '入園/卒園・入学/卒業';
    		$meta_description = 'ランドセルがやって来た日は嬉しくて、小さな背中に背負っては嬉しそうにはしゃぐ姿。ランドセルの中にはピカピカのノートや教科書、それとお友だちにあげるんだよと言って大切に包んだ謎の折り紙も詰め込んで、登校する日を心待ちにしてる。';
    		$slag = $page_file;
    		$meta_image = $url[1] . '/' . $page_file . '/img_main.jpg';
    	}elseif($page_file == 'maternity'){ 
    		$title = 'マタニティ';
    		$meta_description = 'マタニティフォトをお撮りする時、だんだん丸くなってゆくお腹とともに、おなかの中ですくすくと育っていく生命の、神秘的で奇跡のような瞬間を、その存在を1番近くに感じているママの想いや、楽しみに待っている家族の想いも一緒に、生まれてきた赤ちゃんに残してあげられる様に、「家族らしさ」を大切に撮影したいと思っています。';
    		$slag = $page_file;
    		$meta_image = $url[1] . '/' . $page_file . '/img_main.jpg';
    	}elseif($page_file == 'senior'){ 
    		$title = '還暦・喜寿・米寿';
    		$meta_description = '還暦のお祝いのお問合せを頂く時、大体の方がご本人ではなくその家族の誰かだったり、娘さん達からだったりします。';
    		$slag = $page_file;
    		$meta_image = $url[1] . '/' . $page_file . '/img_main.jpg';
    	}elseif($page_file == 'location'){ 
    		$title = '出張撮影';
    		$meta_description = 'ロケーションフォトは、2つの撮影スタイルでご案内しています。ご自宅やお気に入りの場所で、いつもの風景や元気いっぱいに遊ぶ姿を撮影する「 日常撮影プラン 」。大切な記念日に同行して、晴れ姿や見守るご家族の風景を撮影する「 ハレの日同行プラン 」。';
    		$slag = $page_file;
    		$meta_image = $url[1] . '/' . $page_file . '/img_main.jpg';
    	}elseif($page_file == 'family'){ 
    		$title = '家族写真/兄弟写真';
    		$meta_description = '毎年一枚、家族写真を撮りにスタジオに来てくれるお客様が持っていたアルバム。ページをめくっていくとその一冊の中には家族の歴史が1年1年刻まれていて、結婚してひとりがふたりになった日の照れくさそうな表情、ママになってからの表情、ふたりが３人になって、赤ちゃんだったお子さんが毎年大きく成長していく姿....そしてまた今年のストーリーを綴っていく。';
    		$slag = $page_file;
    		$meta_image = $url[1] . '/' . $page_file . '/img_main.jpg';
    	}
    }elseif($url[1] == 'about'){
    	if($page_file == 'privacy'){ 
    		$title = '個人情報保護方針・ご利用規約';
    		$meta_description = 'Mr.MOOK（以下「当社」）は、以下のとおり個人情報保護方針を定め、個人情報保護の仕組みを構築し、全従業員に個人情報保護の重要性の認識と取組みを徹底させることにより、個人情報の保護を推進致します。';
    	}
	}elseif($url[1] == 'mypage'){
		$category_title = 'マイページ';
		$meta_description = '一組一組大切に撮影したいという思いから、スタジオで過ごす時間も楽しんで頂くために、完全貸切での撮影をご案内しております。';
		$member = true;
		if($page_file == 'reserved'){
			$title = '予約の確認';
		}elseif($page_file == 'information'){
			$title = '登録者情報';
		}elseif($page_file == 'information_form'){
			$title = '登録者情報の変更';
		}elseif($page_file == 'history'){
			$title = '撮影履歴';
		}elseif($page_file == 'waiting'){
			$title = 'キャンセル待ち';
		}
	}elseif($url[1] == 'reserve'){
		$category_title = '撮影のご予約';
		$meta_description = 'ご予約は1ヶ月先までお取り頂けます。毎週月曜日の昼12:30に、新しいご予約枠が 1週間分ずつ公開されます。';
		if($page_file == 'calendar'){
			$title = 'カレンダー日付選択';
		}elseif($page_file == 'time'){
			$title = '希望時間の選択';
		}elseif($page_file == 'form'){
			$title = '撮影内容の情報入力';
		}elseif($page_file == 'check'){
			$title = '撮影内容の確認';
		}elseif($page_file == 'thanks'){
			$title = 'ご予約完了';
		}
	}elseif($url[1] == 'registration'){
		$category_title = '新規会員登録';
		if($page_file == 'check'){
			$title = '新規会員登録：入力内容の確認';
			$member = true;
		}elseif($page_file == 'thanks'){
			$title = '新規会員登録：完了';
			$member = true;
		}
    }else{
		$meta_type = website;
    }


	//サイトタイトル
	if($home){
		$site_title = '沖縄フォトスタジオ Mr.MOOK（ミスタームック）| 七五三・百日記念・子供写真';
		$meta_title = $site_title;
	}else{
		$site_title = ' | 沖縄フォトスタジオ Mr.MOOK（ミスタームック）';
	}

	if($category_title){
		$meta_title =  $category_title . $site_title;
		if($title){
			$meta_title = $title . ' | ' . $category_title . $site_title;
		}
	}elseif(!$category_title){
		if($title){
			$meta_title = $title . $site_title;
		}
	}
?>