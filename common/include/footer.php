<?php if(!$loginpage){ ?>
<footer<?php if($home){echo ' class="whiteBg"';}?>>
<p class="gototop"><img src="/common/img/footer/btn_gototop.png" alt="GO TO TOP"></p>
<!--
<div class="footerNav">
<p class="homeSp"><a href=""><span>HOME</span>サイトのトップページへ戻る</a></p>
<ul>
<li><a href=""><span>STUDIO</span>スタジオの紹介</a></li>
<li><a href=""><span>MENU</span>撮影メニュー</a></li>
<li><a href=""><span>PRODUCT</span>各種商品</a></li>
<li><a href=""><span>FLOW</span>撮影の流れ</a></li>
<li><a href=""><span>Q&amp;A</span>よくある質問</a></li>
<li class="footermenuSp"><a href="">利用規約と<br>個人情報保護方針</a></li>
</ul>
-->
</div>
<p class="footerTel"><img src="/common/img/footer/img_tel.png" alt="098-911-6202"></p>
<p class="wanted"><!--<a href="">採用情報はこちら</a>--></p>
<p class="copy">Copyright &copy; PHOTO STUDIO Mr.MOOK All Rights Reserved.</p>
<ul class="footerBtn">
<li class="access"><a href="/#accessArea">交通アクセス</a></li>
<li class="tel"><a href="tel:0989116202">電話する</a></li>
<?php
    if($_SESSION['member_name']!="") { 
        $url_mypage = "/mypage/";
        $url_mypage_class = "";
    } else {
        $url_mypage = "/mypage/login/?return_url=/mypage/";
        $url_mypage_class = "iframe";
    }
?>
<li class="mypage"><a href="<? echo $url_mypage;?>" class="<? echo $url_mypage_class;?>">マイページ</a></li>
<li class="reserve"><a href="/reserve/">撮影のご予約</a></li>
</ul>
</footer>
</div>
<?php } ?>
</body>
</html>