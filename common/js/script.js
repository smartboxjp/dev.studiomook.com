// ----------------------
// pegeTOPへ戻る
// ----------------------
$(function(){
var topBtn=$('.gototop');
//topBtn.hide();
 
//◇ボタンの表示設定
/*
$(window).scroll(function(){
  if($(this).scrollTop()>80){
    //---- 画面を80pxスクロールしたら、ボタンを表示する
    topBtn.fadeIn();
  }else{
    //---- 画面が80pxより上なら、ボタンを表示しない
    topBtn.fadeOut();
  } 
});
 */

// スクロール
topBtn.click(function(){
  $('body,html').animate({
  scrollTop: 0},500);
  return false;
});

});




// ----------------------
// menuSp開閉
// ----------------------

$(function(){
$('.menuBtn').click(function(){
		if ($('#menuSp').css('display') == 'none') {
				$('#menuSp').fadeIn( 500, 'easeOutQuart');
		} else {
				$('#menuSp').fadeOut( 500, 'easeOutQuart');
		}
});

$('article').click(function(){
        $('#menuSp').fadeOut( 500, 'easeOutQuart');
});

});


// ----------------------
// POPUP
// ----------------------

$(document).ready(function(){
$(".iframe").colorbox({
iframe:true,
width:"80%",
height:"560px"
});
});

$(document).ready(function(){
$(".vimeo").colorbox({
iframe:true,
width:"80%",
height:"436px"
});
});

// ----------------------
// テーブルをリンクにする
// ----------------------
jQuery(function($) {
  //data-hrefの属性を持つtrを選択しclassにclickableを付加
  $('tr[data-href]').addClass('clickable')
    //クリックイベント
    .click(function(e) {
      //e.targetはクリックした要素自体、それがa要素以外であれば
      if(!$(e.target).is('a')){
        //その要素の先祖要素で一番近いtrの
        //data-href属性の値に書かれているURLに遷移する
        window.location = $(e.target).closest('tr').data('href');}
  });
});/*テーブルをリンクにする*/
jQuery(function($) {
  //data-hrefの属性を持つtrを選択しclassにclickableを付加
  $('td[data-href]').addClass('clickable')
    //クリックイベント
    .click(function(e) {
      //e.targetはクリックした要素自体、それがa要素以外であれば
      if(!$(e.target).is('a')){
        //その要素の先祖要素で一番近いtdの
        //data-href属性の値に書かれているURLに遷移する
        window.location = $(e.target).closest('td').data('href');}
  });
});

// ----------------------
// bxslider
// ----------------------
$(function(){
  $('.slider').bxSlider({
    auto: true,
    slideWidth: 960,
    minSlides: 3,
    maxSlides: 3,
    moveSlides: 1,
    slideMargin: 0
  });
});