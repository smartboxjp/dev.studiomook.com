
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>
<article>
<section id="<?php echo $slag; ?>">
<div id="innerTit"
<?php if($_SESSION['member_name']!="") { echo ' class="login"';}?>><h1><span>NEW BORN</span><?php echo $title;?><img src="/common/img/menu/index/bg_tit.png"></h1></div>
<p class="mainImg"><img src="/common/img/menu/<?php echo $slag; ?>/img_main.jpg" alt="<?php echo $title;?>"></p>

<section class="pageTop">
<h2 class="mainTxt">生後15日までの、神秘的な瞬間</h2>
<p class="subTxt">ママのおなかの中にいた時のような、まるまった姿勢や産毛<br>
限られたわずかな期間しか撮ることの出来ない<br>
「New Born Photo 」<br>
一瞬で過ぎ去ってしまう貴重な瞬間の大切な一枚</p>
</section>

<section>
<h2 class="menuTit">新生児フォト</h2>
<div class="subBox">
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_sub.jpg" alt="<?php echo $title;?>"></p>
<div class="txt">
<p>「ニューボーンフォト」とは生後15日ぐらいまでの、生まれて間もない赤ちゃんの撮影です。<br>
生後2週間までは、ママのおなかの中にいた時の様なまるまった体勢をしているため、神秘的な写真を残すことができます。</p>
ニューボーンフォトは、主に寝ている時に撮影します。<br>
生後2週間を過ぎると、眠りが浅く起きている時間が長くなる為、撮影途中で起きてしまったり、また、体がしっかりとしてきて赤ちゃんも活動的になり、新生児特有の丸まったポーズが難しくなってきます。<br>
撮影は技術的な知識が必要になりますので、ご希望の方は、ご予約前に一度お電話でお問い合わせ頂き、専任のスタッフに何でもご相談くださいませ。
</div>
</div><!--subBox-->

<section class="planArea">
<div class="planAreaIn">
<h3 class="planTit"><img src="/common/img/menu/share/tit_plan.png" alt="選べるプランをチェック"></h3>
<p class="planTxt"><?php echo $title;?>フォトは赤ちゃんのコンディションに合わせて撮影していきます。<br>
<span class="standard">スタンダード枠</span> の撮影枠のみのご案内となっております。</p>

<section class="planBox" id="standard">
<h4 class="planBoxTit"><span>スタンダード枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影は赤ちゃんが深く眠ってからスタートしますので、その日のコンディションに合わせて進めて行きます。<br>
衣装は基本的には裸んぼで撮影しますが、撮影に必要な小物やおくるみ等はスタジオに用意しております。</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日2枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「スタンダード」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>9:30〜12:30</li>
<li>14:00〜17:00</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>スタンダード枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・アクセサリーや帽子・小物一式<br>
・家族写真サービス</p>
<p class="captionTxt">※ ご家族写真をご希望の場合は、できるだけ柄や色味の強くないシンプルなスタイルをおすすめしています。<br>
特に白い色のお洋服は、抱っこしてあげた時、赤ちゃんの肌をよりきれいに見せてくれる効果があります。家族の雰囲気を統一することで、より素敵な家族写真に仕上がります^^<br>
赤ちゃんのコンディションによっては、全ての撮影ができず、ご希望に添えない場合もございますので予めご了承ください。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_standard.png" alt="Set Product"></p>
</div>
<h4 class="setTit">スタンダード枠で選べる3セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_a.png" alt="Aset"></p>
<p class="setBoxName">1ポーズのみクッション撮影<span>［ 基本セット ＋ パネル ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>25,000</p>
<ul>
<li>六つ切りデザインパネル（ 1カット ）</li> 
<li>データ（ 10カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">撮影スタイルは1ポーズのみ、クッションの上でかわいらしくポーズをとっての撮影になります。<br>
撮影後、お写真選びはスタジオにおまかせ頂き、1枚のパネルにアートレタッチでドラマティックにお作り致します。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set01.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_b.png" alt="Bset"></p>
<p class="setBoxName">クッション＆バスケット撮影<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>32,000</p>
<ul>
<li>MOOKBOOK2面Mサイズ</li> 
<li>データ（ 10〜20カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">撮影スタイルは2シーン。<br>
クッションの上プラス、バスケットなどの中に入ってよりアートな雰囲気の仕上がりになります。<br>
撮影後は1冊のフォトブックへ、レイアウトデザインしてお渡し致します。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set02.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_c.png" alt="Cset"></p>
<p class="setBoxName">等身大ブックとパーツ撮影<span>［ 基本セット ＋ ムックアルバム ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>35,000</p>
<ul>
<li>MOOKBOOK2面Lサイズ</li>
<li>収録データ（ 10〜20カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">撮影スタイルは1シーン。<br>
クッションの上でかわいらしくポーズをとっての撮影後、身長や足、お手てのサイズを測って、生まれて間もない大切な「今」を等身大のお写真として記録し、フォトブックにデザイン致します。</p>
<p class="img bp"><img src="/common/img/menu/<?php echo $slag; ?>/img_set03.jpg" width="380" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

</div>
</section><!--planArea-->

</section><!--menuArea-->

<div class="whiteBg">
<section class="reserveBottom">
<div class="reserveCircle">
<p><img src="/common/img/share/bg_reserve.png" alt="ご予約はWebから"></p>
</div>
<div class="txt">ミスタームックのご予約がwebから出来るようになりました！
<a href="/reserve/" class="yellowBtn"><span>撮影のご予約はこちら</span></a>
</div>
</section>
</div>

</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
