
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>
<article>
<section id="hundreddays">
<div id="innerTit"
<?php if($_SESSION['member_name']!="") { echo ' class="login"';}?>><h1><span>100DAYS</span><?php echo $title;?><img src="/common/img/menu/index/bg_tit.png"></h1></div>
<p class="mainImg"><img src="/common/img/menu/<?php echo $slag; ?>/img_main.jpg" alt="<?php echo $title;?>"></p>

<section class="pageTop">
<h2 class="mainTxt">生まれて100日目のお祝い<br>特別な「赤ちゃんの時間」</h2>
<p class="subTxt">ふたりの手の中に包まれるぐらい小さかった時の記憶は<br>
大きくなったら覚えていないかもしれないけど、<br>
「こんなに小さかったね」っていつか話せるような一枚を</p>
</section>

<section>
<h2 class="menuTit">100Daysフォト</h2>
<div class="subBox">
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_sub.jpg" alt="<?php echo $title;?>"></p>
<div class="txt">
<p>赤ちゃんが誕生してから、目まぐるしい毎日の中でも、ちょっとした仕草や笑顔、泣き顔までも愛おしいと思う、そんな赤ちゃん時期だけの特別な時間は、きっとずっと大切にしたい瞬間。</p>
百日記念の撮影は、赤ちゃんの成長に合わせて、椅子やゆりかご、ベッドを使って撮影します。<br>
お父さんやお母さんの手から、少しだけ離れてひとりでがんばる100日記念。<br>
撮影中は、なるべく近くで声をかけて頂きながら、赤ちゃんの自然な仕草や表情、時にはかわいいおしりやお手てなど、この時期ならではのムニムニした体の一部も含めて、かけがえのない瞬間を撮影します。
</div>
</div><!--subBox-->
<section class="sceneBox">
<h3 class="tit">スタジオ撮影シーン</h3>
<p class="img"><img src="/common/img/menu/share/img_ura.png" alt="公開ウラ舞台"></p>
<ul>
<li><img src="/common/img/menu/<?php echo $slag; ?>/img_scene01.jpg" alt="<?php echo $title;?>スタジオ撮影シーン公開ウラ舞台"></li>
<li><img src="/common/img/menu/<?php echo $slag; ?>/img_scene02.jpg" alt="<?php echo $title;?>スタジオ撮影シーン公開ウラ舞台"></li>
</ul>
</section><!--sceneBox-->

<section class="planArea">
<div class="planAreaIn">
<h3 class="planTit"><img src="/common/img/menu/share/tit_plan.png" alt="選べるプランをチェック"></h3>
<p class="planTxt">百日記念の撮影は、<br class="sp"><span class="short">ショート枠</span> or <span class="standard">スタンダード枠</span> の<br class="sp">2つの撮影枠のどちらかからお選び頂けます。</p>

<section class="planBox" id="short">
<h4 class="planBoxTit"><span>ショート枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間20分、衣装1着・1シーンまでのショートプラン<br>
撮影後の写真セレクトもスタジオにおまかせ♪</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日3枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「ショート」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>10:00〜11:00</li>
<li>12:00〜13:00</li>
<li>14:30〜15:30</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>ショート枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・衣装1着 / アクセサリーや帽子・小物一式<br>
・家族写真サービス</p>
<p class="captionTxt">※ ご機嫌による時間延長等はありません。泣いていても笑顔でも、その瞬間の成長の思い出としてお撮り致します。人見知りなどが心配な方は、スタンダード枠をお選び頂くことをおすすめ致します。<br>
※ 兄妹2人以上が主役となる撮影の場合は、必ずスタンダード枠をお選びください。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_short.png" alt="Set Product"></p>
</div>
<h4 class="setTit">ショート枠で選べる3セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_a.png" alt="Aset"></p>
<p class="setBoxName">シンプルなオリジナルデザイン台紙<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>25,000</p>
<ul>
<li>MOOKBOOK2面Mサイズ（ 5カット ）</li> 
<li>収録データ（ 5カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set01.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_b.png" alt="Bset"></p>
<p class="setBoxName">絵本のようなストーリーデザイン<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>29,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 8カット ）</li> 
<li>収録データ＋2カット（ 10カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set02.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_c.png" alt="Cset"></p>
<p class="setBoxName">飾るをテーマにアートな1枚を創る<span>［ 基本セット ＋ パネル ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>34,000</p>
<ul>
<li>選べるパネル（ 3種類 ）</li>
<li class="none">［ 等身大パネル ］<br>
［ 足型パネル ］＋［ 2Lパネル ］<br>
［ スクエアパネル ］＋［ 2Lパネル ］</li>
<li>ALLデータ（ 20カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">生まれて間もない大切な「今」を等身大のお写真として記録し、パネルにデザイン致します。<br>
赤ちゃんお一人での等身大撮影か、両親の手の中に包まれるぐらいの大きさだったことを表現したアートパネルもおすすめです。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set03.jpg" width="680" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

<section class="planBox" id="standard">
<h4 class="planBoxTit"><span>スタンダード枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間は40分、衣装2〜3着・3シーンまでのスタンダードプラン。<br>
これまでの撮影スタイルと同じ、お子さまに合わせてゆったりとお撮りする撮影です。</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日2枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「スタンダード」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>9:30〜12:30</li>
<li>14:00〜17:00</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>スタンダード枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・衣装2〜3着 / アクセサリーや帽子・小物一式<br>
・家族写真サービス</p>
<p class="captionTxt">※ 兄妹2人以上が主役となる撮影の場合は、必ずスタンダード枠をお選びください。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_standard.png" alt="Set Product"></p>
</div>
<h4 class="setTit">スタンダード枠で選べる3セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_d.png" alt="Aset"></p>
<p class="setBoxName">色んな表情や仕草を未来へ残す一冊<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>35,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 8カット ）</li> 
<li>ALLデータ（ 40〜50カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set04.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_e.png" alt="Bset"></p>
<p class="setBoxName">レイアウト自由なBOOKが新登場♪<span>［ 基本セット ＋ インデックスブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>40,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 14カット ）</li> 
<li>ALLデータ（ 40〜50カット ）</li> 
<li>2L3面台紙 or 2Lパネル</li>
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set05.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_f.png" alt="Cset"></p>
<p class="setBoxName">撮影メイキングも収録した特別な一冊<span>［ 基本セット ＋ ムックアルバム ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>50,000</p>
<ul>
<li>MOOKALBUM（ 20カット ）</li>
<li>ALLデータ（ 40〜50カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">特別な日の記念撮影を、見守る両親の姿や、撮影中の舞台裏で起こっていた数々のメイキングシーンも合わせて撮影し、1冊のアルバムに収録します。<br>
ページをめくる度に思い出が蘇る、未来に残して欲しい1冊です。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set06.jpg" width="546" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

</div>
</section><!--planArea-->

<section class="galleryBox">
<h3 class="menuTit"><span>PHOTO GALLERY</span></h3>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_gallery.jpg" alt="<?php echo $title;?>"></p>
</section>


</section><!--menuArea-->

<section class="reserveBottom">
<div class="reserveCircle">
<p><img src="/common/img/share/bg_reserve.png" alt="ご予約はWebから"></p>
</div>
<div class="txt">ミスタームックのご予約がwebから出来るようになりました！
<a href="/reserve/" class="yellowBtn"><span>撮影のご予約はこちら</span></a>
</div>
</section>

</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
