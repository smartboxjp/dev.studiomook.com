
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>
<article>
<section id="<?php echo $slag; ?>">
<div id="innerTit"
<?php if($_SESSION['member_name']!="") { echo ' class="login"';}?>><h1><span>BIRTHDAY</span><?php echo $title;?><img src="/common/img/menu/index/bg_tit.png"></h1></div>
<p class="mainImg"><img src="/common/img/menu/<?php echo $slag; ?>/img_main.jpg" alt="<?php echo $title;?>"></p>

<section class="pageTop">
<h2 class="mainTxt">一年間のストーリー<br>
成長していく姿や何気ない仕草ひとつひとつ<br>
いつまでも思い出せるように</h2>
<p class="subTxt">1年間の成長を改めて実感できる特別な日<br>
一歩踏み出してはペタンとしりもち、それでも前に進む<br>
記憶の中に閉じ込めておきたい瞬間</p>
</section>

<section>
<h2 class="menuTit">誕生日フォト</h2>
<div class="subBox">
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_sub.jpg" alt="<?php echo $title;?>"></p>
<div class="txt">
<p>何気ない表情や仕草の中に、「その子らしさ」がたくさん溢れているから、笑顔だけにこだわらず、写真スタジオで記念写真を撮るという概念から少しだけ離れたナチュラルスタイルでの撮影をご提案させて頂いています。</p>
よーいスタートの場所は決まっていても、そこからどんな動きを見せてくれるのか、予期せぬ動きが得意な1才さんはいつも私達の予想を飛び越えて、撮影時はいつもにぎやかになります。<br>
かくれてるつもりのいないいないばあの、小さなお手てからはみ出しちゃってるニヤニヤだったり、緊張の涙ポロリンも含めて、色んな成長のひとコマを大切に撮影しています。
</div>
</div><!--subBox-->
<section class="sceneBox">
<h3 class="tit">スタジオ撮影シーン</h3>
<p class="img"><img src="/common/img/menu/share/img_ura.png" alt="公開ウラ舞台"></p>
<ul>
<li><img src="/common/img/menu/<?php echo $slag; ?>/img_scene01.jpg" alt="<?php echo $title;?>スタジオ撮影シーン公開ウラ舞台"></li>
<li><img src="/common/img/menu/<?php echo $slag; ?>/img_scene02.jpg" alt="<?php echo $title;?>スタジオ撮影シーン公開ウラ舞台"></li>
</ul>
</section><!--sceneBox-->

<section class="planArea">
<div class="planAreaIn">
<h3 class="planTit"><img src="/common/img/menu/share/tit_plan.png" alt="選べるプランをチェック"></h3>
<p class="planTxt"><?php echo $title;?>の撮影は、<br class="sp"><span class="short">ショート枠</span> or <span class="standard">スタンダード枠</span> の<br class="sp">2つの撮影枠のどちらかからお選び頂けます。</p>

<section class="planBox" id="short">
<h4 class="planBoxTit"><span>ショート枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間20分、衣装1着・1シーンまでのショートプラン<br>
撮影後の写真セレクトもスタジオにおまかせ♪</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日3枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「ショート」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>10:00〜11:00</li>
<li>12:00〜13:00</li>
<li>14:30〜15:30</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>ショート枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・衣装1着 / アクセサリーや帽子・小物一式<br>
・家族写真サービス</p>
<p class="captionTxt">※ ご機嫌による時間延長等はありません。泣いていても笑顔でも、その瞬間の成長の思い出としてお撮り致します。人見知りなどが心配な方は、スタンダード枠をお選び頂くことをおすすめ致します。<br>
※ 兄妹2人以上が主役となる撮影の場合は、必ずスタンダード枠をお選びください。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_short.png" alt="Set Product"></p>
</div>
<h4 class="setTit">ショート枠で選べる3セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_a.png" alt="Aset"></p>
<p class="setBoxName">シンプルなオリジナルデザイン台紙<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>25,000</p>
<ul>
<li>MOOKBOOK2面Mサイズ（ 5カット ）</li> 
<li>収録データ（ 5カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set01.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_b.png" alt="Bset"></p>
<p class="setBoxName">絵本のようなストーリーデザイン<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>29,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 8カット ）</li> 
<li>収録データ＋2カット（ 10カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set02.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_c.png" alt="Cset"></p>
<p class="setBoxName">飾るをテーマにアートな1枚を創る<span>［ 基本セット ＋ パネル ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>34,000</p>
<ul>
<li>選べるパネル（ 3種類 ）</li>
<li class="none">［ おすわり記念パネル ］＋［ 2Lパネル ］<br>
［ あんよ記念デザインパネル ］<br>
［ スクエアパネル ］＋［ 2Lパネル ］</li>
<li>ALLデータ（ 20カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">誕生から1年、2年...どんどん色んな事が出来るようになっていく我が子の、初めて出来た記念の「サクセスストーリー」を写真として記録し、パネルにデザイン致します。<br>
初めてのあんよやそれぞれの年の色んな表情。見逃せない我が子の成長の瞬間を、大切に残しませんか？</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set03.jpg" width="480" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

<section class="planBox" id="standard">
<h4 class="planBoxTit"><span>スタンダード枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間は40分、衣装2〜3着・3シーンまでのスタンダードプラン。<br>
これまでの撮影スタイルと同じ、お子さまに合わせてゆったりとお撮りする撮影です。</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日2枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「スタンダード」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>9:30〜12:30</li>
<li>14:00〜17:00</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>スタンダード枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・衣装2〜3着 / アクセサリーや帽子・小物一式<br>
・家族写真サービス</p>
<p class="captionTxt">※ 兄妹2人以上が主役となる撮影の場合は、必ずスタンダード枠をお選びください。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_standard.png" alt="Set Product"></p>
</div>
<h4 class="setTit">スタンダード枠で選べる3セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_d.png" alt="Dset"></p>
<p class="setBoxName">色んな表情や仕草を未来へ残す一冊<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>36,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 8カット ）</li> 
<li>ALLデータ（ 70〜80カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set04.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_e.png" alt="Eset"></p>
<p class="setBoxName">レイアウト自由なBOOKが新登場♪<span>［ 基本セット ＋ インデックスブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>43,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 14カット ）</li> 
<li>ALLデータ（ 70〜80カット ）</li> 
<li>2L3面台紙 or 2Lパネル</li>
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set05.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_f.png" alt="Fset"></p>
<p class="setBoxName">撮影メイキングも収録した特別な一冊<span>［ 基本セット ＋ ムックアルバム ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>53,000</p>
<ul>
<li>MOOKALBUM（ 20カット ）</li>
<li>ALLデータ（ 70〜80カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">特別な日の記念撮影を、見守る両親の姿や、撮影中の舞台裏で起こっていた数々のメイキングシーンも合わせて撮影し、1冊のアルバムに収録します。<br>
ページをめくる度に思い出が蘇る、未来に残して欲しい1冊です。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set06.jpg" width="546" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

</div>
</section><!--planArea-->

<section class="galleryBox">
<h3 class="menuTit"><span>PHOTO GALLERY</span></h3>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_gallery.jpg" alt="<?php echo $title;?>"></p>
</section>


</section><!--menuArea-->

<section class="reserveBottom">
<div class="reserveCircle">
<p><img src="/common/img/share/bg_reserve.png" alt="ご予約はWebから"></p>
</div>
<div class="txt">ミスタームックのご予約がwebから出来るようになりました！
<a href="/reserve/" class="yellowBtn"><span>撮影のご予約はこちら</span></a>
</div>
</section>

</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
