
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>
<article>
<section id="<?php echo $slag; ?>">
<div id="innerTit"
<?php if($_SESSION['member_name']!="") { echo ' class="login"';}?>><h1><span>ENTRANCE CEREMONY</span><?php echo $title;?><img src="/common/img/menu/index/bg_tit.png"></h1></div>
<p class="mainImg"><img src="/common/img/menu/<?php echo $slag; ?>/img_main.jpg" alt="<?php echo $title;?>"></p>

<section class="pageTop">
<h2 class="mainTxt">小さかったきみが、初めてひとりで登校して行った日のことを</h2>
<p class="subTxt">ランドセルを背負った、わが子の姿を初めて見た時の記憶<br>
初めてひとりで登校して行った朝、小さい背中をどきどきしながら見送ったこと<br>
新しい第一歩を踏み出したばかりの、特別な記念日</p>
</section>

<section>
<h2 class="menuTit">入園/入学フォト</h2>
<div class="subBox">
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_sub.jpg" alt="<?php echo $title;?>"></p>
<div class="txt">
<p>ランドセルがやって来た日は嬉しくて、小さな背中に背負っては嬉しそうにはしゃぐ姿。ランドセルの中にはピカピカのノートや教科書、それとお友だちにあげるんだよと言って大切に包んだ謎の折り紙も詰め込んで、登校する日を心待ちにしてる。<br>
初めて保育園に入った時、離れるのいやだよーと泣いていた我が子が、小さな背中にランドセルを背負って、初めてひとりで旅立つ特別な日のことを、記憶の中と写真の中に記録する。</p>
制服フォトにあわせてお撮りする、自然な表情の「今」を残すカジュアルフォトも人気のプランです。
</div>
</div><!--subBox-->
<section class="sceneBox">
<h3 class="tit">スタジオ撮影シーン</h3>
<p class="img"><img src="/common/img/menu/share/img_ura.png" alt="公開ウラ舞台"></p>
<ul>
<li><img src="/common/img/menu/<?php echo $slag; ?>/img_scene01.jpg" alt="<?php echo $title;?>スタジオ撮影シーン公開ウラ舞台"></li>
<li><img src="/common/img/menu/<?php echo $slag; ?>/img_scene02.jpg" alt="<?php echo $title;?>スタジオ撮影シーン公開ウラ舞台"></li>
</ul>
</section><!--sceneBox-->

<section class="planArea">
<div class="planAreaIn">
<h3 class="planTit"><img src="/common/img/menu/share/tit_plan.png" alt="選べるプランをチェック"></h3>
<p class="planTxt"><?php echo $title;?>の撮影は、<br class="sp"><span class="short">ショート枠</span> or <span class="standard">スタンダード枠</span> の<br>2つの撮影枠のどちらかからお選び頂けます。</p>

<section class="planBox" id="short">
<h4 class="planBoxTit"><span>ショート枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間20分、1シーンまでのショートプラン<br>
撮影後の写真セレクトもスタジオにおまかせ♪</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日3枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「ショート」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>10:00〜11:00</li>
<li>12:00〜13:00</li>
<li>14:30〜15:30</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>ショート枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・アクセサリーや帽子・小物一式<br>
・家族写真サービス</p>
<p class="captionTxt">※ 衣装はセットの中に含まず、その分お安くご案内しております。<br>
1. 入学式でご用意した制服またはフォーマル着等をご持参下さい。（お靴やランドセルをお忘れにならないように^^）<br>
※ ご機嫌による時間延長等はありません。その瞬間の成長の思い出としてお撮り致します。人見知りなどが心配な方は、スタンダード枠をお選び頂くことをおすすめ致します。<br>
※ 兄妹2人以上が主役となる撮影の場合は、必ずスタンダード枠をお選び下さい。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_short.png" alt="Set Product"></p>
</div>
<h4 class="setTit">ショート枠で選べる3セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_a.png" alt="Aset"></p>
<p class="setBoxName">シンプルで可愛いらしい3面台紙<span>［ 基本セット ＋ 台紙 ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>15,000</p>
<ul>
<li>2L3面台紙</li> 
<li>収録データ（ 3カット ）</li> 
<li>デザインポストカード（ 2枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set01.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_b.png" alt="Bset"></p>
<p class="setBoxName">オリジナルデザイン台紙と六切り<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>25,000</p>
<ul>
<li>MOOKBOOK2面Mサイズ（ 5カット ）</li> 
<li>収録データ（ 2カット ）</li> 
<li>六切りプリント（ 1枚 ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set02.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_c.png" alt="Cset"></p>
<p class="setBoxName">飾るをテーマにアートな1枚を創る<span>［ 基本セット ＋ パネル ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>32,000</p>
<ul>
<li>マグネットフォトパネル</li>
<li>ALLデータ（ 5カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">少しづつ自立して行く姿も毎日一緒に過ごしていると見逃しがちで、歳が大きくなるとなおさら成長を感じることが少なくなっていくから、写真に残すことで大きくなった姿が実感できる、そんなフォトパネルをお作り致します。撮影中にお子様に手書きでご自分の名前を書いて頂き、書ける様になった初めの頃の字を記念にお入れしてデザイン致します。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set03.jpg" width="470" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

<section class="planBox" id="standard">
<h4 class="planBoxTit"><span>スタンダード枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間は40分、制服での撮影後、カジュアルな洋服に着替えて、フォトスタイリングな撮影を行います。<br>
少し大人に近づいた表情や自然な姿を、雑誌の1ページのようなスタイリングで撮影致します。</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日2枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「スタンダード」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>9:30〜12:30</li>
<li>14:00〜17:00</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>スタンダード枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・アクセサリーや帽子・小物一式<br>
・家族写真サービス</p>
<p class="captionTxt">※ 衣装はセットの中に含まず、その分お安くご案内しております。<br>
1. 入学式でご用意した制服またはフォーマル着等をご持参下さい。（お靴やランドセルをお忘れにならないように^^）<br>
2.カジュアル服のご用意もお願い致します。ジーパンに白シャツだけでも格好いいですし、お気にいりの洋服でもOKです。<br>
※ 兄妹2人以上が主役となる撮影の場合は、必ずスタンダード枠をお選び下さい。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_standard.png" alt="Set Product"></p>
</div>
<h4 class="setTit">スタンダード枠で選べる3セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_d.png" alt="Aset"></p>
<p class="setBoxName">色んな表情や仕草を未来へ残す一冊<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>33,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 8カット ）</li> 
<li>ALLデータ（ 40〜50カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set04.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_e.png" alt="Bset"></p>
<p class="setBoxName">レイアウト自由なBOOKが新登場♪<span>［ 基本セット ＋ インデックスブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>38,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 14カット ）</li> 
<li>ALLデータ（ 40〜50カット ）</li> 
<li>2L3面台紙 or 2Lパネル</li>
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set05.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_f.png" alt="Cset"></p>
<p class="setBoxName">撮影メイキングも収録した特別な一冊<span>［ 基本セット ＋ ムックアルバム ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>48,000</p>
<ul>
<li>MOOKALBUM（ 20カット ）</li>
<li>ALLデータ（ 40〜50カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">特別な日の記念撮影を、見守る両親の姿や、撮影中の舞台裏で起こっていた数々のメイキングシーンも合わせて撮影し、1冊のアルバムに収録します。<br>
ページをめくる度に思い出が蘇る、未来に残して欲しい1冊です。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set06.jpg" width="546" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

</div>
</section><!--planArea-->

<section class="galleryBox">
<h3 class="menuTit"><span>PHOTO GALLERY</span></h3>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_gallery.jpg" alt="<?php echo $title;?>"></p>
</section>


</section><!--menuArea-->

<section class="reserveBottom">
<div class="reserveCircle">
<p><img src="/common/img/share/bg_reserve.png" alt="ご予約はWebから"></p>
</div>
<div class="txt">ミスタームックのご予約がwebから出来るようになりました！
<a href="/reserve/" class="yellowBtn"><span>撮影のご予約はこちら</span></a>
</div>
</section>

</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
