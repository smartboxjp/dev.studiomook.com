
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>
<article>
<section id="<?php echo $slag; ?>">
<div id="innerTit"
<?php if($_SESSION['member_name']!="") { echo ' class="login"';}?>><h1><span>753 CELEBRATION</span><?php echo $title;?><img src="/common/img/menu/index/bg_tit.png"></h1></div>
<p class="mainImg"><img src="/common/img/menu/<?php echo $slag; ?>/img_main.jpg" alt="<?php echo $title;?>"></p>

<section class="pageTop">
<h2 class="mainTxt">3歳・5歳・7歳<br>それぞれの年齢の成長を祝って</h2>
<p class="subTxt">昔から伝わる伝統行事が、今なお大切に受け継がれている七五三祝い<br>
まだ幼くてもしっかりと着物を着て頑張る姿が可愛い3歳、好奇心が溢れる5歳、<br>
時々大人っぽい表情をみせる7歳、それぞれの年齢が魅せる成長や個性</p>
</section>

<section>
<h2 class="menuTit">七五三フォト</h2>
<div class="subBox">
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_sub.jpg" alt="<?php echo $title;?>"></p>
<div class="txt">
<p>かつて自分が子どもだった時、両親が残してくれた写真の中で、幼い自分が着物を着て背中をまっすぐに立ちながら、半べその顔をしていたり、照れ笑いしていたり。</p>
履きなれない草履で足が痛くなってしまったことや、帯が苦しくて泣いてしまったこと、写真は「今」を残しておけるからこそ、そんなエピソードまでも残すことができたなら、今日撮った一枚の写真が将来どんな「糸」となって繋がるだろう。<br>
七五三のお祝いを迎えた、それぞれの年齢が魅せる成長や個性。「あの時はこうだったね〜」と思い出を振り返る時、今と過去を結びつけてくれる一枚でありますように
</div>
</div><!--subBox-->
<section class="sceneBox">
<h3 class="tit">スタジオ撮影シーン</h3>
<p class="img"><img src="/common/img/menu/share/img_ura.png" alt="公開ウラ舞台"></p>
<ul>
<li><img src="/common/img/menu/<?php echo $slag; ?>/img_scene01.jpg" alt="<?php echo $title;?>スタジオ撮影シーン公開ウラ舞台"></li>
<li><img src="/common/img/menu/<?php echo $slag; ?>/img_scene02.jpg" alt="<?php echo $title;?>スタジオ撮影シーン公開ウラ舞台"></li>
</ul>
</section><!--sceneBox-->

<section class="planArea">
<div class="planAreaIn">
<h3 class="planTit"><img src="/common/img/menu/share/tit_plan.png" alt="選べるプランをチェック"></h3>
<p class="planTxt"><?php echo $title;?>の撮影は、着物とフォーマル衣装に着替えて撮影していきます。<br>
<span class="standard">スタンダード枠</span> の撮影枠のみのご案内となっております。</p>

<section class="planBox" id="standard">
<h4 class="planBoxTit"><span>スタンダード枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間は40分、着物とフォーマル衣装にお着替えして撮影するスタンダードプラン<br>
これまでの撮影スタイルと同じ、お子さまに合わせてゆったりとお撮りする撮影です。</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日2枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「スタンダード」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>9:30〜12:30</li>
<li>14:00〜17:00</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>スタンダード枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・和装小物・洋装アクセサリーや帽子<br>
・家族写真サービス</p>
<h5 class="captionTit"><span>七五三お支度代</span></h5>
<p>店内撮影用の貸衣装のお着物は、昔ながらの古典柄とレトロモダンな柄からお選びいただけます。<br>
お持込みも可能です♪（ 着付けのみの場合は、衣装代0円・着付け代1,500円でお受けしています。撮影当日までにお子様の着丈に合わせて肩上げをお済ませください ）</p>
<table>
<thead>
<tr>
<th></th>
<th>衣装・着付</th>
<th>ヘアメイク</th>
</tr>
</thead>
<tbody>
<tr>
<th>三歳男児</th>
<td>¥4,000</td>
<td>¥1,000</td>
</tr>
<tr>
<th>三歳女児</th>
<td>¥4,000</td>
<td>¥2,000</td>
</tr>
<tr>
<th>五歳男児</th>
<td>¥5,000</td>
<td>¥1,000</td>
</tr>
<tr>
<th>七歳女児</th>
<td>¥6,000</td>
<td>¥2,000</td>
</tr>
</tbody>
</table>
</div>
</section><!--captionBox-->
<section class="setBox inTxt">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_standard.png" alt="Set Product"></p>
</div>
<h4 class="setTit">スタンダード枠で選べる3セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_a.png" alt="Aset"></p>
<p class="setBoxName">色んな表情や仕草を未来へ残す一冊<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>32,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 8カット ）</li> 
<li>ALLデータ（ 40〜50カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set01.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_b.png" alt="Bset"></p>
<p class="setBoxName">レイアウト自由なBOOKが新登場♪<span>［ 基本セット ＋ インデックスブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>36,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 14カット ）</li> 
<li>ALLデータ（ 40〜50カット ）</li> 
<li>2L3面台紙 or 2Lパネル</li>
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set02.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_c.png" alt="Cset"></p>
<p class="setBoxName">撮影メイキングも収録した特別な一冊<span>［ 基本セット ＋ ムックアルバム ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>46,000</p>
<ul>
<li>MOOKALBUM（ 20カット ）</li>
<li>ALLデータ（ 40〜50カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">特別な日の記念撮影を、見守る両親の姿や、撮影中の舞台裏で起こっていた数々のメイキングシーンも合わせて撮影し、1冊のアルバムに収録します。<br>
ページをめくる度に思い出が蘇る、未来に残して欲しい1冊です。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set03.jpg" width="546" alt="<?php echo $title;?>"></p>
</div>
</div>
<p class="bottomTxt">※ セットの料金にお支度代は含まれておりません</p>
</section><!--setBox-->
</section><!--planBox-->

</div>
</section><!--planArea-->

<section class="galleryBox">
<h3 class="menuTit"><span>PHOTO GALLERY</span></h3>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_gallery.jpg" alt="<?php echo $title;?>"></p>
</section>


</section><!--menuArea-->

<section class="reserveBottom">
<div class="reserveCircle">
<p><img src="/common/img/share/bg_reserve.png" alt="ご予約はWebから"></p>
</div>
<div class="txt">ミスタームックのご予約がwebから出来るようになりました！
<a href="/reserve/" class="yellowBtn"><span>撮影のご予約はこちら</span></a>
</div>
</section>

</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
