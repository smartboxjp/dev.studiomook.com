
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>
<article>
<section id="<?php echo $slag; ?>">
<div id="innerTit"
<?php if($_SESSION['member_name']!="") { echo ' class="login"';}?>><h1><span>FAMILY PHOTO</span><?php echo $title;?><img src="/common/img/menu/index/bg_tit.png"></h1></div>
<p class="mainImg"><img src="/common/img/menu/<?php echo $slag; ?>/img_main.jpg" alt="<?php echo $title;?>"></p>

<section class="pageTop">
<h2 class="mainTxt">過去と今を繋ぐ、今と未来を繋ぐ</h2>
<p class="subTxt">時を越えて思い出す出来事の中に、<br>
家族や兄妹と、こんな風に接してたんだって分かる<br>
一枚があったらきっと楽しい</p>
</section>

<section>
<h2 class="menuTit" id="familyPhoto">ファミリーフォト</h2>
<div class="subBox">
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_sub01.jpg" alt="<?php echo $title;?>"></p>
<div class="txt">
毎年一枚、家族写真を撮りにスタジオに来てくれるお客様が持っていたアルバム。ページをめくっていくとその一冊の中には家族の歴史が1年1年刻まれていて、結婚してひとりがふたりになった日の照れくさそうな表情、ママになってからの表情、ふたりが３人になって、赤ちゃんだったお子さんが毎年大きく成長していく姿....そしてまた今年のストーリーを綴っていく。<br>
そのアルバムを見た時に「ああ、いいなあ」と、改めて写真の持つ力について考えさせられました。<br>
Mr.MOOKのすべての撮影メニューには、「家族写真」も一緒にお付けすることが出来ます。人生の節目節目に訪れる、家族の誰かの大切なお祝い事。そんな日は「家族写真」も一緒に記念に残しませんか？<br>
家族写真だけの撮影日をご希望のお客様は、定期的に開催される「家族写真の日キャンペーン」をご利用頂くか、またはお電話にてご相談ください^^
</div>
</div><!--subBox-->
<h2 class="menuTit" id="brotherPhoto">兄弟フォト</h2>
<div class="subBox">
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_sub02.jpg" alt="<?php echo $title;?>"></p>
<div class="txt">
子ども時代の仲良しショットを可愛らしく記念に残す、兄妹写真はとても人気の撮影です。<br>
ただ、お子さん同士だけの撮影ってじつは１番大変！<br>
「ショート枠」では、家族写真を撮ったあとに、お時間があまった場合は撮影できます♪「スタンダード枠」も同様ですが、こちらの枠の方がお時間がたっぷりありますので、お子さまの様子を見ながらトライ出来ます。<br>
兄妹写真をご希望のお客様は、撮影前の練習が肝です♪特に、まだまだ小さい年齢のお子さま同士だとなかなかカメラの前で仲良く座るというのが難しいお年頃...。撮影日が近づいたらぜひご家庭で、上のお子さんが抱っこしてあげる練習や、仲良くぎゅーの練習をして来て頂くと、撮影中とってもかわいいベストショットに出会えたりしておすすめです♪
</div>
</div><!--subBox-->

<section class="familyArea">
<div class="familyAreaIn">
<h3 class="tit">兄妹2人以上が主役となる撮影の場合</h3>
<p class="txt">ご兄弟どちらとも記念日が重なって、せっかくだから一緒に！とお考えのご家族の為に、お得なプランをご用意しました♪<br>
通常の1枠の時間を、2人でシェアする形で撮影を行っていきますので、ご予約の際は必ず「スタンダード枠」をお選びください。<br>
主役ひとりひとりのソロショットを交代でお撮りしたあと、ご希望で兄妹写真も撮影致します^^</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_sub03.jpg" alt="<?php echo $title;?>"></p>
</div>
</section><!--familyAra-->

<section class="planArea">
<div class="planAreaIn">
<h3 class="planTit"><img src="/common/img/menu/share/tit_plan.png" alt="選べるプランをチェック"></h3>
<p class="planTxt">主役が2人の撮影の場合、<br class="sp"><span class="pinkBar">1人1冊づつ</span> or <span class="pinkBar">2人分まとめて1冊に</span> の<br class="sp">どちらかお得なセットからお選び頂けます。</p>

<section class="planBox" id="standard">
<h4 class="planBoxTit"><span>主役が2人のお得なセットプラン</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間は40分。主役ひとりひとりのソロショットを交代でお撮りしたあと、ご希望で兄妹写真も撮影致します。<br>
1枠の時間枠を2人でシェアする形になりますので、基本的には下のお子さまから撮影して行きますが、ご機嫌によっては順番を変えながら進めていきます。</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>兄妹2人以上が主役となる撮影の場合は、必ず「スタンダード枠」をお選びください。<br>
予約カレンダーの「スタンダード」の日を選択し、ご都合のいいお時間枠をお選びください。<br>
1日2枠の中から、ご希望のお時間をお選び頂けます。</p>
<ul>
<li>9:30〜12:30</li>
<li>14:00〜17:00</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>兄弟セットに含まれるもの</span></h5>
<p class="bold">・それぞれの撮影代<br>
・アクセサリーや帽子・小物一式<br>
・家族写真/兄妹写真サービス<br>
・衣装それぞれ2着ずつ</p>
<p class="captionTxt">※ 七五三で着物レンタルの場合は別途お支度代がかかります。
お支度代に関しましては<a href="/menu/celebration753.php" class="cBlue" target="_blank">七五三のページ</a>にてご確認ください。</p>
<p class="captionTxt">その場合、下記の各セットプランの料金から、七五三のお子様の人数×2,000円の割引サービスも行っています。<br>
【 例 】七五三3才男の子と百日記念の撮影<br>
Aセット<br>
￥38,000( ←ここから￥2,000割引 )＋お支度代￥4,000<br>
合計￥40,000</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_standard.png" alt="Set Product"></p>
</div>
<h4 class="setTit">主役が2人プラン♪選べるお得な3セット</h4>
<div class="setBoxInner">
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_a.png" alt="Aset"></p>
<p class="setBoxName">データONLY！シンプルプラン<span>［ 基本セット ＋  データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>38,000</p>
<ul>
<li>ALLデータ（ 70〜80カット ）</li> 
<li>デザインポストカード（ 5枚 ）</li>
</ul>
</div>
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_b.png" alt="Bset"></p>
<p class="setBoxName">1冊に2人分！オリジナル3面台紙<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>50,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 12カット ）</li>
<li>ALLデータ（ 70〜80カット ）</li>
<li>デザインポストカード（ 5枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set01.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_c.png" alt="Cset"></p>
<p class="setBoxName">1冊に2人分！12ページアルバム<span>［ 基本セット ＋ ムックアルバム ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>60,000</p>
<ul>
<li>MOOKALBUM12P（ 20カット ）</li>
<li>ALLデータ（ 70〜80カット ）</li>
<li>デザインポストカード（ 5枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set02.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_d.png" alt="Dset"></p>
<p class="setBoxName">1人1冊すつ！オリジナル2面台紙<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>56,000</p>
<ul>
<li>MOOKBOOK2面Mサイズ（ 5カット ）</li>
<li>MOOKBOOK2面Mサイズ（ 5カット ）</li>
<li>ALLデータ（ 70〜80カット ）</li>
<li>デザインポストカード（ 5枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set03.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_e.png" alt="Eset"></p>
<p class="setBoxName">1人1冊すつ！オリジナル3面台紙<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>62,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 8カット ）</li>
<li>MOOKBOOK3面Mサイズ（ 8カット ）</li>
<li>ALLデータ（ 70〜80カット ）</li>
<li>デザインポストカード（ 5枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set04.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

</div>
</section><!--planArea-->

<section class="galleryBox">
<h3 class="menuTit"><span>PHOTO GALLERY</span></h3>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_gallery.jpg" alt="<?php echo $title;?>"></p>
</section>


</section><!--menuArea-->

<section class="reserveBottom">
<div class="reserveCircle">
<p><img src="/common/img/share/bg_reserve.png" alt="ご予約はWebから"></p>
</div>
<div class="txt">ミスタームックのご予約がwebから出来るようになりました！
<a href="/reserve/" class="yellowBtn"><span>撮影のご予約はこちら</span></a>
</div>
</section>

</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
