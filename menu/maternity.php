
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>
<article>
<section id="<?php echo $slag; ?>">
<div id="innerTit"
<?php if($_SESSION['member_name']!="") { echo ' class="login"';}?>><h1><span>MATERNITY</span><?php echo $title;?><img src="/common/img/menu/index/bg_tit.png"></h1></div>
<p class="mainImg"><img src="/common/img/menu/<?php echo $slag; ?>/img_main.jpg" alt="<?php echo $title;?>"></p>

<section class="pageTop">
<h2 class="mainTxt">生まれる前から始まっていたStoryを、いつかきみに伝えたい</h2>
<p class="subTxt">これから出会う赤ちゃんとの最初の一枚<br>
会える日のことを思うと、戸惑いながらも楽しみで幸せな時間<br>
一緒だった奇跡の時間の事を、大きくなったきみといつか話せる日を楽しみに</p>
</section>

<section>
<h2 class="menuTit">マタニティフォト</h2>
<div class="subBox">
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_sub.jpg" alt="<?php echo $title;?>"></p>
<div class="txt">
<p>飾りすぎないマタニティフォト。<br>
私達は撮影する時、いつもその写真が将来どんなシーンで見られているんだろうと想像します。マタニティフォトをお撮りする時、だんだん丸くなってゆくお腹とともに、おなかの中ですくすくと育っていく生命の、神秘的で奇跡のような瞬間を、その存在を1番近くに感じているママの想いや、楽しみに待っている家族の想いも一緒に、生まれてきた赤ちゃんに残してあげられる様に、「家族らしさ」を大切に撮影したいと思っています。</p>
基本的には衣装はご持参頂くスタイルです。撮影をご検討のお客様は、ご予約前にぜひお電話で色々とご相談下さい。
</div>
</div><!--subBox-->

<section class="planArea">
<div class="planAreaIn">
<h3 class="planTit"><img src="/common/img/menu/share/tit_plan.png" alt="選べるプランをチェック"></h3>
<p class="planTxt"><?php echo $title;?>の撮影は、<br class="sp"><span class="short">ショート枠</span> or <span class="standard">スタンダード枠</span> の<br class="sp">2つの撮影枠のどちらかからお選び頂けます。</p>

<section class="planBox" id="short">
<h4 class="planBoxTit"><span>ショート枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間20分、1シーンまでのショートプラン<br>
撮影後の写真セレクトもスタジオにおまかせ♪</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日3枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「ショート」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>10:00〜11:00</li>
<li>12:00〜13:00</li>
<li>14:30〜15:30</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>ショート枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・アクセサリーや帽子・小物一式<br>
・家族写真サービス</p>
<p class="captionTxt">※ 衣装はセットの中に含まず、その分お安くご案内しております。<br>
1. 衣装はスタジオにも数点ありますが、Mサイズのお腹の見えないセパレートなしタイプですので、なるべくご自身でご持参頂いております。<br>
2. 旦那様やお子さまとの撮影も可能です。なるべく白色など色味の少ないお洋服を揃えてきて頂くと、おなかに色移りせずよりきれいに仕上がります。<br>
※ お2人以上が主役となる撮影の場合は、必ずスタンダード枠をお選び下さい。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_short.png" alt="Set Product"></p>
</div>
<h4 class="setTit">ショート枠で選べる3セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_a.png" alt="Aset"></p>
<p class="setBoxName">シンプルで可愛いらしい3面台紙<span>［ 基本セット ＋ 台紙 ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>15,000</p>
<ul>
<li>2L3面台紙</li> 
<li>収録データ（ 3カット ）</li> 
<li>デザインポストカード（ 2枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set01.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_b.png" alt="Bset"></p>
<p class="setBoxName">オリジナルデザイン台紙と六切り<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>25,000</p>
<ul>
<li>MOOKBOOK2面Mサイズ（ 5カット ）</li> 
<li>収録データ（ 5カット ）</li> 
<li>六切りプリント（ 1枚 ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set02.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_c.png" alt="Cset"></p>
<p class="setBoxName">飾るをテーマにアートな1枚を創る<span>［ 基本セット ＋ パネル ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>32,000</p>
<ul>
<li>２回分の撮影チケット</li>
<li>デザインパネル</li>
<li>ALLデータ（ 5〜10カット ）</li> 
<li>デザイン2Lフォトプリント（ 1枚 ）</li>
</ul>
<p class="setBoxSubTxt">撮影を2回に分けて行い、マタニティフォトとBABY誕生フォトを一枚のパネルに仕上げます。<br>
将来のお子さまへの贈り物として、メッセージ付きフォトプリントもデザインしてお作り致します。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set03.jpg" width="450" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

<section class="planBox" id="standard">
<h4 class="planBoxTit"><span>スタンダード枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間は40分。ゆっくりと撮影を楽しめるスタンダードプランです。<br>兄妹が増えるのを心待ちにしている上のお子様を主役に、おなかの赤ちゃんとのコミュニケーションシーンもおすすめです。</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日2枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「スタンダード」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>9:30〜12:30</li>
<li>14:00〜17:00</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>スタンダード枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・アクセサリーや帽子・小物一式<br>
・家族写真サービス</p>
<p class="captionTxt">※ 衣装はセットの中に含まず、その分お安くご案内しております。<br>
1. 衣装はスタジオにも数点ありますが、Mサイズのお腹の見えないセパレートなしタイプですので、なるべくご自身でご持参頂いております。<br>
2. 旦那様やお子さまとの撮影も可能です。なるべく白色など色味の少ないお洋服を揃えてきて頂くと、おなかに色移りせずよりきれいに仕上がります。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_standard.png" alt="Set Product"></p>
</div>
<h4 class="setTit">スタンダード枠で選べる3セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_d.png" alt="Aset"></p>
<p class="setBoxName">色んな表情や仕草を未来へ残す一冊<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>33,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 8カット ）</li> 
<li>ALLデータ（ 40〜50カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set04.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_e.png" alt="Bset"></p>
<p class="setBoxName">レイアウト自由なBOOKが新登場♪<span>［ 基本セット ＋ インデックスブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>38,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 14カット ）</li> 
<li>ALLデータ（ 40〜50カット ）</li> 
<li>2L3面台紙 or 2Lパネル</li>
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set05.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_f.png" alt="Cset"></p>
<p class="setBoxName">撮影メイキングも収録した特別な一冊<span>［ 基本セット ＋ ムックアルバム ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>48,000</p>
<ul>
<li>MOOKALBUM（ 20カット ）</li>
<li>ALLデータ（ 40〜50カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">特別な日の記念撮影当日、撮影中の舞台裏で起こっていた数々のメイキングシーンも合わせて撮影し、1冊のアルバムに収録します。<br>
ページをめくる度に思い出が蘇る、未来に残して欲しい1冊です。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set06.jpg" width="546" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

</div>
</section><!--planArea-->

<section class="galleryBox">
<h3 class="menuTit"><span>PHOTO GALLERY</span></h3>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_gallery.jpg" alt="<?php echo $title;?>"></p>
</section>


</section><!--menuArea-->

<section class="reserveBottom">
<div class="reserveCircle">
<p><img src="/common/img/share/bg_reserve.png" alt="ご予約はWebから"></p>
</div>
<div class="txt">ミスタームックのご予約がwebから出来るようになりました！
<a href="/reserve/" class="yellowBtn"><span>撮影のご予約はこちら</span></a>
</div>
</section>

</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
