
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>
<article>
<section id="<?php echo $slag; ?>">
<div id="innerTit"
<?php if($_SESSION['member_name']!="") { echo ' class="login"';}?>><h1><span>SENIOR PHOTOGRAPHY</span><?php echo $title;?><img src="/common/img/menu/index/bg_tit.png"></h1></div>
<p class="mainImg"><img src="/common/img/menu/<?php echo $slag; ?>/img_main.jpg" alt="<?php echo $title;?>"></p>

<section class="pageTop">
<h2 class="mainTxt">あなたがいてくれたおかげで、私達もここにいる</h2>
<p class="subTxt">六十年生きて、干支が生まれた年に戻る「 還暦 」<br>
そしてその先の長寿をお祝いする<br>
七十七歳の「 喜寿 」、八十八歳の「 米寿 」</p>
</section>

<section>
<h2 class="menuTit"><span>還暦/喜寿/米寿フォト</span></h2>
<div class="subBox">
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_sub.jpg" alt="<?php echo $title;?>"></p>
<div class="txt">
<p>還暦のお祝いのお問合せを頂く時、大体の方がご本人ではなくその家族の誰かだったり、娘さん達からだったりします。<br>
年を重ねて、我が子も大きくなると、一緒に写真を撮ることが減ったり、特に私達の世代の様に写真を撮る習慣がなかった高齢の世代の方々にとって、自分で自分のお祝い写真を撮ることって少ないのかもしれません。<br>
親になって初めて子育ての大変さに気づいた時、両親がどんな気持ちで自分との時間を過ごしていたのか、自分が年を重ねた時、親もまた年を重ねていくのだと、自身の老いを持ってまた沢山のことを学ばせてくれる、かけがえのない偉大な存在｡<br>
だから「ありがとう」を沢山詰め込んだ、親しい方からの祝福は、やはり嬉しいものだと思うのです。
</div>
</div><!--subBox-->

<section class="planArea">
<div class="planAreaIn">
<h3 class="planTit"><img src="/common/img/menu/share/tit_plan.png" alt="選べるプランをチェック"></h3>
<p class="planTxt"><?php echo $title;?>の撮影は、<br class="sp"><span class="short">ショート枠</span> or <span class="standard">スタンダード枠</span> の<br>2つの撮影枠のどちらかからお選び頂けます。</p>

<section class="planBox" id="short">
<h4 class="planBoxTit"><span>ショート枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間20分。ご本人のお一人カットorご夫婦2ショット、家族みんなで集合写真1カットのショートプランです。</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日3枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「ショート」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>10:00〜11:00</li>
<li>12:00〜13:00</li>
<li>14:30〜15:30</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>ショート枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・家族写真サービス</p>
<p class="captionTxt">※ 衣装はセットの中に含まず、その分お安くご案内しております。<br>
ご家族揃っての撮影も可能です。赤いちゃんちゃんこ等のご用意はありませんのでご希望でご持参下さい。皆様でお洋服を揃えてきて頂いたり、手作りのTシャツをプレゼントしたり、前もって準備をして頂くと、お写真全体の雰囲気がより素敵に仕上がります。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_short.png" alt="Set Product"></p>
</div>
<h4 class="setTit">ショート枠で選べるセット</h4>
<div class="setBoxInner">
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_a.png" alt="Aset"></p>
<p class="setBoxName">台紙に記録するお祝いの日<span>［ 基本セット ＋ 台紙 ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>16,000</p>
<ul>
<li>四つ切2面台紙</li>
<li>ALLデータ（ 5〜10カット ）</li> 
</ul>
<p class="setBoxSubTxt">生成りで製本された2面台紙は、ぬくもりのある優しい風合いです。右と左に一枚ずつお写真が入りますので、ご本人の表情違いのお写真を組合せたり、家族写真を片方に合わせてプレゼントしたり、自由に組合せることが出来ます。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set01.jpg" width="465" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

<section class="planBox" id="standard">
<h4 class="planBoxTit"><span>スタンダード枠</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間は40分。ゆっくりと撮影を楽しめるスタンダードプランです。お孫さん達に囲まれてにこやかに遊ぶ風景をお撮りするナチュラル撮影もおすすめです。</p>
<h5 class="captionTit"><span>タイムスケジュール</span></h5>
<p>1日2枠の中から、ご希望のお時間をお選び頂けます。<br>
予約カレンダーの「スタンダード」の日を選択し、ご都合のいいお時間枠をお選びください。</p>
<ul>
<li>9:30〜12:30</li>
<li>14:00〜17:00</li>
</ul>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>スタンダード枠のセットに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・家族写真サービス</p>
<p class="captionTxt">※ 衣装はセットの中に含まず、その分お安くご案内しております。<br>
ご家族揃ってやお孫さん達との撮影も可能です。赤いちゃんちゃんこ等のご用意はありませんのでご希望でご持参下さい。皆様でお洋服を揃えてきて頂いたり、手作りのTシャツをプレゼントしたり、前もって準備をして頂くと、お写真全体の雰囲気がより素敵に仕上がります。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_standard.png" alt="Set Product"></p>
</div>
<h4 class="setTit">スタンダード枠で選べるセット</h4>
<div class="setBoxInner">
<div class="setBox03">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_b.png" alt="bset"></p>
<p class="setBoxName">自然な表情も一緒に一冊の中に<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>33,000</p>
<ul>
<li>MOOKBOOK3面Mサイズ（ 8カット ）</li>
<li>ALLデータ（ 40〜50カット ）</li> 
<li>デザインポストカード（ 3枚 ）</li>
</ul>
<p class="setBoxSubTxt">特別な日の記念撮影だから、お孫さん達に囲まれてにこやかに遊ぶ風景をお撮りするナチュラル撮影もおすすめです。<br>
開く度に思い出が蘇る、家族の揃うリビングに飾って欲しい一冊です。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set02.jpg" width="465" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

</div>
</section><!--planArea-->

<section class="galleryBox">
<h3 class="menuTit"><span>PHOTO GALLERY</span></h3>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_gallery.jpg" alt="<?php echo $title;?>"></p>
</section>


</section><!--menuArea-->

<section class="reserveBottom">
<div class="reserveCircle">
<p><img src="/common/img/share/bg_reserve.png" alt="ご予約はWebから"></p>
</div>
<div class="txt">ミスタームックのご予約がwebから出来るようになりました！
<a href="/reserve/" class="yellowBtn"><span>撮影のご予約はこちら</span></a>
</div>
</section>

</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
