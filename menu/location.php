
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>
<article>
<section id="<?php echo $slag; ?>">
<div id="innerTit"
<?php if($_SESSION['member_name']!="") { echo ' class="login"';}?>><h1><span>LOCATION PHOTO</span><?php echo $title;?><img src="/common/img/menu/index/bg_tit.png"></h1></div>
<p class="mainImg"><img src="/common/img/menu/<?php echo $slag; ?>/img_main.jpg" alt="<?php echo $title;?>"></p>

<section class="pageTop">
<h2 class="mainTxt">This is True Story</h2>
<p class="subTxt">いつもの場所、過ごした時間、<br>
共に歩んだ場所でしか、伝えきれない物語り<br>
日常に溢れた数々の瞬間を</p>
</section>

<section>
<h2 class="menuTit"><span>ロケーションフォト</span></h2>
<div class="subBox">
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_sub.jpg" alt="<?php echo $title;?>"></p>
<div class="txt">
ロケーションフォトは、2つの撮影スタイルでご案内しています。ご自宅やお気に入りの場所で、いつもの風景や元気いっぱいに遊ぶ姿を撮影する「 日常撮影プラン 」。大切な記念日に同行して、晴れ姿や見守るご家族の風景を撮影する「 ハレの日同行プラン 」。<br>
スタジオの中だけじゃ伝えきれない思い出が、家族の数だけあるように、いつもの場所やお気にいりの場所で過ごす時間を切り取る事も、大切な「記念日」を主役だけじゃなく、まわりのみんながどんな風に過ごしていたのかを未来のきみに残していくことも、きっと「今」しか出来ないことだから。
</div>
</div><!--subBox-->

<section class="planArea">
<div class="planAreaIn">
<h3 class="planTit"><img src="/common/img/menu/share/tit_plan.png" alt="選べるプランをチェック"></h3>
<p class="planTxt"><?php echo $title;?>は、<br class="sp"><span class="dailyBar">日常撮影プラン</span> or <span class="fineBar">ハレの日同行プラン</span> の<br class="sp">2つの撮影枠のどちらかからお選び頂けます。</p>

<section class="planBox" id="daily">
<h4 class="planBoxTit"><span>日常撮影プラン</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間1時間。ご自宅や海、お気に入りの場所で自然な姿を撮影致します。ロケーション1コース。撮影場所の移動はありません♪<br>
・青い空の下で元気に遊んでいるシーン<br>
・自宅やお気に入りの場所での日常シーン<br>
・ロケーションでプロフィール写真をコーディネート</p>
<h5 class="captionTit"><span>予約方法</span></h5>
<p>出張撮影は現在「予約カレンダー」からのご予約はお受けしておりません。ご希望のお客様は、まずはお電話で詳細をお伝え頂き、ご相談をお受けしながら予約のご案内をさせて頂きますので、まずはお電話をお願い致します♪</p>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>このセットプランに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・出張費/1時間（延長1時間/￥5,000）<br>
・撮影用小物一式</p>
<p class="captionTxt"><span class="bold">交通費無料地域</span><br>
浦添市・宜野湾市・沖縄市・北谷町・嘉手納・中城・北中城・那覇市・与那原町・南風原町・西原町・豊見城・南城市・糸満市（それ以外は別途1,000円 ※離島の場合はお問合せ下さい）</p>
<p class="captionTxt">※ 出張場所に付きましては、撮影許可が必要な場所もございますので事前にご相談ください。有料施設の入場料や駐車料金が必要な場合は、カメラマン1人分のチケットのご用意をお願いしております。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_daily.png" alt="Set Product"></p>
</div>
<h4 class="setTit">日常撮影プランで選べる2セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_a.png" alt="Aset"></p>
<p class="setBoxName">Photoデータプラン<span>［ 基本セット ＋  データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>35,000</p>
<ul>
<li>ALLデータ（80〜100カット）</li>
</ul>
<p class="setBoxSubTxt">出張撮影とOKカットの全データがセットになったプランです。撮影後、お写真を綺麗に補整してお渡し致します。<br>
撮影内容に合わせて、かわいい小物等もセッティング致しますので、ご希望をご相談下さい^^</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set01.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_sh_b.png" alt="Bset"></p>
<p class="setBoxName">PhotoデータとBOOKプラン<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>48,000</p>
<ul>
<li>ALLデータ（80〜100カット）</li> 
<li>MOOKBOOK3面Mサイズ（ 10カット ）</li>
</ul>
<p class="setBoxSubTxt">出張撮影とOKカットの全データと合わせて、フォトブックがセットになったプランです。撮影時にサンプルBOOKをお持ち致しますので、実際に手にとってご覧ください。</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set02.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

<section class="planBox" id="fine">
<h4 class="planBoxTit"><span>ハレの日同行プラン</span></h4>
<section class="captionBox">
<div class="captionLeft">
<h5 class="captionTit"><span>撮影内容</span></h5>
<p>撮影時間1〜2時間。大切な記念日に同行して、晴れ姿や見守るご家族の風景を撮影致します。<br>
ご自宅での着付けなどのお支度風景を撮影してから、神社でのお参りシーンの撮影など、撮影場所を大きく移動しての撮影も可能ですので、事前にご相談下さい♪<br>
お宮参り・誕生日・タンカーユーエー・七五三・入園入学・卒業式・各種イベントスナップ</p>
<h5 class="captionTit"><span>予約方法</span></h5>
<p>出張撮影は現在「予約カレンダー」からのご予約はお受けしておりません。ご希望のお客様は、まずはお電話で詳細をお伝え頂き、ご相談をお受けしながら予約のご案内をさせて頂きますので、まずはお電話をお願い致します♪</p>
</div>
<div class="captionRight">
<h5 class="captionTit"><span>このセットプランに含まれるもの</span></h5>
<p class="bold">・撮影代<br>
・出張費/1時間（延長1時間/￥5,000）<br>
・撮影用小物一式</p>
<p class="captionTxt"><span class="bold">交通費無料地域</span><br>
浦添市・宜野湾市・沖縄市・北谷町・嘉手納・中城・北中城・那覇市・与那原町・南風原町・西原町・豊見城・南城市・糸満市（それ以外は別途1,000円 ※離島の場合はお問合せ下さい）</p>
<p class="captionTxt">※ 出張場所に付きましては、撮影許可が必要な場所もございますので事前にご相談ください。有料施設の入場料や駐車料金が必要な場合は、カメラマン1人分のチケットのご用意をお願いしております。<br>
※weddingフォトは別途お見積り致しますのでお電話でお問合せ下さい。</p>
</div>
</section><!--captionBox-->
<section class="setBox">
<div class="setCircle">
<p><img src="/common/img/menu/share/tit_set_fine.png" alt="Set Product"></p>
</div>
<h4 class="setTit">スタンダード枠で選べる3セット</h4>
<div class="setBoxInner">
<div class="setBox01">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_a.png" alt="Aset"></p>
<p class="setBoxName">Photoデータプラン<span>［ 基本セット ＋  データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>38,000</p>
<ul>
<li>ALLデータ（80〜100カット）</li>
</ul>
<p class="setBoxSubTxt">出張撮影とOKカットの全データがセットになったプランです。撮影後、お写真を綺麗に補整してお渡し致します。<br>
撮影内容に合わせて、かわいい小物等もセッティング致しますので、ご希望をご相談下さい^^</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set03.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
<div class="setBox02">
<p class="setBoxTit"><img src="/common/img/menu/share/tit_set_st_b.png" alt="Bset"></p>
<p class="setBoxName">PhotoデータとBOOKプラン<span>［ 基本セット ＋ ムックブック ＋ データ ］</span></p>
<p class="setBoxTxt"><span>¥</span>50,000</p>
<ul>
<li>ALLデータ（80〜100カット）</li> 
<li>MOOKBOOK3面Mサイズ（ 10カット ）</li>
</ul>
<p class="setBoxSubTxt">出張撮影とOKカットの全データと合わせて、フォトブックがセットになったプランです。撮影時にサンプルBOOKをお持ち致しますので、実際に手にとってご覧ください♪</p>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_set04.jpg" width="378" alt="<?php echo $title;?>"></p>
</div>
</div>
</section><!--setBox-->
</section><!--planBox-->

</div>
</section><!--planArea-->

<section class="galleryBox">
<h3 class="menuTit"><span>PHOTO GALLERY</span></h3>
<p class="img"><img src="/common/img/menu/<?php echo $slag; ?>/img_gallery.jpg" alt="<?php echo $title;?>"></p>
</section>


</section><!--menuArea-->

<section class="reserveBottom">
<div class="reserveCircle">
<p><img src="/common/img/share/bg_reserve.png" alt="ご予約はWebから"></p>
</div>
<div class="txt">ミスタームックのご予約がwebから出来るようになりました！
<a href="/reserve/" class="yellowBtn"><span>撮影のご予約はこちら</span></a>
</div>
</section>

</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
