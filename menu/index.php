<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<article>
<section>
<div id="innerTit"
<?php if($_SESSION['member_name']!="") { echo ' class="login"';}?>><h1><span>PHOTO MENU</span>撮影メニュー<img src="/common/img/menu/index/bg_tit.png"></h1></div>

<section class="pageTop">
<h2 class="mainTxt">その時、その瞬間の、”自然な表情・仕草”を大切に撮影する写真スタジオです</h2>
<p class="subTxt">一組一組大切に撮影したいという思いから、スタジオで過ごす時間も楽しんで頂くために、<br>
完全貸切での撮影をご案内しております。</p>
<div class="purpleBox">これまで1日2組限定で撮影をご案内していましたが、2017年3月のご予約より「ショート枠」と「スタンダード枠」がお選び頂けるようになり、撮影枠が増えました！<br>
これまでの料金体制も変更になり、撮影メニューごとにお得なセットをご用意致しましたので、ご希望の撮影メニューから詳細をご覧ください。</div>
</section>

<section class="timelineArea">
<h2 class="menuTit">ご予約の流れ</h2>
<div class="timelineBox">

<div class="timelineList">
<p class="img"><a href="newborn.php"><img src="/common/img/menu/index/img_newborn.png" alt="ニューボーン"></a></p>
<div class="txt">
<h3 class="timelineTit"><span>NewBorn</span>ニューボーン</h3>
<p class="standard">スタンダード</p>
</div>
</div><!--timelineList-->
<div class="timelineList">
<p class="img"><a href="100days.php"><img src="/common/img/menu/index/img_100days.png" alt="百日記念"></a></p>
<div class="txt">
<h3 class="timelineTit"><span>100days</span>百日記念</h3>
<p class="short">ショート</p>
<p class="standard">スタンダード</p>
</div>
</div><!--timelineList-->
<div class="timelineList">
<p class="img"><a href="halfbirthday.php"><img src="/common/img/menu/index/img_halfbirthday.png" alt="ハーフバースディ"></a></p>
<div class="txt">
<h3 class="timelineTit"><span>HalfBirthday</span>ハーフバースディ</h3>
<p class="short">ショート</p>
<p class="standard">スタンダード</p>
</div>
</div><!--timelineList-->
<div class="timelineList years01">
<p class="img"><a href="birthday.php"><img src="/common/img/menu/index/img_birthday.png" alt="バースディ"></a></p>
<div class="txt">
<h3 class="timelineTit"><span>Birthday</span>バースディ</h3>
<p class="short">ショート</p>
<p class="standard">スタンダード</p>
</div>
</div><!--timelineList-->
<div class="timelineList years03">
<p class="img"><a href="celebration753.php"><img src="/common/img/menu/index/img_celebration753_3.png" alt="七五三（3歳）男/女"></a></p>
<div class="txt">
<h3 class="timelineTit"><span>753celebration</span>七五三（3歳）男/女</h3>
<p class="standard">スタンダード</p>
</div>
</div><!--timelineList-->
<div class="timelineList">
<p class="img"><a href="celebration753.php"><img src="/common/img/menu/index/img_celebration753_5.png" alt="七五三（5歳）男"></a></p>
<div class="txt">
<h3 class="timelineTit"><span>753celebration</span>七五三（5歳）男</h3>
<p class="standard">スタンダード</p>
</div>
</div><!--timelineList-->
<div class="timelineList">
<p class="img"><a href="celebration753.php"><img src="/common/img/menu/index/img_celebration753_7.png" alt="七五三（7歳）女"></a></p>
<div class="txt">
<h3 class="timelineTit"><span>753celebration</span>七五三（7歳）女</h3>
<p class="standard">スタンダード</p>
</div>
</div><!--timelineList-->
<div class="timelineList">
<p class="img"><a href="entrance.php"><img src="/common/img/menu/index/img_entrance.png" alt="入園/卒園・入学/卒業"></a></p>
<div class="txt">
<h3 class="timelineTit"><span>EntranceCeremony</span>入園/卒園・入学/卒業</h3>
<p class="short">ショート</p>
<p class="standard">スタンダード</p>
</div>
</div><!--timelineList-->
<div class="timelineList years10">
<p class="img"><span><img src="/common/img/menu/index/img_13years.png" alt="十三祝い 男/女"></span></p>
<div class="txt">
<h3 class="timelineTit"><span>13yearsCeremony</span>十三祝い 男/女</h3>
</div>
</div><!--timelineList-->
<div class="timelineList years20">
<p class="img"><span><img src="/common/img/menu/index/img_20years.png" alt="成人式"></span></p>
<div class="txt">
<h3 class="timelineTit"><span>2OyearsCeremony</span>成人式</h3>
</div>
</div><!--timelineList-->
<div class="timelineList">
<p class="img"><a href="maternity.php"><img src="/common/img/menu/index/img_maternity.png" alt="マタニティ"></a></p>
<div class="txt">
<h3 class="timelineTit"><span>Maternity</span>マタニティ</h3>
<p class="short">ショート</p>
<p class="standard">スタンダード</p>
</div>
</div><!--timelineList-->
<div class="timelineList years60">
<p class="img"><a href="senior.php"><img src="/common/img/menu/index/img_senior.png" alt="還暦・喜寿・米寿"></a></p>
<div class="txt">
<h3 class="timelineTit"><span>SeniorPhotography</span>還暦・喜寿・米寿</h3>
<p class="short">ショート</p>
<p class="standard">スタンダード</p>
</div>
</div><!--timelineList-->

</div>

</section>

<ul class="otherMenu">
<li><a href="family.php#brotherPhoto"><img src="/common/img/menu/index/btn_brother.jpg" width="247" alt="兄弟一緒のお得なセット"></a></li>
<li><a href="location.php"><img src="/common/img/menu/index/btn_location.jpg" width="247" alt="出張撮影"></a></li>
<li><a href="family.php"><img src="/common/img/menu/index/btn_family.jpg" width="247" alt="家族写真"></a></li>
</ul>

<section class="reserveBottom">
<div class="reserveCircle">
<p><img src="/common/img/share/bg_reserve.png" alt="ご予約はWebから"></p>
</div>
<div class="txt">ミスタームックのご予約がwebから出来るようになりました！
<a href="/reserve/" class="yellowBtn"><span>撮影のご予約はこちら</span></a>
</div>
</section>

</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
