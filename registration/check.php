<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<?php
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    
    //メール重複チェック
    $sql = "SELECT member_id, member_email " ;
    $sql .= " FROM member " ;
    $sql .= " where member_email='".$member_email."' " ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        if($db_result[0]["member_id"]!="")
        {
            $common_connect -> Fn_javascript_back("すでに登録されているメールです。");
        }
    }

    //ユーザー重複チェック
    $sql = "SELECT member_id, login_id " ;
    $sql .= " FROM member " ;
    $sql .= " where login_id='".$login_id."' " ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        if($db_result[0]["member_id"]!="")
        {
            $common_connect -> Fn_javascript_back("すでに登録されているユーザー名です。");
        }
    }
?>
<article>
<section>
<h1 id="pageTitle">新規会員登録：入力内容の確認</h1>

<section class="descriptionArea">
<h2 class="tit"><?php echo $member_name_1 . " " . $member_name_2; ?> 様のご登録内容</h2>
<p>入力内容にお間違いがないか確認のうえ「確定する」ボタンを押して下さい。</p>
</section>

<form action="save.php" method="post">
<section class="formCheckArea">
<h3 class="tit">会員登録内容のご確認</h3>
<div class="formCheckAreaIn">
<table>
<tr>
<th>ユーザー名</th>
<td>
    <?php $var = "login_id"; ?>
    <?php echo $$var;?>
    <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>パスワード</th>
<td>
    <?php $var = "login_pw"; ?>
    <?
    $view_login_pw = "";
    for ($loop=0; $loop<strlen($login_pw) ; $loop++) { 
        $view_login_pw .= "●";
    }
    echo $view_login_pw;
    ?>
    <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>お名前</th>
<td>
    <?php $var = "member_name_1"; ?>
    <?php echo $$var;?>
    <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
    <?php $var = "member_name_2"; ?>
    <?php echo $$var;?>
    <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>ふりがな</th>
<td>
    <?php $var = "member_name_kana"; ?>
    <?php echo $$var;?>
    <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>電話番号</th>
<td>
    <?php $var = "tel"; ?>
    <?php echo $$var;?>
    <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>E-Mail</th>
<td>
    <?php $var = "member_email"; ?>
    <?php echo $$var;?>
    <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>住所</th>
<td>
    <?php $var = "post_num"; ?>
    &#12306;<?php echo $$var;?><input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>"><br>
    <?php $var = "address_1"; ?>
    <?php echo $$var;?><input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
    <?php $var = "address_2"; ?>
    <?php echo $$var;?><input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
    <br>
    <?php $var = "address_3"; ?>
    <?php echo $$var;?><input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>メール配信</th>
<td>
    <?php
        $var = "flag_mailling";
        if($$var==1) { echo "希望する";} else { echo "希望しない";}
    ?>
    <input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
</table>
</div>
</section>
<div class="descriptionArea">
<p class="formTxt">入力頂いたお客様の登録情報に関しましては、<a href="/about/privacy.php" class="cBlue" target="_blank">個人情報保護方針・ご利用規約</a>の同意をお願い致します。</p>
<input type="submit" value="利用規約に同意して、登録を確定する" class="submitBtn">
<input type="button" value="入力内容を編集に戻る" class="backBtn" onclick="javascirpt:history.back();">
</div>

</form>
</section>
</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
