<?php $title = "新規会員登録";?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<script type="text/javascript">
    $(function() {
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            err_check = false;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";


            err_check_count += check_input("member_name_1");
            err_check_count += check_input("member_name_2");
            err_check_count += check_input("member_name_kana");
            
            err_check_count += check_input_id("login_id");
            err_check_count += check_input_password("login_pw");
            err_check_count += check_input_tel("tel");
            err_check_count += check_input_email("member_email");
            err_check_count += check_input_post_num("post_num");
            
            if(err_check_count)
            {
                //alert("入力に不備があります");
                return false;
            }
            else
            {
                $('#form_regist').submit();
                return true;
            }
            
            
        });
        

        
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);
            $("#"+$str).removeClass("error").removeClass("reauired");
            
            if($('#'+$str).val().replace(/　/g," ").match(/^\s+$/))
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                $("#"+$str).focus();
                
                return 1;
            }
            else if($('#'+$str).val()=="")
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                $("#"+$str).focus();
                
                return 1;
            }
            return 0;
        }
        
        function check_input_email($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            
            if(checkIsEmail($('#'+$str_1).val()) == false)
            {
                err ="<span class='error'>メールアドレスは半角英数字でご入力ください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                $("#"+$str_1).focus();
                
                return 1;
            }
            
            return 0;
        }

        function check_input_tel($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            
            if(checkIsTel($('#'+$str_1).val()) == false)
            {
                err ="<span class='error'>電話番号は半角英数字でご入力ください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                $("#"+$str_1).focus();
                
                return 1;
            }
            
            return 0;
        }
        
        function check_input_password($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            
            if(checkIsPassword($('#'+$str_1).val()) == false)
            {
                err ="<span class='error'>パスワードは6文字以上の半角英数字「!#$%&@()*+,.」でご入力ください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                $("#"+$str_1).focus();
                
                return 1;
            }
            
            return 0;
        }

        function check_input_id($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
        
            if(checkIsID($('#'+$str_1).val()) == false)
            {
                err ="<span class='error'>IDは4文字以上の半角英数字「-_」でご入力ください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                $("#"+$str_1).focus();
                
                return 1;
            }
            
            return 0;
        }
        

        function check_input_post_num($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
        
            if(checkIsPostNum($('#'+$str_1).val()) == false)
            {
                err ="<span class='error'>7文字の半角英数字でご入力ください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                $("#"+$str_1).focus();
                
                return 1;
            }
            
            return 0;
        }
        

        //メールチェック
        function checkIsEmail(value) {
            if (value.match(/.+@.+\..+/) == null) {
                return false;
            }
            return true;
        }
        
        function checkIsTel(value) {
            if (value.match(/^[0-9]{8,}$/) == null) {
                return false;
            }
            return true;
        }
        
        function checkIsPostNum(value) {
            if (value.match(/^[0-9]{7,}$/) == null) {
                return false;
            }
            return true;
        }
        
        function checkIsID(value) 
        {
            if (value.match(/^[0-9a-z_-]{4,}$/) == null) {
                return false;
            }
            return true;
        }

        function checkIsPassword(value) 
        {
            if (value.match(/^[0-9a-zA-Z!#$%&@()*+,./_-]{6,}$/) == null) {
                return false;
            }
            return true;
        }

        
    });
    
//-->
</script>
<script src="/common/js/ajaxzip3.js" charset="UTF-8"></script>


<article>
<section>
<h1 id="pageTitle">新規会員登録：登録フォーム</h1>
<section class="descriptionArea">
<h2 class="txt">1家族につき1アカウントの取得でお願い致します。</h2>
<p>1家族様につき１アカウントのみの取得をお願い致します。<br>
当スタジオは小さなスタジオですので、1日に撮影できるのは、2〜3組のお客様となっています。1組でも多くのお客様にご利用頂きたいと思っておりますので、1家族様で複数枠のご予約や、複数名義での登録はご遠慮頂いております。ご理解の程よろしくお願い致します。</p>
</section>

<form action="./check.php" method="POST" name="form_write" id="form_regist">
<section class="formEnterArea">
<h3 class="tit"><span>ログイン情報を入力してください。</span></h3>
<dl>
<dt>ユーザー名<span class="nomal">（※半角英数4文字以上）</span></dt>
<dd>
    <? $var = "login_id"; ?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="例）mrmook">
    <label id="err_<?php echo $var;?>"></label>
</dd>
<dt>パスワード<span class="nomal">（※半角英数6文字以上）</span></dt>
<dd>
    <? $var = "login_pw"; ?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="例）mook55">
    <label id="err_<?php echo $var;?>"></label>
</dd>
</dl>
<p class="formTxt">※このユーザー名とパスワードがログイン時に必要な会員アカウントとなります。忘れない様メモ等をお願い致します。</p>
</section>
<section class="formEnterArea">
<h3 class="tit"><span>お客様情報を入力してください。</span></h3>
<p class="formTxt">ご登録頂いた代表者様のお名前や電話番号は、撮影前の連絡や、商品が出来上がった際の連絡先として使用させて頂く予定です。<br>
繋がりやすい方のご連絡先をお願い致します。</p>
<dl>
<dt>代表者 お名前（姓）</dt>
<dd>
    <? $var = "member_name_1"; ?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="例）鈴木">
    <label id="err_<?php echo $var;?>"></label>
</dd>
<dt>代表者 お名前（名）</dt>
<dd>
    <? $var = "member_name_2"; ?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="例）一郎">
    <label id="err_<?php echo $var;?>"></label>
</dd>
<dt>ふりがな</dt>
<dd>
    <? $var = "member_name_kana"; ?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="例）すずきいちろう">
    <label id="err_<?php echo $var;?>"></label>
</dd>
<dt>電話番号<span class="nomal">（※-ハイフンなし 半角英数）</span></dt>
<dd>
    <? $var = "tel"; ?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="例）09012341234">
    <label id="err_<?php echo $var;?>"></label>
</dd>
<dt>E-Mail</dt>
<dd>
    <? $var = "member_email"; ?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="メールアドレス">
    <label id="err_<?php echo $var;?>"></label>
</dd>
<dt>郵便番号<span class="nomal">（※-ハイフンなし 半角英数）</span></dt>
<dd>
    <? $var = "post_num"; ?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" placeholder="例）9012134" class="shortForm" onKeyUp="AjaxZip3.zip2addr(this,'','address_1','address_2');">
    <input type="button" value="自動住所入力" class="addressBtn" onclick="AjaxZip3.zip2addr(this,'','address_1','address_2');">
    <label id="err_<?php echo $var;?>"></label>
</dd>
<dt>都道府県</dt>
<dd>
    <? $var = "address_1"; ?>
    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" maxlength="8">
    <label id="err_<?php echo $var;?>"></label>
</dd>
<dt>市町村・番地</dt>
<dd>
    <? $var = "address_2"; ?>
    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" placeholder="例）浦添市港川2-12-8">
    <label id="err_<?php echo $var;?>"></label>
</dd>
<dt>建物名など</dt>
<dd>
    <? $var = "address_3"; ?>
    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" placeholder="例）○○マンション502号">
    <label id="err_<?php echo $var;?>"></label>
</dd>
</dl>
<p class="formTxt">
    <? $var = "flag_mailling"; ?>
    <label><input name="<? echo $var;?>" id="<? echo $var;?>" value="1" type="checkbox" checked="checked"> スタジオからのお知らせやお得な情報などをメールでお送りしてもよろしいでしょうか</label>
</p>
<input type="submit" value="入力内容を確認へ進む" class="submitBtn" id="form_confirm">
</section>

</form>
</section>
</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
