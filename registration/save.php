<?php
    require_once $_SERVER['DOCUMENT_ROOT'].$DOCUMENT_ROOT."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT'].$DOCUMENT_ROOT."/app_include/CommonEmail.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    $common_email = new CommonEmail(); //メール関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>会員登録</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    
    if($login_id == "" || $login_pw == "" || $member_name_1 == "" || $member_name_2 == "" || $member_name_kana == "" || $tel == "" || $member_email == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }
    
    //メール重複チェック
    $sql = "SELECT member_id, member_email " ;
    $sql .= " FROM member " ;
    $sql .= " where member_email='".$member_email."' " ;

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        if($db_result[0]["member_id"]!="")
        {
            $common_connect -> Fn_javascript_back("すでに登録されているメールです。");
        }
    }

    //ユーザー重複チェック
    $sql = "SELECT member_id, login_id " ;
    $sql .= " FROM member " ;
    $sql .= " where login_id='".$login_id."' " ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        if($db_result[0]["member_id"]!="")
        {
            $common_connect -> Fn_javascript_back("すでに登録されているユーザー名です。");
        }
    }

    $datetime = date("Y/m/d H:i:s");
    $flag_open = 1;
    
    //array
    $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "login_id", "login_pw", "post_num", "address_1", "address_2", "address_3", "tel", "member_email", "flag_mailling", "flag_open");

    //基本情報
    $db_insert = "insert into member ( ";
    $db_insert .= " member_id, ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= $val.", ";
    }
    $db_insert .= " regi_date, up_date ";
    $db_insert .= " ) values ( ";
    $db_insert .= " '', ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= " '".$$val."', ";
    }
    $db_insert .= " '$datetime', '$datetime')";
    $common_dao->db_update($db_insert);

                
    if ($member_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id"; 
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $member_id = $db_result[0]["last_id"];
        }
    }

    $temp_url = global_ssl."/mypage/";
    $birth_yyyymmdd =   $birth_yyyy."年 ".$birth_mm."月 ".$birth_dd."日";
    $view_login_pw = substr($login_pw, 0, 2);
    for ($loop=2; $loop<strlen($login_pw) ; $loop++) { 
        $view_login_pw .= "*";
    }

    $var = "flag_mailling";
    if($$var==1) { $view_flag_mailling = "希望する";} else { $view_flag_mailling = "希望しない";}

    //Thank youメール
    if ($member_email != "")
    {
        $subject = "[Mr.MOOK]会員登録頂きありがとうございます";
        
        $body = "";
        $body = file_get_contents("./mail/user.mail.php");
        $body = str_replace("[login_id]", $login_id, $body);
        $body = str_replace("[view_login_pw]", $view_login_pw, $body);
        $body = str_replace("[member_name_1]", $member_name_1, $body);
        $body = str_replace("[member_name_2]", $member_name_2, $body);
        $body = str_replace("[member_name_kana]", $member_name_kana, $body);
        $body = str_replace("[tel]", $tel, $body);
        $body = str_replace("[member_email]", $member_email, $body);
        $body = str_replace("[post_num]", $post_num, $body);
        $body = str_replace("[address_1]", $address_1, $body);
        $body = str_replace("[address_2]", $address_2, $body);
        $body = str_replace("[address_3]", $address_3, $body);
        $body = str_replace("[view_flag_mailling]", $view_flag_mailling, $body);
        $body = str_replace("[temp_url]", $temp_url, $body);
        $body = str_replace("[datetime]", $datetime, $body);
        $body = str_replace("[global_email_footer]", $global_email_footer, $body);


        $common_email-> Fn_send_utf($member_email."<".$member_email.">",$subject,$body,$global_mail_from,$global_send_mail);
        
    }


    $subject = "[Mr.MOOK]会員登録受付";
    
    $body = "";
    $body = file_get_contents("./mail/admin.mail.php");
    $body = str_replace("[login_id]", $login_id, $body);
    $body = str_replace("[view_login_pw]", $view_login_pw, $body);
    $body = str_replace("[member_name_1]", $member_name_1, $body);
    $body = str_replace("[member_name_2]", $member_name_2, $body);
    $body = str_replace("[member_name_kana]", $member_name_kana, $body);
    $body = str_replace("[tel]", $tel, $body);
    $body = str_replace("[member_email]", $member_email, $body);
    $body = str_replace("[post_num]", $post_num, $body);
    $body = str_replace("[address_1]", $address_1, $body);
    $body = str_replace("[address_2]", $address_2, $body);
    $body = str_replace("[address_3]", $address_3, $body);
    $body = str_replace("[view_flag_mailling]", $view_flag_mailling, $body);
    $body = str_replace("[temp_url]", $temp_url, $body);
    $body = str_replace("[datetime]", $datetime, $body);
    $body = str_replace("[global_email_footer]", $global_email_footer, $body);


    $common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);
    
    session_start();
    $_SESSION['member_id']=$member_id;
    $_SESSION['member_name']=$member_name_1." ".$member_name_2;
                

    //会員登録の時はcookies削除
    /*
    setcookie("pck", "", time()-3600, "/");

    foreach($_POST as $key => $value)
    { 
        setcookie($key, "");
    }
    */
    
    $common_connect-> Fn_redirect("thanks.php");
?>
</body>
</html>