<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<article>
<section>
<h1 id="pageTitle">新規会員登録：完了</h1>

<section class="descriptionArea thankyou">
<h2 class="tit">会員登録が完了しました！</h2>
<div class="formTxt">
<p>ご登録頂いたメールアドレス宛に内容確認メールを自動送信致しました。</p>
<p class="subTxt">※携帯メールをご利用の場合、スタジオからのメールが届かない場合があります。お客様のメール設定でパソコンからのメールが受信拒否になっている可能性がありますのでご確認頂きますようお願い致します。</p>
</div>
</section>

<div class="descriptionArea">
<a href="/mypage/" class="mypageBtn">マイページに進む！</a>
<p class="formTxt mt10">当サイトの「マイページ」からも登録内容をご確認頂けます。<br>
<span class="cRed">万が一、マイページに登録内容が反映されていない場合は、お手数ですがお電話にてご連絡くださいます様お願い致します。</span></p>
</div>

</form>
</section>
</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
