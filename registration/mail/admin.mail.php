『Mr.MOOK』に会員登録がありました。

─────↓─────────────────────

--------------------------------------------------
登録内容
--------------------------------------------------
【ユーザー名】 [login_id]
【パスワード】 [view_login_pw]
【お名前】 [member_name_1] [member_name_2]
【ふりがな】 [member_name_kana]
【電話番号】 [tel]
【E-Mail】 [member_email]
【住所】〒 [post_num]
[address_1] [address_2] [address_3]
【メール配信】 [view_flag_mailling]

【受付日時】[datetime]

─────↑─────────────────────