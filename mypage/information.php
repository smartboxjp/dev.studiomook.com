<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<?
    //ログインチェック
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION["member_id"];

    //会員情報
    $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "tel", "member_email", "post_num", "address_1", "address_2", "address_3");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM member where member_id='".$member_id."' and flag_open=1" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }
?>
<article>
<section id="mypageTop">
<h1 id="mypageTitle">ー マイページ ー</h1>
</section>

<div id="mypageNav">
<ul>
<li class="home"><a href="./"><img src="/common/img/mypage/ico_home.png"></a></li>
<li><a href="reserved.php">予約の確認</a></li>
<li class="act"><a href="information.php">登録者情報</a></li>
<li><a href="history.php">撮影履歴</a></li>
</ul>
</div>

<section class="mypageTitArea">
<h2 class="mypageTit">登録者情報</h2>
<p>ご登録頂いている代表者様の情報です。引っ越し等で住所や連絡先等が変わった場合は、登録内容の変更をお願い致します。</p>
</section>

<section class="formEnterArea">
<table>
<tr>
<th>お名前</th>
<td><? echo $member_name_1;?> <? echo $member_name_2;?></td>
</tr>
<tr>
<th>ふりがな</th>
<td><? echo $member_name_kana;?></td>
</tr>
<tr>
<th>電話番号</th>
<td><? echo $tel;?></td>
</tr>
<tr>
<th>E-Mail</th>
<td><? echo $member_email;?></td>
</tr>
<tr>
<th>住所</th>
<td>〒<? echo $post_num;?><br>
<? echo $address_1;?><br>
<? echo $address_2;?><br>
<? echo $address_3;?></td>
</tr>
</table>
<div class="mypageBtnArea">
<a href="information_form.php" class="smallYellowBtn mr10">登録内容を変更する</a>
</div>

</section>
</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
