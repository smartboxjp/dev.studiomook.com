<?php
    require_once $_SERVER['DOCUMENT_ROOT'].$DOCUMENT_ROOT."/app_include/connect.php";
    require_once $_SERVER['DOCUMENT_ROOT'].$DOCUMENT_ROOT."/app_include/CommonEmail.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
    $common_email = new CommonEmail(); //メール関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>会員登録</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php
    //ログインチェック
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION["member_id"];

    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
    
    if($member_name_1 == "" || $member_name_2 == "" || $member_name_kana == "" || $tel == "" || $member_email == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }
    
    //メール重複チェック
    $sql = "SELECT member_id, member_email " ;
    $sql .= " FROM member " ;
    $sql .= " where member_email='".$member_email."' and member_id!='".$member_id."' " ;

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        if($db_result[0]["member_id"]!="")
        {
            $common_connect -> Fn_javascript_back("すでに登録されているメールです。");
        }
    }

    $datetime = date("Y/m/d H:i:s");
    $flag_open = 1;
    
    //array
    $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "post_num", "address_1", "address_2", "address_3", "tel", "member_email", "flag_mailling", "flag_open");

    //基本情報
    $db_insert = "update member set ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= $val."='".$$val."', ";
    }
    $db_insert .= " up_date='".$datetime."' ";
    $db_insert .= " where member_id='".$member_id."'";
    $common_dao->db_update($db_insert);

    //パスワードが設定された場合更新する
    if($login_pw!="")
    {
        $db_insert = "update member set login_pw='".$login_pw."', ";
        $db_insert .= " up_date='".$datetime."' ";
        $db_insert .= " where member_id='".$member_id."'";
        $common_dao->db_update($db_insert);
    }

    session_start();
    $_SESSION['member_id']=$member_id;
    $_SESSION['member_name']=$member_name_1." ".$member_name_2;
                

    //会員登録の時はcookies削除
    /*
    setcookie("pck", "", time()-3600, "/");

    foreach($_POST as $key => $value)
    { 
        setcookie($key, "");
    }
    */
    
    $common_connect-> Fn_redirect("/mypage/information.php");
?>
</body>
</html>