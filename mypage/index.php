<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<?
    //ログインチェック
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION["member_id"];

    //会員情報
    $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "tel", "member_email", "post_num", "address_1", "address_2", "address_3", "member_img");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM member where member_id='".$member_id."' and flag_open=1" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }


    //プランコース
    $arr_plan_course = array();
    $sql = "select cate_course_time_id, cate_course_time_from, cate_course_time_to ";
    $sql .= " from cate_course_time   ";
    $sql .= " where flag_open=1  ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_time[$db_result[$db_loop]["cate_course_time_id"]] = substr($db_result[$db_loop]["cate_course_time_from"], 0, 5)." 〜 ".substr($db_result[$db_loop]["cate_course_time_to"], 0, 5);
        }
    }
?>
<article>
<section id="mypageTop">
<h1 id="mypageTitle">ー マイページ ー</h1>
<div class="mypageTopBox">
<p class="img">
<? if($member_img=="") { ?>
    <img src="/common/img/mypage/img_default.jpg">
<? } else { ?>
    <img src='/<? echo global_member_dir.$member_id."/".$member_img; ?>' >
<? } ?>
</p>
<p class="txt"><span class="tit">ようこそマイページへ！</span>
このページでは予約の確認や、登録者情報の確認・変更、また過去の撮影履歴をみることが出来ます。</p>
</div>
</section>

<div id="mypageNav">
<ul>
<li class="home act"><a href="./"><img src="/common/img/mypage/ico_home.png"></a></li>
<li><a href="reserved.php">予約の確認</a></li>
<li><a href="information.php">登録者情報</a></li>
<li><a href="history.php">撮影履歴</a></li>
</ul>
</div>

<section class="mypageArea">
<h2 class="tit"><span>予約状況</span></h2>
<?
//予約情報
$arr_db_field = array("reserve_id", "member_id", "member_name_1", "member_name_2", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "visited_count", "visit_adult", "visit_child", "status");

$sql = "SELECT ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM reserve where reserve_day>='".date("Y-m-d")."' and member_id='".$member_id."' " ;
$sql .= " and status=1" ; //予約中
$sql .= " and flag_open=1" ;
$sql .= " order by reserve_day " ;

$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
?>
<section class="mypageBox">
<h3 class="subTit">確定した予約</h3>
<dl>
<dt>現在予約中の撮影日時</dt>
<?
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }

        $yyyy = date("Y",strtotime($reserve_day));
        $mm = date("m",strtotime($reserve_day));
        $dd = date("d",strtotime($reserve_day));

        echo "<dd>".$yyyy."年".$mm."月".$dd."日（".$common_connect->Fn_date_day($yyyy.$mm.$dd)."）".$arr_cate_course_time[$cate_course_time_id]."</dd>";
    }
?>
</dl>
</section>
<div class="mypageBtnArea">
<a href="reserved.php" class="smallYellowBtn">予約内容を確認する</a>
<p class="subTxt">予約日の変更・キャンセルについてもこちらからご確認ください。</p>
</div>
<?
}
else
{
?>
<section class="mypageBox">
<h3 class="subTit">確定した予約</h3>
<dl>
<dt>現在予約中のデータがありません。</dt>
</dl>
</section>
<?
}
?>


<?
//キャンセル待ちの予約情報
$arr_db_field = array("reserve_id", "member_id", "member_name_1", "member_name_2", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "visited_count", "visit_adult", "visit_child", "status");

$sql = "SELECT ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM reserve where reserve_day>='".date("Y-m-d")."' and member_id='".$member_id."' " ;
$sql .= " and (status=2 or status=3 or status=4)" ; //キャンセル待ち
$sql .= " and flag_open=1" ;
$sql .= " order by reserve_day " ;

$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
?>
<section class="mypageBox">
<h3 class="subTit">キャンセル待ちの予約</h3>
<table>
<?
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }

        $yyyy = date("Y",strtotime($reserve_day));
        $mm = date("m",strtotime($reserve_day));
        $dd = date("d",strtotime($reserve_day));

        echo "<th>第".($db_loop+1)."希望</th>";
        echo "<td>".$yyyy."年".$mm."月".$dd."日（".$common_connect->Fn_date_day($yyyy.$mm.$dd).'）<br class="sp">'.$arr_cate_course_time[$cate_course_time_id]."</td>";
        echo "</tr>";
    }
?>

</table>
</section>
<div class="mypageBtnArea">
<a href="waiting.php" class="smallGrayBtn">キャンセル待ちについて</a>
</div>
<p class="mypageNotes">万が一、マイページに予約内容が反映されていない場合は、お手数ですがお電話にてご連絡くださいます様お願い致します。</p>

<?
} else {
?>
<section class="mypageBox">
<h3 class="subTit">キャンセル待ちの予約</h3>
<dl>
<dt>キャンセル待ちの予約のデータがありません。</dt>
</dl>
</section>
<?
} 
?>

<section class="mypageArea">
<h2 class="tit"><span>会員情報</span></h2>
<section class="mypageGuideBox">
<h3 class="subTit">マイページで出来ること</h3>
<div class="mypageGuideBoxIn">
<table>
<tr>
<td>
<a href="information.php">
<p class="tableTit">登録者情報の確認</p>
<p class="img"><img src="/common/img/mypage/img_information.png" alt="登録者情報の確認"></p>
</a>
<p class="txt">ご登録頂いている代表者の住所・連絡先等の変更はこちらから行えます。</p>
</td>
<td>
<a href="history.php">
<p class="tableTit">撮影履歴をみる</p>
<p class="img"><img src="/common/img/mypage/img_history.png" alt="撮影履歴をみる"></p>
</a>
<p class="txt">過去、Mr.MOOKで撮影頂いた際の履歴を掲載しています！</p>
</td>
</tr>
</table>
</div>
</section>
</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
