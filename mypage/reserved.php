<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php $title = "予約の確認";?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<?
    //ログインチェック
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION["member_id"];

    //会員情報
    $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "tel", "member_email", "post_num", "address_1", "address_2", "address_3");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM member where member_id='".$member_id."' and flag_open=1" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }


    //プランコース
    $arr_plan_course = array();
    $sql = "select cate_course_time_id, cate_course_time_from, cate_course_time_to ";
    $sql .= " from cate_course_time   ";
    $sql .= " where flag_open=1  ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_time[$db_result[$db_loop]["cate_course_time_id"]] = substr($db_result[$db_loop]["cate_course_time_from"], 0, 5)." 〜 ".substr($db_result[$db_loop]["cate_course_time_to"], 0, 5);
        }
    }
?>
<article>
<section id="mypageTop">
<h1 id="mypageTitle">ー マイページ ー</h1>
</section>

<div id="mypageNav">
<ul>
<li class="home"><a href="./"><img src="/common/img/mypage/ico_home.png"></a></li>
<li class="act"><a href="reserved.php">予約の確認</a></li>
<li><a href="information.php">登録者情報</a></li>
<li><a href="history.php">撮影履歴</a></li>
</ul>
</div>

<section class="mypageTitArea">
<h2 class="mypageTit">予約の確認</h2>
<p>ご予約ありがとうございます！<br>
現在ご予約頂いている撮影内容、またはご来店前のチェックポイントはこちらでご確認ください。<br>
スタッフ一同、当日を楽しみにお待ちしています。</p>
</section>

<section class="mypageArea">
<?
//予約情報
$arr_db_field = array("reserve_id", "member_id", "member_name_1", "member_name_2", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "visited_count", "visit_adult", "visit_child", "status");

$sql = "SELECT ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM reserve where reserve_day>='".date("Y-m-d")."' and member_id='".$member_id."' " ;
$sql .= " and status=1" ; //予約中
$sql .= " and flag_open=1" ;
$sql .= " order by reserve_day " ;

$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
?>
<section class="mypageBox">
<h3 class="subTit">確定した予約</h3>
<dl>
<dt>現在予約中の撮影日時</dt>
<?
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }

        $yyyy = date("Y",strtotime($reserve_day));
        $mm = date("m",strtotime($reserve_day));
        $dd = date("d",strtotime($reserve_day));

        echo "<dd>".$yyyy."年".$mm."月".$dd."日（".$common_connect->Fn_date_day($yyyy.$mm.$dd)."）".$arr_cate_course_time[$cate_course_time_id]."</dd>";
    }
?>
</dl>
</section>
<?
}
else
{
?>
<section class="mypageBox">
<h3 class="subTit">確定した予約</h3>
<dl>
<dt>現在予約中のデータがありません。</dt>
</dl>
</section>
<?
}
?> 



<?
//予約情報
$arr_db_field = array("reserve_id", "member_id", "member_name_1", "member_name_2", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "visited_count", "reserve_count", "visit_adult", "visit_child", "status");

$sql = "SELECT ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM reserve where reserve_day>='".date("Y-m-d")."' and member_id='".$member_id."' " ;
$sql .= " and status=1" ; //予約中
$sql .= " and flag_open=1" ;
$sql .= " order by reserve_day " ;

$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
?>
<section class="formEnterArea">
<h3 class="tit"><span>予約内容</span></h3>
<?
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }

        $yyyy = date("Y",strtotime($reserve_day));
        $mm = date("m",strtotime($reserve_day));
        $dd = date("d",strtotime($reserve_day));
?>

<table class="mb30">
<tr>
<th>撮影日</th>
<td><? echo $yyyy."年".$mm."月".$dd."日（".$common_connect->Fn_date_day($yyyy.$mm.$dd)."）";?></td>
</tr>
<tr>
<th>撮影時間</th>
<td><? echo $arr_cate_course_time[$cate_course_time_id];?></td>
</tr>
<tr>
<th>撮影コース</th>
<td><? echo $cate_course_name;?></td>
</tr>
<tr>
<th>主役の人数</th>
<td><? echo $reserve_count;?>人</td>
</tr>
<tr>
<th>来店人数</th>
<td>大人<? echo $visit_adult;?>人 子供<? echo $visit_child;?>人</td>
</tr>
<tr>
<th>スタジオご利用回数</th>
<td><? echo $visited_count;?>回目</td>
</tr>

<?
//予約情報sub
$arr_db_field_sub = array("reserve_sub_id", "reserve_id", "reserve_sub_name", "reserve_sub_kana", "sex", "reserve_sub_birth", "cate_menu_id", "cate_menu_name");

$sql_sub = "SELECT ";
foreach($arr_db_field_sub as $val)
{
    $sql_sub .= $val.", ";
}
$sql_sub .= " 1 FROM reserve_sub where reserve_id='".$reserve_id."' " ;
$sql_sub .= " order by reserve_sub_id " ;

$db_result_sub = $common_dao->db_query_bind($sql_sub);
if($db_result_sub)
{
    for($db_loop_sub=0 ; $db_loop_sub < count($db_result_sub) ; $db_loop_sub++)
    {
        foreach($arr_db_field_sub as $val)
        {
            $$val = $db_result_sub[$db_loop_sub][$val];
        }
?>
    <tr>
    <th>主役<? echo ($db_loop_sub+1);?>のお名前</th>
    <td><? echo $reserve_sub_name;?></td>
    </tr>
    <tr>
    <th>主役<? echo ($db_loop_sub+1);?>のふりがな</th>
    <td><? echo $reserve_sub_kana;?></td>
    </tr>
    <tr>
    <th>主役<? echo ($db_loop_sub+1);?>の撮影メニュー</th>
    <td><? echo $cate_menu_name;?></td>
    </tr>
    <tr>
    <th>主役<? echo ($db_loop_sub+1);?>の性別</th>
    <td><? echo $sex;?></td>
    </tr>
    <tr>
    <th>主役<? echo ($db_loop_sub+1);?>の生年月日</th>
    <td><? echo date("Y年m月d日",strtotime($reserve_sub_birth));?></td>
    </tr>
<?
    }
}
?>

</table>

<?
    }
?>

<p class="pinkTit">予約内容の変更・キャンセルについて</p>
<p class="formTxt">予約内容の変更や、予約日のキャンセルは、<span class="bold">お電話のみの受付</span>となっております。ご自身では行えませんので、必ずスタジオまで直接お電話くださいます様お願い致します。<br>
撮影当日連絡なくキャンセルされた場合、次回から予約をご遠慮頂いております。</p>
<div class="pinkBox img">
当日お子様の急な発熱などでも、キャンセル料等は発生しませんので心配なくスタジオにご連絡ください♪ <br>
キャンセル待ちのお客様もいらっしゃいますので、お早目にご連絡をお願い致します。<br>
撮影中の場合は折り返しますので留守番電話への伝言をお願い致します！
</div>
</section>
<?
}
?>


<section class="formEnterArea">
<h3 class="tit"><span>ご来店前のチェックポイント</span></h3>
<p class="formTxt">撮影前に気になることやご不明点等は、すべてお電話にて丁寧にお答えさせて頂いております。
<!--また「<a href="/about/qa.php" class="cBlue">よくある質問</a>」もご覧ください。--></p>
<div class="mypageCheckBox">
<h4 class="checkTit"><span>check1</span>撮影当日は、お時間ちょうどにご来店ください</h4>
一組様貸切りで撮影を行っています。 撮影当日、ご予約時間より早めにご来店頂くと駐車場に余裕がない為、一旦近隣の有料駐車場にてお待ち頂く形となってしまいます。<br>また、15分以上遅れて来店された場合、次のお客様の順番になってしまうこともございますのでご注意ください。
</div>
<div class="mypageCheckBox">
<h4 class="checkTit"><span>check2</span>パパ＆ママのお洋服もご準備を^^</h4>
大人の方の衣装のレンタルは行っていませんので、家族写真をご希望の方は、ぜひお洋服の雰囲気を家族で合わせるなど、ご用意ください。<br>
おすすめは柄や色味の強くないシンプルなスタイルですが、あまり気になさらず、パパとママの服装の色味を揃え、家族の雰囲気を統一することで、より素敵な家族写真に仕上がります^^
</div>
<div class="mypageCheckBox">
<h4 class="checkTit"><span>check3</span>より素敵な表情を引き出すために</h4>
当日はお昼寝時間、食事時間などあらかじめご調整くださいますようお願い致します。撮影に飽きてしまったとき用のリフレッシュアイテムとしておもちゃやおやつをご持参頂いたり、ご来店前から「今日は一緒に写真撮ろうね♪」などの楽しいお声がけをしながら来て頂いたりするだけで、お子さまのリラックスした表情に繋がって行きますので、ぜひご協力の程よろしくお願い致します^^
</div>
</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
