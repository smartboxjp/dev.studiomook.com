<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<?
    //ログインチェック
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION["member_id"];

    //会員情報
    $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "tel", "member_email", "post_num", "address_1", "address_2", "address_3");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM member where member_id='".$member_id."' and flag_open=1" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }


    //プランコース
    $arr_plan_course = array();
    $sql = "select cate_course_time_id, cate_course_time_from, cate_course_time_to ";
    $sql .= " from cate_course_time   ";
    $sql .= " where flag_open=1  ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_time[$db_result[$db_loop]["cate_course_time_id"]] = substr($db_result[$db_loop]["cate_course_time_from"], 0, 5)." 〜 ".substr($db_result[$db_loop]["cate_course_time_to"], 0, 5);
        }
    }
?>
<article>
<section id="mypageTop">
<h1 id="mypageTitle">ー マイページ ー</h1>
</section>

<div id="mypageNav">
<ul>
<li class="home"><a href="./"><img src="/common/img/mypage/ico_home.png"></a></li>
<li class="act"><a href="reserved.php">予約の確認</a></li>
<li><a href="information.php">登録者情報</a></li>
<li><a href="history.php">撮影履歴</a></li>
</ul>
</div>

<section class="mypageTitArea">
<h2 class="mypageTit">キャンセル待ち</h2>
</section>

<?
//キャンセル待ちの予約情報
$arr_db_field = array("reserve_id", "member_id", "member_name_1", "member_name_2", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "visited_count", "visit_adult", "visit_child", "status");

$sql = "SELECT ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM reserve where reserve_day>='".date("Y-m-d")."' and member_id='".$member_id."' " ;
$sql .= " and (status=2 or status=3 or status=4)" ; //キャンセル待ち
$sql .= " and flag_open=1" ;
$sql .= " order by reserve_day " ;

$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
?>
<section class="mypageArea">
<section class="mypageBox">
<h3 class="subTit">キャンセル待ちの予約</h3>
<table>
<?
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }

        $yyyy = date("Y",strtotime($reserve_day));
        $mm = date("m",strtotime($reserve_day));
        $dd = date("d",strtotime($reserve_day));

        echo "<th>第".($status-1)."希望</th>";
        echo "<td>".$yyyy."年".$mm."月".$dd."日（".$common_connect->Fn_date_day($yyyy.$mm.$dd).'）<br class="sp">'.$arr_cate_course_time[$cate_course_time_id]."</td>";
        echo "</tr>";
    }
?>
</table>
</section>
</section>
<?
} else {
?>
<section class="mypageBox">
<h3 class="subTit">キャンセル待ちの予約</h3>
<dl>
<dt>キャンセル待ちの予約のデータがありません。</dt>
</dl>
</section>
<?
} 
?>

<section class="formEnterArea">
<h3 class="tit"><span>キャンセル待ちについて</span></h3>
<div class="formTxt">
<p class="bTit">キャンセル待ちが出た場合</p>
<p>ご希望の日時にキャンセルが出た場合、ご登録頂いているお電話へご連絡させて頂きます。<span class="cPink bold">お電話での確認が取れ次第、ご予約が確定となります。</span><br>
電話に出られなかった場合、24時間以内に折り返しご連絡をお願い致します。（ 留守番電話にメッセージをお願いします ）指定時間以内に折り返しの連絡がなかった場合は、次のキャンセル待ちの方へご連絡させて頂きます。<br>
予約が確定すると、マイページの「 <a href="reserved.php" class="cBlue">予約の確認</a> 」ページに、ご予約内容が反映されます。</p>
</div>
<div class="formTxt">
<p class="bTit">キャンセルが出なかった場合</p>
<p>スタジオからの連絡はありませんので予めご了承下さい。</p>
</div>
<div class="formTxt">
<p class="bTit">キャンセル待ちの変更</p>
<p>キャンセル待ちの日時の変更・キャンセルを行いたい場合は、本予約と同様、<span class="bold">お電話のみの受付</span>となっております。ご自身では行えませんので、必ずスタジオまで直接お電話下さいます様お願い致します。</p>
</div>
</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
