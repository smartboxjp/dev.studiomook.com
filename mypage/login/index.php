<?php
    require_once $_SERVER['DOCUMENT_ROOT'].$DOCUMENT_ROOT."/app_include/connect.php";
    
    $common_connect = new CommonConnect();
    $common_dao = new CommonDao(); //DB関連
?>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<script type="text/javascript">
    $(function() {
        
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";
            
            err_check_count += check_input_id("login_id");
            err_check_count += check_input_pw("login_pw");
            
            if(err_check_count!=0)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {
                //$('#form_confirm').submit();
                $('#form_confirm', "body").submit();
                return true;
            }
            
            
        });
                
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);

            if($('#'+$str).val().replace(/　/g," ").match(/^\s+$/))
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                $("#"+$str).focus();
                
                return 1;
            }
            else if($('#'+$str).val()=="")
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }


        //メールチェック
        function check_input_id($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            
            if($('#'+$str_1).val().replace(/　/g," ").match(/^\s+$/))
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                $("#"+$str_1).focus();
                
                return 1;
            }
            else if($('#'+$str_1).val()=="")
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                
                return 1;
            }
            else if($('#'+$str).val().length<4)
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            
            return 0;
        }

        //メールチェック
        
        function checkIsEmail(value) {
            if (value.match(/.+@.+\..+/) == null) {
                return false;
            }
            return true;
        }
        
        function check_input_pw($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);
            
            if($('#'+$str).val()=="")
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            else if($('#'+$str).val().length<6)
            {
                err ="<span class='error'>６文字以上入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }
                        
    });
    
//-->
</script>
<?
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
?>
<div class="loginArea">
<h1 id="popupTitle">ご利用はログインが必要です</h1>
<script type="text/javascript">// <![CDATA[
$(document).ready(function(){
$(".hoge_btn").colorbox({"inline:true"});
});
// HTMLで指定したIDの要素をクリックするとColorBoxウィンドウが閉じます。
$(".loginBtn").click(function(){
parent.$.fn.colorbox.close(); return false;
});
// ]]></script>

<div class="loginAreaIn">

<form action="/mypage/login/login_check.php" name="form_regist" id="form_regist" method="post">
<div class="loginBox">
    <h3 class="tit">ログイン情報を入力してください。</h3>
    <dl>
        <dt>ユーザー名</dt>
        <dd>
            <? $var = "login_id";?>
            <input name="<?=$var;?>" id="<?=$var;?>" type="text">
            <label id="err_<?=$var;?>"></label>
        </dd>
        <dt>パスワード</dt>
        <dd>
            <? $var = "login_pw";?>
            <input name="<?=$var;?>" id="<?=$var;?>" type="password">
            <label id="err_<?=$var;?>"></label>
        </dd>
    </dl>
    <? $var = "form_confirm";?>
    <input name="<?php echo $var;?>" id="<?php echo $var;?>" type="submit" value="ログイン" class="loginBtn">
    <p>※ユーザー名またはパスワードをお忘れての方は<a href="/mypage/login/password.php">こちら</a></p>
</div>
<? $var = "return_url";?>
<input name="<?=$var;?>" id="<?=$var;?>" type="hidden" value="<?=$$var;?>">
</form>

<div class="loginBox">
<h3 class="tit">会員登録がまだの方はまずは新規会員登録！</h3>
<p class="txt">次回からはIDとパスワードを入力するだけで、カレンダーからご予約頂けます。</p>
<a onclick="parent.$.fn.colorbox.close();" href="/registration/" target="_top" class="loginBtn">新規会員登録はこちら</a>
</div>

</div>
</div>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
