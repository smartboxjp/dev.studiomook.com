<?php $title = "パスワード再発行";?>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>

<script type="text/javascript">
    $(function() {
        
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";
            
            err_check_count += check_input_email("member_email");
            
            if(err_check_count!=0)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {
                //$('#form_confirm').submit();
                $('#form_confirm', "body").submit();
                return true;
            }
            
            
        });
                
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);

            if($('#'+$str).val().replace(/　/g," ").match(/^\s+$/))
            {
                err ="<div style='color:#F00;'>正しく入力してください。</div>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                $("#"+$str).focus();
                
                return 1;
            }
            else if($('#'+$str).val()=="")
            {
                err ="<div style='color:#F00;'>正しく入力してください。</div>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }


        //メールチェック
        function check_input_email($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            
            if($('#'+$str_1).val().replace(/　/g," ").match(/^\s+$/))
            {
                err ="<div style='color:#F00;'>正しく入力してください。</div>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                $("#"+$str_1).focus();
                
                return 1;
            }
            else if($('#'+$str_1).val()=="")
            {
                err ="<div style='color:#F00;'>正しく入力してください。</div>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                
                return 1;
            }
            else if(checkIsEmail($('#'+$str_1).val()) == false)
            {
                err ="<div style='color:#F00;'>メールアドレスは半角英数字でご入力ください。</div>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                
                return 1;
            }
            
            return 0;
        }

        //メールチェック
        
        function checkIsEmail(value) {
            if (value.match(/.+@.+\..+/) == null) {
                return false;
            }
            return true;
        }
                
    });
    
//-->
</script>

<div class="loginArea">
<h1 id="popupTitle">パスワード再発行</h1>

<div class="loginAreaIn">

<div class="loginBox">
<h3 class="tit">登録したメールアドレスを入力してください。</h3>
<form action="/mypage/login/password_save.php" name="form_regist" id="form_regist" method="post">
<dl>
    <dt>メールアドレス</dt>
    <dd>
        <? $var = "member_email";?>
        <input name="<?=$var;?>" id="<?=$var;?>" type="email" placeholder="例）info@studiomook.com" maxlength="50">
        <label id="err_<?=$var;?>"></label>
    </dd>
</dl>
<? $var = "form_confirm";?>
<input name="<?php echo $var;?>" id="<?php echo $var;?>" type="submit" value="再発行" class="loginBtn">
<p>メールアドレスも忘れてしまった場合は、お電話でお問い合わせください。TEL 098-911-6202（9:30〜18:00）</p>
</form>
</div>

</div>

</div>


</body>
</html>
