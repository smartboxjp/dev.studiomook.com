<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
    require_once $_SERVER['DOCUMENT_ROOT'].$DOCUMENT_ROOT."/app_include/CommonEmail.php";
    $common_email = new CommonEmail(); //メール関連
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>パスワードを再発行</title>
</head>

<body>

<?
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    if ($member_email == "") 
    {
        $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
    }
    
    $datetime = date("Y-m-d H:i:s");

    $sql = "select member_id from member where member_email ='".$member_email."' and flag_open=1 ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $member_id = $db_result[0]["member_id"];
    }
    else
    {
        $common_connect->Fn_javascript_back("登録されてないメールです。");
    }
    
    $login_pw = $common_connect-> Fn_random_password(10);
    $db_insert = "update member set login_pw='".$login_pw."' ";
    $db_insert .= " where member_id='".$member_id."' ";
    
    $db_result = $common_dao->db_update($db_insert);
    
    $temp_url = global_ssl;

    //Thank youメール
    if ($member_email != "")
    {
        $subject = "『Mr.MOOK』パスワードを再発行しました。";

        $body = "";
        $body = file_get_contents("./mail/password.user.php");
        $body = str_replace("[login_pw]", $login_pw, $body);
        $body = str_replace("[member_email]", $member_email, $body);
        $body = str_replace("[temp_url]", $temp_url, $body);
        $body = str_replace("[datetime]", $datetime, $body);
        $body = str_replace("[global_email_footer]", $global_email_footer, $body);

        $common_email-> Fn_send_utf($member_email."<".$member_email.">",$subject,$body,$global_mail_from,$global_send_mail);
        
    }
       
    $common_email-> Fn_send_utf($global_bcc_mail."<".$global_bcc_mail.">",$subject,$body,$global_mail_from,$global_send_mail);

    $_SESSION['member_id']="";
    $_SESSION['member_name']="";
    /*
    setcookie("pck", "", time() - 1800, "/");
    */
    
    //$common_connect-> Fn_javascript_move("パスワード再発行完了しました。メールを確認してください。", global_ssl."/member/login/");
?>


<script type="text/javascript">
// <![CDATA[
alert("パスワード再発行完了しました。メールを確認してください。");
parent.$.fn.colorbox.close();
parent.location.reload();
//return false;

// ]]>
</script>

</body>
</html>