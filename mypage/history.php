<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/header.php"); ?>
<?
    //ログインチェック
    $common_connect -> Fn_member_check();
    $member_id = $_SESSION["member_id"];
?>
<article>
<section id="mypageTop">
<h1 id="mypageTitle">ー マイページ ー</h1>
</section>

<div id="mypageNav">
<ul>
<li class="home"><a href="./"><img src="/common/img/mypage/ico_home.png"></a></li>
<li><a href="reserved.php">予約の確認</a></li>
<li><a href="information.php">登録者情報</a></li>
<li class="act"><a href="history.php">撮影履歴</a></li>
</ul>
</div>

<section class="mypageTitArea">
<h2 class="mypageTit">撮影履歴</h2>
<p>2017年3月以降に、Mr.MOOKにご来店頂いた撮影履歴となります。</p>
</section>

<?
//予約情報
$arr_db_field = array("reserve_id", "member_id", "member_name_1", "member_name_2", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "reserve_count", "img_1", "status");

$sql = "SELECT ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM reserve where member_id='".$member_id."' " ;
$sql .= " and status=100" ; //撮影完了
$sql .= " and flag_open=1" ;
$sql .= " order by reserve_day desc " ;

$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }

        $yyyy = date("Y",strtotime($reserve_day));
        $mm = date("m",strtotime($reserve_day));
        $dd = date("d",strtotime($reserve_day));

?>
<section class="historyArea">
<section class="historyBox">
<h3 class="subTit"><span><? echo count($db_result)-$db_loop;?>回目</span>撮影日：<? echo $yyyy."年".$mm."月".$dd."日";?></h3>
<div class="historyBoard">
<div class="historyBoardIn">
<p class="img">
<? if($img_1!="") { ?>
<img src="<? echo "/".global_reserve_dir.$reserve_id."/".$img_1; ?>">
<? } ?>
</p>
<dl>
<dt>主役<? echo $reserve_count;?>人</dt>


<?
//主役
$arr_db_field_sub = array("reserve_sub_id", "reserve_id", "reserve_sub_name", "reserve_sub_kana", "sex", "reserve_sub_birth", "cate_menu_id", "cate_menu_name");
$sql_sub = "SELECT ";
foreach($arr_db_field_sub as $val_sub)
{
    $sql_sub .= $val_sub.", ";
}
$sql_sub .= " 1 FROM reserve_sub " ;
$sql_sub .= " where reserve_id='".$reserve_id."'  ";

$db_result_sub = $common_dao->db_query_bind($sql_sub);
if($db_result_sub)
{
    for($db_loop_sub=0 ; $db_loop_sub < count($db_result_sub) ; $db_loop_sub++)
    {
        foreach($arr_db_field_sub as $val_sub)
        {
            $$val_sub = $db_result_sub[$db_loop_sub][$val_sub];
        }
        echo "<dt>".$reserve_sub_name."</dt>";
        echo "<dd>".$cate_menu_name."</dd>";
    }
}
?>

</dl>
</div>
</div>
</section>
<?
    }
}
?>


</section>

</article>

<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/common/include/footer.php"); ?>
