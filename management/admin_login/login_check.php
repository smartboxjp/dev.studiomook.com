<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_connect->h($value);
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title><?=$global_name;?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>
<body>

<?
	$datetime = date("Y-m-d H:i:s");

	if ($admin_id == "" or $admin_pw == "") 
	{
	    $common_connect->Fn_javascript_back("必須項目を正しく入力してください。");
	}
	else
	{
		//ログインチェック
		$sql = "SELECT admin_id, admin_name, admin_pw, flag_open FROM admin where ";
		$sql .= " admin_id='".$admin_id."' and admin_pw='".$admin_pw."'";

		$result_login = $common_dao->db_query_bind ($sql);
		
		if($result_login)
		{
			$db_admin_id = $result_login[0]["admin_id"];
			$db_admin_name = $result_login[0]["admin_name"];
			$db_flag_open = $result_login[0]["flag_open"];
		}
		else
		{
			$common_connect->Fn_javascript_back("IDとパスワードを確認してください。");
		}
		
		if($db_flag_open!="1")
		{
			$common_connect->Fn_javascript_back("ログイン権限がありません。");
		}

		if ($db_admin_id == $admin_id)
		{
			//管理者の場合
			session_start();
			$_SESSION['admin_id']=$db_admin_id;
			$_SESSION['admin_name']=$db_admin_name;
			//最終ログイン
			$sql_update = "update admin set lastlogin_date='".$datetime."' where ";
			$sql_update .= " admin_id='".$admin_id."'";
			$common_dao->db_update ($sql_update);


			if($db_admin_id!="")
			{
				$login_cookie = crypt($db_admin_id."_".$db_login_email."_".$login_pw);


				$sql_update = "update admin set login_cookie='".$login_cookie."' where ";
				$sql_update .= " admin_id='".$admin_id."'";
				$common_dao->db_update ($sql_update);
		
				setcookie("account", $login_cookie, time()+3600*24*31);//暗号化してクッキーに保存, 31日間
			}
			
			$common_connect->Fn_redirect(global_ssl."/management/dashboard/");

		}
	}
?>
</body>
</html>