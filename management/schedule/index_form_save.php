<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連

    //管理者チェック
    $common_connect -> Fn_admin_check();
    
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   
    if($s_yyyy == "" || $s_mm == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }
    
    $datetime = date("Y/m/d H:i:s");
    $lastday = date('t', strtotime($s_yyyy.'-'.$s_mm.'-01'));

    $db_insert = "update schedule set ";
    for($loop=1 ; $loop<=$lastday ; $loop++) {
        $db_insert .= "p_".substr($loop+100, 1)."='".$_POST["p_".$loop]."', ";
    }
    $db_insert .= " up_date='".$datetime."' ";
    $db_insert .= " where yyyymm='".$s_yyyy.substr($s_mm+100, 1)."'";
    $db_result = $common_dao->db_update($db_insert);
    
    $common_connect-> Fn_redirect("./index.php?s_yyyy=".$s_yyyy."&s_mm=".$s_mm);
?>
</body>
</html>