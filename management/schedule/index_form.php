<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    if($s_yyyy=="" || s_mm=="")
    {
        $yyyymm = date("Ym");
    }
    else
    {
        $yyyymm = $s_yyyy.$s_mm;
    }
    
    $sql = "SELECT yyyymm FROM schedule where ";
    $sql .= " yyyymm='".$yyyymm."' ";

    $result_yyyymm = $common_dao->db_query_bind ($sql);
    
    if(!$result_yyyymm)
    {
        $sql_insert = "Insert into schedule (yyyymm) value ('".$yyyymm."') ";
        $common_dao->db_update ($sql_insert);
    }

    //コース
    $sql = " select cate_course_id, cate_course_name FROM cate_course where flag_open=1 order by view_level" ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_id[$db_result[$db_loop]["cate_course_id"]] = $db_result[$db_loop]["cate_course_name"];
        }
    }
?>
<script type="text/javascript">
    $(function(){
      $('#schedule_save').click(function(){
        //var result = confirm('保存しますか？'); 
        //if(result){ 
            $('#form_regist').submit();
        //}
      });
    })
    
</script>

<article>

<section class="table04">
<form action="./index_form.php" method="GET" name="form_search">
<table>
<tr>
<th>表示月</th>
<td width="120"><div class="customSelect">
<?
    $var = "s_yyyy";
    if($$var=="")
    {
        $$var = date("Y");
    }
?>
<select name="s_yyyy">
    <? for($loop=date(Y) ; $loop<date(Y)+3 ; $loop++) { ?>
    <option value="<? echo $loop;?>" <? if($$var == $loop) { echo " selected";}?>><? echo $loop;?>年</option>
    <? } ?>
</select>
</div></td>
<td width="120"><div class="customSelect">
<?
    $var = "s_mm";
    if($$var=="")
    {
        $$var = date("m");
    }
?>
<select name="<? echo $var;?>">
    <? for($loop=1 ; $loop<13 ; $loop++) { ?>
    <option value="<? echo substr($loop+100, 1);?>" <? if(intval($$var) == substr($loop+100, 1)) { echo " selected=true ";}?>><? echo $loop;?>月</option>
    <? } ?>
</select>
</div></td>
<td width="120"><input type="submit" value="表示"></td>
</tr>
</table>
</form>
</section>

<?
    $lastday = date('t', strtotime($s_yyyy.'-'.$s_mm.'-01'));

    $sql = "select  ";
    for($loop=1 ; $loop<=$lastday ; $loop++) {
        $sql .= "p_".substr($loop+100, 1)." , ";
    }
    $sql .= " up_date from schedule ";
    $sql .= " where yyyymm='".$s_yyyy.substr($s_mm+100, 1)."'";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($loop=1 ; $loop<=$lastday ; $loop++) {
            $p[substr($loop+100, 1)] = $db_result[0]["p_".substr($loop+100, 1)];
        }
    }
?>


<form action="./index_form_save.php" method="POST" name="form_write" id="form_regist">
<? $var = "s_yyyy"; ?>
<input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
<? $var = "s_mm"; ?>
<input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">
<section class="table01">
<table>
<thead>
<tr>
<th>日付</th>
<th width="70%">コース</th>
</tr>
</thead>
<tbody>
    <? for($loop=1 ; $loop<=$lastday ; $loop++) { ?>
    <tr>
    <th><? echo $s_yyyy.'/'.$s_mm.'/'.$loop;?>（<? echo $common_connect->Fn_date_day($s_yyyy.'/'.$s_mm.'/'.$loop);?>）</th>
    <td>
        <? foreach($arr_cate_course_id as $key=>$value) {?>
        <label><input name="p_<? echo $loop;?>" type="radio" value="<? echo $key;?>" <? if($p[substr($loop+100, 1)] == $key) { echo " checked ";}?>> <? echo $value;?></label>
        <? } ?>
    </td>
    </tr>
    <? } ?>
</tbody>
</table>

</section>
</form>

</article>


</body>
</html>
