<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    if($s_yyyy=="" || s_mm=="")
    {
        $yyyymm = date("Ym");
    }
    else
    {
        $yyyymm = $s_yyyy.$s_mm;
    }
    
?>

<article>

<section class="table04">
<form action="./" method="GET" name="form_search">
<table>
<tr>
<th>表示月</th>
<td width="120"><div class="customSelect">
<?
    $var = "s_yyyy";
    if($$var=="")
    {
        $$var = date("Y");
    }
?>
<select name="s_yyyy">
    <? for($loop=date(Y) ; $loop<date(Y)+3 ; $loop++) { ?>
    <option value="<? echo $loop;?>" <? if($$var == $loop) { echo " selected";}?>><? echo $loop;?>年</option>
    <? } ?>
</select>
</div></td>
<td width="120"><div class="customSelect">
<?
    $var = "s_mm";
    if($$var=="")
    {
        $$var = date("m");
    }
?>
<select name="<? echo $var;?>">
    <? for($loop=1 ; $loop<13 ; $loop++) { ?>
    <option value="<? echo substr($loop+100, 1);?>" <? if(intval($$var) == substr($loop+100, 1)) { echo " selected=true ";}?>><? echo $loop;?>月</option>
    <? } ?>
</select>
</div></td>
<td width="120"><input type="submit" value="表示"></td>
</tr>
</table>
</form>
</section>



<?
    //コース
    $sql = " select cate_course_id, cate_course_name FROM cate_course where flag_open=1 order by view_level" ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_id[$db_result[$db_loop]["cate_course_id"]] = $db_result[$db_loop]["cate_course_name"];
        }
    }

    //コース時間
    $sql = " select cate_course_time_id, cate_course_id, cate_course_time_from, cate_course_time_to FROM cate_course_time where flag_open=1 order by view_level" ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_time_id[$db_result[$db_loop]["cate_course_id"]][] = $db_result[$db_loop]["cate_course_time_id"];
            $arr_cate_course_time_name[$db_result[$db_loop]["cate_course_id"]][] = substr($db_result[$db_loop]["cate_course_time_from"], 0, 5)."〜".substr($db_result[$db_loop]["cate_course_time_to"], 0, 5);
        }
    }

    $sql = "select cate_course_id, cate_course_name, ";
    $sql .= " (select count(cate_course_time_id) from cate_course_time where cate_course_time.cate_course_id=cate_course.cate_course_id
 and flag_open=1) as cate_course_time_count ";
    $sql .= " FROM cate_course where flag_open=1 order by view_level";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_name[$db_result[$db_loop]["cate_course_id"]] = $db_result[$db_loop]["cate_course_time_count"];
        }
    }

    $yyyy = substr($yyyymm, 0, 4);
    $mm = substr($yyyymm, 4, 2);

    //最後の日
    for ($ld = 28; checkdate($mm,$ld,$yyyy); $ld ++); 
    $lastDay = $ld - 1;

    //日毎のプラン
    //初期化
    $p = array();
    $sql = "select  ";
    for($loop=1 ; $loop<=$lastDay ; $loop++) {
        $sql .= "p_".substr($loop+100, 1)." , ";
    }
    $sql .= " up_date from schedule ";
    $sql .= " where yyyymm='".$yyyy.substr($mm+100, 1)."'";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($loop=1 ; $loop<=$lastDay ; $loop++) {
            $p[substr($loop+100, 1)] = $db_result[0]["p_".substr($loop+100, 1)];
        }
    }

    //予約データ
    $arr_db_field = array("reserve_id", "member_id", "member_name_1", "member_name_2", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "status", "flag_open");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM reserve where left(reserve_day, 7)='".$yyyy."-".$mm."' "; //and flag_open=1
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_reserve[str_replace("-", "", $db_result[$db_loop]["reserve_day"])][$db_result[$db_loop]["cate_course_time_id"]][$db_result[$db_loop]["status"]][$db_result[$db_loop]["flag_open"]] = $db_result[$db_loop]["member_name_1"]." ".$db_result[$db_loop]["member_name_2"];
        }
    }
?>
<section class="table01">
<table>
<thead>
<tr data-href="detail.php">
<th>日付</th>
<th>時間帯</th>
<th width="15%">撮影完了</th>
<th width="15%">予約確定</th>
<th width="15%">キャンセル待ち1</th>
<th width="15%">キャンセル待ち2</th>
<th width="15%">キャンセル待ち3</th>
</tr>
</thead>
<tbody>
<?
    for($day=1 ; $day<=$lastDay ; $day++)
    {
        $dd = substr(($day+100),1);
        if($p[$dd]==5)//撮影なし
        {
?>
    <tr>
    <th colspan="7" class="noreserve"><? echo $yyyy."/".$mm."/".$dd;?>（<? echo $common_connect->Fn_date_day($yyyy.$mm.$dd);?>） <? echo $arr_cate_course_id[$p[$dd]];?></th>
    </tr>
<?

        }
        else
        {
            $max_course = count($arr_cate_course_time_id[$p[$dd]]);
            if($max_course<1) {$max_course=1;}
            $loop = 0;
?>
    <tr data-href="detail.php?yyyymmdd=<? echo $yyyy.$mm.$dd;?>&cate_course_time_id=<? echo $arr_cate_course_time_id[$p[$dd]][$loop];?>">
        <th rowspan="<? echo $max_course;?>" class="daycourse"><? echo $yyyy."/".$mm."/".$dd;?>（<? echo $common_connect->Fn_date_day($yyyy.$mm.$dd);?>）<br /><? echo $arr_cate_course_id[$p[$dd]];?></th>
        <th><? echo $arr_cate_course_time_name[$p[$dd]][$loop];?></th>
        <?
            $var_1 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop]][100][1];
            $var_0 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop]][100][0];
        ?>
        <td <? if($var_1=="" && $var_0=="") { echo " class=\"noreserve\" ";}elseif($var_0!="") { echo " class=\"noreserve keep\" ";}?>><? if($var_0!="") {echo $var_0;} elseif($var_1!="") {echo $var_1;}?></td>
        <?
            $var_1 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop]][1][1];
            $var_0 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop]][1][0];
        ?>
        <td <? if($var_1=="" && $var_0=="") { echo " class=\"noreserve\" ";}elseif($var_0!="") { echo " class=\"noreserve keep\" ";}?>><? if($var_0!="") {echo $var_0;} elseif($var_1!="") {echo $var_1;}?></td>
        <?
            $var_1 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop]][2][1];
            $var_0 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop]][2][0];
        ?>
        <td <? if($var_1=="" && $var_0=="") { echo " class=\"noreserve\" ";}elseif($var_0!="") { echo " class=\"noreserve keep\" ";}?>><? if($var_0!="") {echo $var_0;} elseif($var_1!="") {echo $var_1;}?></td>
        <?
            $var_1 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop]][3][1];
            $var_0 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop]][3][0];
        ?>
        <td <? if($var_1=="" && $var_0=="") { echo " class=\"noreserve\" ";}elseif($var_0!="") { echo " class=\"noreserve keep\" ";}?>><? if($var_0!="") {echo $var_0;} elseif($var_1!="") {echo $var_1;}?></td>
        <?
            $var_1 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop]][4][1];
            $var_0 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop]][4][0];
        ?>
        <td <? if($var_1=="" && $var_0=="") { echo " class=\"noreserve\" ";}elseif($var_0!="") { echo " class=\"noreserve keep\" ";}?>><? if($var_0!="") {echo $var_0;} elseif($var_1!="") {echo $var_1;}?></td>
    </tr>
    <? 
            if($max_course>1){
                for($loop_course=2 ; $loop_course<=$max_course ; $loop_course++) {
    ?>
    <tr data-href="detail.php?yyyymmdd=<? echo $yyyy.$mm.$dd;?>&cate_course_time_id=<? echo $arr_cate_course_time_id[$p[$dd]][$loop_course-1];?>">
        <th><? echo $arr_cate_course_time_name[$p[$dd]][$loop_course-1];?></th>
        <?
            $var_1 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop_course-1]][100][1];
            $var_0 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop_course-1]][100][0];
        ?>
        <td <? if($var_1=="" && $var_0=="") { echo " class=\"noreserve\" ";}elseif($var_0!="") { echo " class=\"noreserve keep\" ";}?>><? if($var_0!="") {echo $var_0;} elseif($var_1!="") {echo $var_1;}?></td>
        <?
            $var_1 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop_course-1]][1][1];
            $var_0 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop_course-1]][1][0];
        ?>
        <td <? if($var_1=="" && $var_0=="") { echo " class=\"noreserve\" ";}elseif($var_0!="") { echo " class=\"noreserve keep\" ";}?>><? if($var_0!="") {echo $var_0;} elseif($var_1!="") {echo $var_1;}?></td>
        <?
            $var_1 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop_course-1]][2][1];
            $var_0 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop_course-1]][2][0];
        ?>
        <td <? if($var_1=="" && $var_0=="") { echo " class=\"noreserve\" ";}elseif($var_0!="") { echo " class=\"noreserve keep\" ";}?>><? if($var_0!="") {echo $var_0;} elseif($var_1!="") {echo $var_1;}?></td>
        <?
            $var_1 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop_course-1]][3][1];
            $var_0 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop_course-1]][3][0];
        ?>
        <td <? if($var_1=="" && $var_0=="") { echo " class=\"noreserve\" ";}elseif($var_0!="") { echo " class=\"noreserve keep\" ";}?>><? if($var_0!="") {echo $var_0;} elseif($var_1!="") {echo $var_1;}?></td>
        <?
            $var_1 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop_course-1]][4][1];
            $var_0 = $arr_reserve[$yyyy.$mm.$dd][$arr_cate_course_time_id[$p[$dd]][$loop_course-1]][4][0];
        ?>
        <td <? if($var_1=="" && $var_0=="") { echo " class=\"noreserve\" ";}elseif($var_0!="") { echo " class=\"noreserve keep\" ";}?>><? if($var_0!="") {echo $var_0;} elseif($var_1!="") {echo $var_1;}?></td>
    </tr>
    <?
            } //for
        }//if
    }//if
    ?>
<?
    }
?>

</tbody>
</table>

</section>

</article>


</body>
</html>
