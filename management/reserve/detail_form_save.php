<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
    $common_image = new CommonImage(); //画像
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    if($reserve_id=="")
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }
    else
    {

        //予約データ
        $arr_db_field = array("reserve_day", "cate_course_time_id");
        
        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM reserve " ;
        $sql .= " where reserve_id='".$reserve_id."'  ";

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $db_loop=0;
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[$db_loop][$val];
            }
            $yyyymmdd = date("Ymd",strtotime($reserve_day));
        }
    }

    $datetime = date("Y/m/d H:i:s");
    $cate_menu_id = $cate_menu_id_main;

    //メニュー
    $sql = " select cate_menu_id, cate_course_id, cate_menu_name FROM cate_menu ";
    //$sql .= " where flag_open=1 " ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_menu_id[$db_result[$db_loop]["cate_menu_id"]] = $db_result[$db_loop]["cate_menu_name"];
        }
    }


    //プラン メニュー
    if($plan_price_id!="")
    { 
        $sql = " select p.cate_menu_id, cate_menu_name, plan_price_name ";
        $sql .= " FROM plan_price p inner join cate_menu c on p.cate_menu_id=c.cate_menu_id ";
        $sql .= " where  ";//p.flag_open=1 and c.flag_open=1 and
        $sql .= " plan_price_id='".$plan_price_id."'" ; 

        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $cate_menu_id = $db_result[0]["cate_menu_id"];
            $cate_menu_name = $db_result[0]["cate_menu_name"];
            $plan_price_name = $db_result[0]["plan_price_name"];
        }
    }

    if($cate_menu_name=="")
    {
        $cate_menu_id = $common_connect->h($_POST["cate_menu_id"][1]);
        $cate_menu_name = $arr_cate_menu_id[$cate_menu_id];
    }
    $arr_db_field = array("reserve_count", "visited_count", "visit_adult", "visit_child", "cate_menu_id", "cate_menu_name", "plan_price_id", "plan_price_name", "pay_method", "reserve_comment", "reserve_price");
    $db_insert = "update reserve set ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= $val."='".$$val."', ";
    }
    $db_insert .= " up_date='".$datetime."' ";
    $db_insert .= " where reserve_id='".$reserve_id."'";
    $db_result = $common_dao->db_update($db_insert);
    
    //仮予約subリセット登録
    $db_del .= " Delete from reserve_sub where reserve_id='".$reserve_id."' ";
    $db_result = $common_dao->db_update($db_del);

    for($loop=1 ; $loop<=$reserve_count; $loop++) {
        $reserve_sub_name = $common_connect->h($_POST["reserve_sub_name"][$loop]);
        $reserve_sub_kana = $common_connect->h($_POST["reserve_sub_kana"][$loop]);
        $sex = $common_connect->h($_POST["sex"][$loop]);
        //日付チェック
        $reserve_sub_birth = "";
        if(checkdate($common_connect->h($_POST["reserve_sub_birth_mm"][$loop]), $common_connect->h($_POST["reserve_sub_birth_dd"][$loop]), $common_connect->h($_POST["reserve_sub_birth_yyyy"][$loop]))) {
            $reserve_sub_birth = $common_connect->h($_POST["reserve_sub_birth_yyyy"][$loop])."-".$common_connect->h($_POST["reserve_sub_birth_mm"][$loop])."-".$common_connect->h($_POST["reserve_sub_birth_dd"][$loop]);
        }
        $cate_menu_id = $common_connect->h($_POST["cate_menu_id"][$loop]);
        $cate_menu_name = $arr_cate_menu_id[$cate_menu_id];

        $arr_db_field = array("reserve_id", "reserve_sub_name", "reserve_sub_kana", "sex", "reserve_sub_birth", "cate_menu_id", "cate_menu_name");

        $db_insert = "insert into reserve_sub ( ";
        $db_insert .= " reserve_sub_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " regi_date, up_date ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '".$loop."', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '$datetime', '$datetime')";

        $db_result = $common_dao->db_update($db_insert);
    }

    //オプションリセット
    $option_price_all = 0;
    $db_del = "delete from reserve_option  ";
    $db_del .= " where reserve_id='".$reserve_id."' " ;
    $common_dao->db_update($db_del); 

    foreach($_POST["reserve_option_name"] as $key => $value)
    { 
        $$key = $common_connect->h($value);

        if($value!="")
        {
            $db_insert = "insert into reserve_option ( ";
            $db_insert .= " reserve_option_id, reserve_id, reserve_option_name, reserve_option_price, ";
            $db_insert .= " regi_date, up_date ";
            $db_insert .= " ) values ( ";
            $db_insert .= " '".($key+1)."', '".$reserve_id."', '".$value."', '".$_POST["reserve_option_price"][$key]."', ";
            $db_insert .= " '$datetime', '$datetime')";
            $db_result = $common_dao->db_update($db_insert);
            $option_price_all += $_POST["reserve_option_price"][$key];
        }
    }
    
    $reserve_price = ($plan_price) + $option_price_all;

    $db_up = "update reserve set reserve_price='".$reserve_price."', ";
    $db_up .= " up_date='".$datetime."' ";
    $db_up .= " where reserve_id='".$reserve_id."'";
    $db_result = $common_dao->db_update($db_up);

    //Folder生成
    $save_dir = $global_path.global_reserve_dir.$reserve_id."/";

    //Folder生成
    $common_image -> create_folder ($save_dir);
    
    $new_end_name="_1";
    $var = "img_1";
    $text_var = "text_".$var;
    $fname_new_name[1] = $common_image -> img_save($var, $common_connect, $save_dir, $new_end_name, $reserve_id, $$text_var, "300");//300
    
    $dbup = "update reserve set ";
    for($loop = 1 ; $loop<2 ; $loop++)
    {
        $dbup .= " img_".$loop."='".$fname_new_name[$loop]."',";
    }
    $dbup .= " up_date='$datetime' where reserve_id='".$reserve_id."'";
    
    $db_result = $common_dao->db_update($dbup);

    $common_connect-> Fn_redirect("/management/reserve/detail.php?yyyymmdd=".$yyyymmdd."&cate_course_time_id=".$cate_course_time_id);

?>