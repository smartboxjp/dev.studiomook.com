<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連


    //管理者チェック
    $common_connect -> Fn_admin_check();

    header("Content-Type: application/vnd.ms-excel;"); 
    header("Content-Disposition: attachment; filename=reserve_".date("YmdHi").".csv"); 
    header("Content-Transfer-Encoding: text/plain"); 
    header("Pragma: no-cache"); 
    header("Expires: 0"); 


    //プラン
    $sql = " select plan_price_id, m.cate_menu_id, plan_price_name, plan_price, m.cate_menu_name ";
    $sql .= " FROM plan_price p inner join cate_menu m on m.cate_menu_id=p.cate_menu_id ";
    $sql .= " where m.cate_course_id=1 and m.flag_open=1 and p.flag_open=1 ";
    $sql .= " order by p.view_level ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_plan_price_id[$db_result[$db_loop]["cate_menu_id"]][$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["plan_price_name"];
            $arr_plan_price[$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["plan_price"];
            $arr_plan_price_name[$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["cate_menu_name"]."　".$db_result[$db_loop]["plan_price_name"];
        }
    }
    
    $enter = "\r";
    $csv_list = "";
    $csv_list .= "予約ID,登録日,ステータス,顧客の管理ID,代表者,TEL,";
    $csv_list .= "予約日,主役の人数,スタジオご利用,来店人数,";
    $csv_list .= "撮影メニュー,プラン,オプション料金,基本料金,合計,支払い方法,備考,";
    for($loop=1 ; $loop<=5 ; $loop++)
    {
        $csv_list .= "主役名".$loop.",主役ふりがな".$loop.",主役性別".$loop.",主役歳".$loop.",誕生日".$loop.",メニュー".$loop.",";
    }
    $csv_list .= $enter;

    //予約データ
    $arr_db_field = array("reserve_id", "member_id", "member_name_1", "member_name_2", "regi_date", "status");
    $arr_db_field = array_merge($arr_db_field, array("reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "cate_menu_id", "cate_menu_name", "plan_price_id", "plan_price_name", "visited_count", "visit_adult", "visit_child", "reserve_count", "reserve_price", "img_1", "reserve_comment", "pay_method"));
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM reserve " ;
    $sql .= " where flag_open=1  ";
    $sql .= " order by reserve_day desc ";

    $db_result_reserve = $common_dao->db_query_bind($sql);
    if($db_result_reserve)
    {
        for($db_loop_reserve=0 ; $db_loop_reserve < count($db_result_reserve) ; $db_loop_reserve++)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result_reserve[$db_loop_reserve][$val];
            }


            //会員情報
            $arr_db_field_member = array("member_name_1", "member_name_2", "member_name_kana", "tel", "member_email", "post_num", "address_1", "address_2", "address_3");
            $sql_member = "SELECT ";
            foreach($arr_db_field_member as $val)
            {
                $sql_member .= $val.", ";
            }
            $sql_member .= " 1 FROM member " ;
            $sql_member .= " where member_id='".$member_id."'  ";

            $db_result_member = $common_dao->db_query_bind($sql_member);
            if($db_result_member)
            {
                $db_loop_member=0;
                foreach($arr_db_field_member as $val)
                {
                    $$val = $db_result_member[$db_loop_member][$val];
                }
            }

            $sum_price = 0;

            $csv_list .= "\"".$reserve_id."\",\"".$regi_date."\",\"".$global_reserve_admin_view[$status]."\",\"".$member_id."\",\"".$member_name_1."\",\"".$tel."\",";
            $csv_list .= "\"".$reserve_day."\",\"".$reserve_count."\",\"".$visited_count."\",\"".$visit_adult."\",";
            $csv_list .= "\"".$cate_menu_name."\",\"".$plan_price_name."\",\"".$arr_plan_price[$plan_price_id]."\",";

            //オプション
            $sql_option = "SELECT sum(reserve_option_price) as sum_reserve_option_price FROM reserve_option " ;
            $sql_option .= " where reserve_id='".$reserve_id."' ";

            $db_result_option = $common_dao->db_query_bind($sql_option);
            if($db_result_option)
            {
                $csv_list .= "\"".$db_result_option[0]["sum_reserve_option_price"]."\",";
            }
            else
            {
                $csv_list .= "\"0\",";
            }
            $csv_list .= "\"".$reserve_price."\",\"".$pay_method."\",\"".$reserve_comment."\",";


            //主役
            $csv_list_sub = "";
            $arr_db_field_sub = array("reserve_sub_id", "reserve_id", "reserve_sub_name", "reserve_sub_kana", "sex", "reserve_sub_birth", "cate_menu_id", "cate_menu_name");
            $sql_sub = "SELECT ";
            foreach($arr_db_field_sub as $val)
            {
                $sql_sub .= $val.", ";
            }
            $sql_sub .= " 1 FROM reserve_sub " ;
            $sql_sub .= " where reserve_id='".$reserve_id."'  ";

            $db_result_sub = $common_dao->db_query_bind($sql_sub);
            if($db_result_sub)
            {
                for($db_loop_sub=0 ; $db_loop_sub < count($db_result_sub) ; $db_loop_sub++)
                {
                    $reserve_sub_name="";
                    $sex="";
                    $reserve_sub_birth="";
                    $cate_menu_name="";
                    $reserve_sub_kana="";
                    foreach($arr_db_field_sub as $val)
                    {
                        $$val = $db_result_sub[$db_loop_sub][$val];
                    }
                    $csv_list_sub .= "\"".$reserve_sub_name."\",\"".$reserve_sub_kana."\",\"".$sex."\",\"".floor((date("Ymd")-date("Ymd", strtotime($reserve_sub_birth)))/10000)."\",\"".$reserve_sub_birth."\",\"".$cate_menu_name."\",";
                }
            }

            $csv_list .=  $csv_list_sub.$enter;
        }
    }

    //echo mb_convert_encoding($csv_list, 'SJIS', 'UTF-8');
    echo pack('C*',0xEF,0xBB,0xBF).$csv_list; //BOM付きのCSV
?>