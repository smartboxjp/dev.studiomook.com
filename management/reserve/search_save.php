<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    if($member_id== "" || $status == "" || $yyyymmdd == "" || $cate_course_time_id == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }
    $datetime = date("Y/m/d H:i:s");

    $yyyy = date("Y",strtotime($yyyymmdd));
    $mm = date("m",strtotime($yyyymmdd));
    $dd = date("d",strtotime($yyyymmdd));
    //日付チェック
    if(!checkdate($mm, $dd, $yyyy))
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }

    //30分以内の仮予約削除
    $del_time = date("Y-m-d H:i", strtotime('-31 minute'));
    $db_del = "Delete from reserve where flag_open=0 and regi_date<'".$del_time."' ";
    $common_dao->db_update($db_del);
    


    //同じ日のすでに予約されてるかをチェック
    $sql = "SELECT flag_open, reserve_id " ;
    $sql .= " FROM reserve " ;
    $sql .= " where member_id='".$member_id."' and reserve_day='".date("Y-m-d",strtotime($yyyymmdd))."' " ;
    $sql .= " and cate_course_time_id='".$cate_course_time_id."' " ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        if($db_result[0]["flag_open"]=="1")
        {
            $common_connect -> Fn_javascript_back("すでに予約されています。");
        }
        //すでに入力中のデータがある場合
        elseif($db_result[0]["flag_open"]=="0")
        {
            $common_connect -> Fn_javascript_back("登録中の予約があります。");
        }
    }

    /*
    //同じ時間代予約件数確認
    $reserve_count = 0;
    $sql = "SELECT max(status) as reserve_count " ;
    $sql .= " FROM reserve " ;
    $sql .= " where reserve_day='".date("Y-m-d",strtotime($yyyymmdd))."' " ;
    $sql .= " and cate_course_time_id='".$cate_course_time_id."' " ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $reserve_count = $db_result[0]["reserve_count"];
        if($reserve_count>4)
        {
            $common_connect -> Fn_javascript_back("予約空きがありません。");
        }
    }
    */

    //会員情報
    $arr_db_field = array("member_name_1", "member_name_2", "tel", "member_email");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM member where member_id='".$member_id."' and flag_open=1" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }
    if($member_name_1=="")
    {
        $common_connect -> Fn_javascript_back("会員情報が正しくありません。");
    }

    //コース時間情報
    $arr_db_field = array("cate_course_id", "cate_course_time_from", "cate_course_time_to");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM cate_course_time where cate_course_time_id='".$cate_course_time_id."' and flag_open=1" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }

    //コース情報
    $arr_db_field = array("cate_course_name");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM cate_course where cate_course_id='".$cate_course_id."' and flag_open=1" ;
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[0][$val];
        }
    }

    $flag_open = 1;//1:登録完了 0:仮予約
    $status = $reserve_count+1; //1:予約、2：キャンセル待ち1、2：キャンセル待ち2、2：キャンセル待ち3
    $reserve_day = date("Y-m-d",strtotime($yyyymmdd));
    
    //array
    $arr_db_field = array("member_id", "member_name_1", "member_name_2", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "status", "flag_open");

    //基本情報
    $db_insert = "insert into reserve ( ";
    $db_insert .= " reserve_id, ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= $val.", ";
    }
    $db_insert .= " regi_date, up_date ";
    $db_insert .= " ) values ( ";
    $db_insert .= " '', ";
    foreach($arr_db_field as $val)
    {
        $db_insert .= " '".$$val."', ";
    }
    $db_insert .= " '$datetime', '$datetime')";
    $common_dao->db_update($db_insert);

    if ($reserve_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id"; 
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $reserve_id = $db_result[0]["last_id"];
        }
    }

    $common_connect-> Fn_redirect("/management/reserve/detail_form.php?reserve_id=".$reserve_id);
?>


</body>
</html>