

<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="robots" content="noindex,nofollow">
<title>[Mr.MOOK管理画面]</title>

<link rel="stylesheet" href="/management/common/css/management.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/management/common/js/script.js"></script>
<script src="/management/common/js/jquery.easing.1.3.js"></script>
<script src="/management/common/js/colorbox/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="/management/common/js/colorbox/colorbox.css">
<!--▼favicon-->
<link href="https://fonts.googleapis.com/css?family=Crimson+Text" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link rel="apple-touch-icon" sizes="180x180" href="/common/img/share/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/common/img/share/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/common/img/share/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/common/img/share/favicon/manifest.json">
<link rel="mask-icon" href="/common/img/share/favicon/safari-pinned-tab.svg" color="#26afaf">
<meta name="theme-color" content="#ffffff">
<!--▲favicon-->

</head>

<body>
<header>

<ul class="statusList">
<li class="reserved"><a href="/management/reserve/"><span>
13</span></a><p class="tooltip">予約確定</p></li>
<li class="waiting"><a href="/management/reserve/"><span>
23</span></a><p class="tooltip">キャンセル待ち</p></li>
<li class="check"><a href="/management/reserve/"><span>
0</span></a><p class="tooltip">処理予定</p></li>
</ul>

<ul class="operateList">
<li class="logout"><a href="/management/admin_login/logout.php"><span class="tooltip">ログアウト</span></a></li>
            <li class="csv"><a href="/management/reserve/reserve_csv.php"><span class="tooltip">csvダウンロード</span></a></li>
            </ul>


</header>
<nav>
<p></p>
<ul>
<li class="dashboard"><a href="/management/dashboard/">ダッシュボード</a></li>
<li class="customer"><a href="/management/customer/">顧客管理</a></li>
<li class="reserve act"><a href="/management/reserve/">予約管理</a></li>
<li class="schedule"><a href="/management/schedule/">スケジュール管理</a></li>
<li class="data"><a href="/management/data/">データ集計</a></li>
</ul>
</nav>



<article>

<section class="table04">
<form action="./index.php" method="GET" name="form_search">
<table>
<tr>
<th>表示月</th>
<td width="120"><div class="customSelect">
<select name="s_yyyy">
        <option value="2017"  selected>2017年</option>
        <option value="2018" >2018年</option>
        <option value="2019" >2019年</option>
    </select>
</div></td>
<td width="120"><div class="customSelect">
<select name="s_mm">
        <option value="01" >1月</option>
        <option value="02" >2月</option>
        <option value="03"  selected=true >3月</option>
        <option value="04" >4月</option>
        <option value="05" >5月</option>
        <option value="06" >6月</option>
        <option value="07" >7月</option>
        <option value="08" >8月</option>
        <option value="09" >9月</option>
        <option value="10" >10月</option>
        <option value="11" >11月</option>
        <option value="12" >12月</option>
    </select>
</div></td>
<td width="120"><input type="submit" value="表示"></td>
</tr>
</table>
</form>
</section>



<section class="table01">
<table>
<thead>
<tr data-href="detail.php">
<th>日付</th>
<th>時間帯</th>
<th width="15%">撮影完了</th>
<th width="15%">予約確定</th>
<th width="15%">キャンセル待ち1</th>
<th width="15%">キャンセル待ち2</th>
<th width="15%">キャンセル待ち3</th>
</tr>
</thead>
<tbody>
    <tr data-href="detail.php?yyyymmdd=20170301&cate_course_time_id=">
        <th rowspan="1" class="daycourse">2017/03/01（水）<br /></th>
        <th></th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170302&cate_course_time_id=">
        <th rowspan="1" class="daycourse">2017/03/02（木）<br /></th>
        <th></th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170303&cate_course_time_id=">
        <th rowspan="1" class="daycourse">2017/03/03（金）<br /></th>
        <th></th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170304&cate_course_time_id=">
        <th rowspan="1" class="daycourse">2017/03/04（土）<br /></th>
        <th></th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
<!--予約キープとタイムアップ-------------------------------------------------------------------------------------------------------------->
        <tr data-href="detail.php?yyyymmdd=20170305&cate_course_time_id=">
        <th rowspan="1" class="daycourse">2017/03/05（日）<br /></th>
        <th></th>
        <td  class="noreserve" ></td>
        <td  class="noreserve keep" >秋元 智道 [KEEP]</td>
        <td  class="noreserve" >田中 隆弘 [TIMEUP]</td>
        <td >内田 綾</td>
        <td  class="noreserve" ></td>
<!--予約キープとタイムアップ-------------------------------------------------------------------------------------------------------------->
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170306&cate_course_time_id=1">
        <th rowspan="2" class="daycourse">2017/03/06（月）<br />スタンダード</th>
        <th>09:30〜12:30</th>
        <td  class="noreserve" ></td>
        <td >坂下 徹</td>
        <td >糸洲 慧美</td>
        <td >内田 綾</td>
        <td >砂川 美樹</td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170306&cate_course_time_id=2">
        <th>14:00〜17:00</th>
        <td  class="noreserve" ></td>
        <td >上條 亜弓</td>
        <td >田中 めぐみ</td>
        <td >成岡 里美</td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170307&cate_course_time_id=1">
        <th rowspan="2" class="daycourse">2017/03/07（火）<br />スタンダード</th>
        <th>09:30〜12:30</th>
        <td  class="noreserve" ></td>
        <td >佐久川 綾</td>
        <td >大城 英恵</td>
        <td >砂川 美樹</td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170307&cate_course_time_id=2">
        <th>14:00〜17:00</th>
        <td  class="noreserve" ></td>
        <td >下地 由佳</td>
        <td >阿波連 大輔</td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170308&cate_course_time_id=3">
        <th rowspan="3" class="daycourse">2017/03/08（水）<br />ショート</th>
        <th>09:30〜10:30</th>
        <td  class="noreserve" ></td>
        <td >渡口 祐河</td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170308&cate_course_time_id=4">
        <th>11:00〜12:00</th>
        <td  class="noreserve" ></td>
        <td >村田 斉子</td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170308&cate_course_time_id=5">
        <th>14:00〜15:00</th>
        <td  class="noreserve" ></td>
        <td >仲村 玲央</td>
        <td >波多江 絵美</td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170309&cate_course_time_id=1">
        <th rowspan="2" class="daycourse">2017/03/09（木）<br />スタンダード</th>
        <th>09:30〜12:30</th>
        <td  class="noreserve" ></td>
        <td >栗原 里奈</td>
        <td >糸洲 慧美</td>
        <td >内田 綾</td>
        <td >砂川 美樹</td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170309&cate_course_time_id=2">
        <th>14:00〜17:00</th>
        <td  class="noreserve" ></td>
        <td >伊智 睦子</td>
        <td >波多江 絵美</td>
        <td >摩文仁 祐樹</td>
        <td >成岡 里美</td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170310&cate_course_time_id=3">
        <th rowspan="3" class="daycourse">2017/03/10（金）<br />ショート</th>
        <th>09:30〜10:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170310&cate_course_time_id=4">
        <th>11:00〜12:00</th>
        <td  class="noreserve" ></td>
        <td >大城 英恵</td>
        <td >村田 斉子</td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170310&cate_course_time_id=5">
        <th>14:00〜15:00</th>
        <td  class="noreserve" ></td>
        <td >鈴木 陽祐</td>
        <td >成岡 里美</td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr>
    <th colspan="7" class="noreserve">2017/03/11（土） 撮影なし</th>
    </tr>
    <tr data-href="detail.php?yyyymmdd=20170312&cate_course_time_id=1">
        <th rowspan="2" class="daycourse">2017/03/12（日）<br />スタンダード</th>
        <th>09:30〜12:30</th>
        <td  class="noreserve" ></td>
        <td >肥田 絵里</td>
        <td >糸洲 慧美</td>
        <td >田中 めぐみ</td>
        <td >内田 綾</td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170312&cate_course_time_id=2">
        <th>14:00〜17:00</th>
        <td  class="noreserve" ></td>
        <td >黒木 麻梨</td>
        <td >島野 歩美</td>
        <td >池原 健太</td>
        <td >宮里 彩</td>
    </tr>
        <tr>
    <th colspan="7" class="noreserve">2017/03/13（月） 撮影なし</th>
    </tr>
    <tr data-href="detail.php?yyyymmdd=20170314&cate_course_time_id=1">
        <th rowspan="2" class="daycourse">2017/03/14（火）<br />スタンダード</th>
        <th>09:30〜12:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170314&cate_course_time_id=2">
        <th>14:00〜17:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170315&cate_course_time_id=3">
        <th rowspan="3" class="daycourse">2017/03/15（水）<br />ショート</th>
        <th>09:30〜10:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170315&cate_course_time_id=4">
        <th>11:00〜12:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170315&cate_course_time_id=5">
        <th>14:00〜15:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170316&cate_course_time_id=1">
        <th rowspan="2" class="daycourse">2017/03/16（木）<br />スタンダード</th>
        <th>09:30〜12:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170316&cate_course_time_id=2">
        <th>14:00〜17:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170317&cate_course_time_id=3">
        <th rowspan="3" class="daycourse">2017/03/17（金）<br />ショート</th>
        <th>09:30〜10:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170317&cate_course_time_id=4">
        <th>11:00〜12:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170317&cate_course_time_id=5">
        <th>14:00〜15:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr>
    <th colspan="7" class="noreserve">2017/03/18（土） 撮影なし</th>
    </tr>
    <tr data-href="detail.php?yyyymmdd=20170319&cate_course_time_id=1">
        <th rowspan="2" class="daycourse">2017/03/19（日）<br />スタンダード</th>
        <th>09:30〜12:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170319&cate_course_time_id=2">
        <th>14:00〜17:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr>
    <th colspan="7" class="noreserve">2017/03/20（月） 撮影なし</th>
    </tr>
    <tr>
    <th colspan="7" class="noreserve">2017/03/21（火） 撮影なし</th>
    </tr>
    <tr data-href="detail.php?yyyymmdd=20170322&cate_course_time_id=3">
        <th rowspan="3" class="daycourse">2017/03/22（水）<br />ショート</th>
        <th>09:30〜10:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170322&cate_course_time_id=4">
        <th>11:00〜12:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170322&cate_course_time_id=5">
        <th>14:00〜15:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170323&cate_course_time_id=1">
        <th rowspan="2" class="daycourse">2017/03/23（木）<br />スタンダード</th>
        <th>09:30〜12:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170323&cate_course_time_id=2">
        <th>14:00〜17:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170324&cate_course_time_id=3">
        <th rowspan="3" class="daycourse">2017/03/24（金）<br />ショート</th>
        <th>09:30〜10:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170324&cate_course_time_id=4">
        <th>11:00〜12:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170324&cate_course_time_id=5">
        <th>14:00〜15:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr>
    <th colspan="7" class="noreserve">2017/03/25（土） 撮影なし</th>
    </tr>
    <tr data-href="detail.php?yyyymmdd=20170326&cate_course_time_id=1">
        <th rowspan="2" class="daycourse">2017/03/26（日）<br />スタンダード</th>
        <th>09:30〜12:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170326&cate_course_time_id=2">
        <th>14:00〜17:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr>
    <th colspan="7" class="noreserve">2017/03/27（月） 撮影なし</th>
    </tr>
    <tr data-href="detail.php?yyyymmdd=20170328&cate_course_time_id=1">
        <th rowspan="2" class="daycourse">2017/03/28（火）<br />スタンダード</th>
        <th>09:30〜12:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170328&cate_course_time_id=2">
        <th>14:00〜17:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170329&cate_course_time_id=3">
        <th rowspan="3" class="daycourse">2017/03/29（水）<br />ショート</th>
        <th>09:30〜10:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170329&cate_course_time_id=4">
        <th>11:00〜12:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170329&cate_course_time_id=5">
        <th>14:00〜15:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170330&cate_course_time_id=1">
        <th rowspan="2" class="daycourse">2017/03/30（木）<br />スタンダード</th>
        <th>09:30〜12:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170330&cate_course_time_id=2">
        <th>14:00〜17:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170331&cate_course_time_id=3">
        <th rowspan="3" class="daycourse">2017/03/31（金）<br />ショート</th>
        <th>09:30〜10:30</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170331&cate_course_time_id=4">
        <th>11:00〜12:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
        <tr data-href="detail.php?yyyymmdd=20170331&cate_course_time_id=5">
        <th>14:00〜15:00</th>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
        <td  class="noreserve" ></td>
    </tr>
    
</tbody>
</table>

</section>

</article>


</body>
</html>
