<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
    $max_count = 5;
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }


    //予約データ
    $arr_db_field = array("reserve_id", "member_id", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "plan_price_id", "plan_price_name", "reserve_count", "visited_count", "visit_adult", "visit_child", "pay_method", "reserve_comment", "cate_menu_id", "cate_menu_name", "reserve_price", "img_1", "status", "regi_date");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM reserve " ;
    $sql .= " where reserve_id='".$reserve_id."'  ";
    $sql .= " and flag_open='1'  ";
    echo $sql;

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $db_loop=0;
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }
    }
    $course_time = substr($db_result[$db_loop]["cate_course_time_from"], 0, 5)." 〜 ".substr($db_result[$db_loop]["cate_course_time_to"], 0, 5);

    //会員情報
    $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "tel", "member_email", "post_num", "address_1", "address_2", "address_3");
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM member " ;
    $sql .= " where member_id='".$member_id."'  ";

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $db_loop=0;
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }
    }

    $yyyy = date("Y",strtotime($reserve_day));
    $mm = date("m",strtotime($reserve_day));
    $dd = date("d",strtotime($reserve_day));

    //プラン
    $sql = " select plan_price_id, m.cate_menu_id, plan_price_name, plan_price, m.cate_menu_name ";
    $sql .= " FROM plan_price p inner join cate_menu m on m.cate_menu_id=p.cate_menu_id ";
    $sql .= " where m.cate_course_id='".$cate_course_id."' ";//and m.flag_open=1 and p.flag_open=1 ";
    $sql .= " order by p.view_level ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_plan_price_id[$db_result[$db_loop]["cate_menu_id"]][$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["plan_price_name"];
            $arr_plan_price[$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["plan_price"];
            $arr_plan_price_name[$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["cate_menu_name"]."　".$db_result[$db_loop]["plan_price_name"];
        }
    }

    //メニュー
    $sql = " select cate_menu_id, cate_course_id, cate_menu_name FROM cate_menu ";
    $sql .= " where cate_course_id='".$cate_course_id."' order by view_level" ; //flag_open=1 and 

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_menu_id[$db_result[$db_loop]["cate_menu_id"]] = $db_result[$db_loop]["cate_menu_name"];
        }
    }

    $arr_sex = array();
    $arr_sex[] = "男";
    $arr_sex[] = "女";

    $arr_birth_yyyy = array();
    for($loop_yyyy=(date("Y")+1);$loop_yyyy>1930;$loop_yyyy--) {
        $arr_birth_yyyy[] = $loop_yyyy;
    }

    $arr_birth_mm = array();
    for($loop_mm=1;$loop_mm<13;$loop_mm++) {
        $arr_birth_mm[] = $loop_mm;
    }

    $arr_birth_dd = array();
    for($loop_dd=1;$loop_dd<32;$loop_dd++) {
        $arr_birth_dd[] = $loop_dd;
    }
?>
<script type="text/javascript">
    $(function() {
        
        $('#reserve_detail_save').click(function() {
            err_default = "";
            err_check_count = 0;
            bgcolor_default = "transparent";
            bgcolor_err = "#FFCCCC";
            background = "background-color";
            
            err_check_count += check_input_sel("reserve_count");
            err_check_count += check_input_sel("visited_count");
            err_check_count += check_input_sel("visit_adult");
            err_check_count += check_input_sel("visit_child");

            max_i = $("select[name=reserve_count]").children(':selected').val();


            for (var loop=1; loop<=max_i; loop++)
            {
                err_check_count += check_input("cate_menu_id_"+loop);
                err_check_count += check_input("reserve_sub_name_"+loop);
                err_check_count += check_input("reserve_sub_kana_"+loop);
                err_check_count += check_input("sex_"+loop);
                err_check_count += check_input_birth("reserve_sub_birth_"+loop, "reserve_sub_birth_yyyy_"+loop, "reserve_sub_birth_mm_"+loop, "reserve_sub_birth_dd_"+loop);
            }
            
            if(err_check_count!=0)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {
                $('#form_regist').submit();
                //$('#reserve_detail_save', "body").submit();
                return true;
            }
            
            
        });
                
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);

            if($('#'+$str).val().replace(/　/g," ").match(/^\s+$/))
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                $("#"+$str).focus();
                
                return 1;
            }
            else if($('#'+$str).val()=="")
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }

        function check_input_birth($str, $str1, $str2, $str3) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str1).css(background,bgcolor_default);
            $("#"+$str2).css(background,bgcolor_default);
            $("#"+$str3).css(background,bgcolor_default);


            if(!check_date($('#'+$str1).val(), $('#'+$str2).val(), $('#'+$str3).val()))
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str1).css(background,bgcolor_err);
                $("#"+$str2).css(background,bgcolor_err);
                $("#"+$str3).css(background,bgcolor_err);
                $("#"+$str1).focus();
                
                return 1;
            }

            return 0;
        }
                
        function check_input_sel($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);

            if($('#'+$str).val().replace(/　/g," ").match(/^\s+$/))
            {
                err ="<span class='error'>正しく選択してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                $("#"+$str).focus();
                
                return 1;
            }
            else if($('#'+$str).val()=="")
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                
                return 1;
            }
            return 0;
        }

        function check_date(vYear, vMonth, vDay) {
            vMonth = (vMonth-1);
        　// 月,日の妥当性チェック
        　if(vMonth >= 0 && vMonth <= 11 && vDay >= 1 && vDay <= 31){
        　　var vDt = new Date(vYear, vMonth, vDay);
        　　if(isNaN(vDt)){
        　　　return false;
        　　}else if(vDt.getFullYear() == vYear && vDt.getMonth() == vMonth && vDt.getDate() == vDay){　　
        　　　return true;
        　　}else{
        　　　return false;
        　　}
        　}else{
        　　return false;
        　}
        }
       
    });


    $(function(){
        <?
        for($loop=2 ; $loop<=$max_count ; $loop++) {
            //修正の場合表示
            if($loop>$reserve_count) {
        ?>
        $('.sel_<? echo $loop;?>').css("display","none");
        <?
            }
        }
        ?>
        $('#reserve_count').change(function(){
            <? for($loop=2 ; $loop<=$max_count ; $loop++) { ?>
            $('.sel_<? echo $loop;?>').css("display","none");
            <? } ?>
            max_i = $("select[name=reserve_count]").children(':selected').val();
            for (var i=1; i<=max_i; i++)
            {
                $('.sel_'+i).css("display","table-row");
            }
        });

        <? 
        $arr_plan_price_key = "";
        $arr_plan_price_value = "";
        foreach($arr_plan_price as $key => $value) { 
            $arr_plan_price_key .= "\"".$key."\",";
            $arr_plan_price_value .= "\"".$value."\",";
            $arr_plan_price_name_value .= "\"".$arr_plan_price_name[$key]."\",";
            
        }
        ?>
        var arr_plan_price_key = [<? echo rtrim($arr_plan_price_key, ',');?>]; 
        var arr_plan_price_value = [<? echo rtrim($arr_plan_price_value, ',');?>]; 
        var arr_plan_price_name_value = [<? echo rtrim($arr_plan_price_name_value, ',');?>]; 


        //textbox追加
        $('#addForm').click(function(){
            if ($('form .option').children().length < 50) {
                $('form .option').append('<input name="reserve_option_name[]" type="text" placeholder="オプション商品"> <input name="reserve_option_price[]" type="text" placeholder="オプション料金" class="option_price">');
            }
        });


        //撮影メニュー&プランが選択された場合
        $('.class_plan').click(function(){
            plan_price_id = $(this).attr("title");
            $('#plan_price_id').val(plan_price_id);
            $('#txt_plan_text').html(arr_plan_price_name_value[$.inArray(plan_price_id, arr_plan_price_key)]);
            //プラン金額
            $('#txt_plan_price').html("¥"+arr_plan_price_value[$.inArray(plan_price_id, arr_plan_price_key)].replace( /(\d)(?=(\d\d\d)+(?!\d))/g, '$1,'));
            $('#plan_price').val(arr_plan_price_value[$.inArray(plan_price_id, arr_plan_price_key)]);
            $('#txt_reserve_price').html("登録ボタンを押してから計算されます。");
            parent.$.fn.colorbox.close();
        });


    })

    function fnImgDel(i, j, k) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './img_one_del.php?reserve_id='+i+'&img='+j+'&img_name='+k;
        } 
    }
</script>

<article>


<form action="./detail_form_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data">
<? $var = "reserve_id";?>
<input name="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
<section class="table01">
<table>
<thead>
<tr>
<th colspan="3"><? echo $yyyy;?>/<? echo $mm;?>/<? echo $dd;?> <? echo $cate_course_name;?> [<? echo $course_time;?>]</th>
</tr>
</thead>
<tbody>
<tr>
<th rowspan="20" width="120">
<? echo $global_reserve_admin_view[$status];?>
</th>
<th width="200">代表者</th>
<td data-href="/management/customer/detail.php?member_id=<? echo $member_id;?>"><? echo $member_name_1." ".$member_name_2;?></td>
</tr>
<tr>
<th>TEL</th>
<td><? echo $tel;?></td>
</tr>
<tr>
<th>予約日</th>
<td><? echo str_replace("-", "/", $regi_date);?></td>
</tr>
<tr>
<th>主役の人数</th>
<td>
<? $var = "reserve_count";?>
<select name="<? echo $var;?>" id="<? echo $var;?>">
<? for($loop=1 ; $loop<=$max_count; $loop++) { ?>
<option value="<? echo $loop;?>" <? if($$var==$loop) { echo " selected ";}?>><? if($loop==$max_count) {echo $loop."〜";} else {echo $loop;}?></option>
<? } ?>
</select> 人
<label id="err_<?=$var;?>"></label>
</td>
</tr>


<!--▼主役-->
<?
for($loop=1 ; $loop<=$max_count; $loop++) { 

    $reserve_sub_name = "";
    $reserve_sub_kana = "";
    $sex = "";
    $reserve_sub_birth = "";
    $birth_yyyy = "";
    $birth_mm = "";
    $birth_dd = "";
    $cate_menu_id = "";
    $cate_menu_name = "";

    $sql = " select reserve_sub_name, reserve_sub_kana, sex, reserve_sub_birth, cate_menu_id, cate_menu_name FROM reserve_sub ";
    $sql .= " where reserve_id='".$reserve_id."' and reserve_sub_id='".$loop."' " ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $reserve_sub_name = $db_result[0]["reserve_sub_name"];
        $reserve_sub_kana = $db_result[0]["reserve_sub_kana"];
        $sex = $db_result[0]["sex"];
        $reserve_sub_birth = $db_result[0]["reserve_sub_birth"];
        $birth_yyyy = date("Y",strtotime($reserve_sub_birth));
        $birth_mm = date("m",strtotime($reserve_sub_birth));
        $birth_dd = date("d",strtotime($reserve_sub_birth));
        $cate_menu_id = $db_result[0]["cate_menu_id"];
        $cate_menu_name = $db_result[0]["cate_menu_name"];
    }
?>

<tr class="sel_<? echo $loop;?>">
<th>主役<? echo $loop;?></th>
<td>
<div class="modelDl modelEnter">
<dl>
<dt>お名前</dt>
<dd>
    <? $var = "reserve_sub_name[$loop]";?>
    <input name="<? echo $var;?>" id="reserve_sub_name_<? echo $loop;?>" value="<? echo $reserve_sub_name;?>" type="text" placeholder="木村　花子"><label id="err_reserve_sub_name_<? echo $loop;?>"></label>
</dd>
<dt>ふりがな</dt>
<dd>
    <? $var = "reserve_sub_kana[$loop]";?>
    <input name="<? echo $var;?>" id="reserve_sub_kana_<? echo $loop;?>" value="<? echo $reserve_sub_kana;?>" type="text" placeholder="きむら　はなこ"><label id="err_reserve_sub_kana_<? echo $loop;?>"></label>
</dd>
<dt>性別</dt>
<dd>
<div class="selectBox">
    <? $var = "sex[$loop]";?>
    <select name="<? echo $var;?>" id="sex_<? echo $loop;?>">
        <option value="">選択</option>
        <? foreach($arr_sex as $key=>$value) { ?>
        <option value="<? echo $value;?>" <? if($sex==$value && $sex!=""){ echo " selected ";}?>><? echo $value;?></option>
        <? } ?>
    </select><label id="err_sex_<? echo $loop;?>"></label>
</div>
</dd>
<dt>誕生日</dt>
<dd>
    <span class="selectYear">
    <? $var = "reserve_sub_birth_yyyy[$loop]";?>
    <select name="<? echo $var;?>" id="reserve_sub_birth_yyyy_<? echo $loop;?>">
    <option value="">選択</option>
    <? foreach($arr_birth_yyyy as $key=>$value) { ?>
    <option value="<? echo $value;?>" <? if($birth_yyyy==$value){ echo " selected ";}?>><? echo $value;?></option>
    <? } ?>
    </select> 年</span>
    <span class="selectMonth">
    <? $var = "reserve_sub_birth_mm[$loop]";?>
    <select name="<? echo $var;?>" id="reserve_sub_birth_mm_<? echo $loop;?>">
    <option value="">選択</option>
    <? foreach($arr_birth_mm as $key=>$value) { ?>
    <option value="<? echo $value;?>" <? if($birth_mm==$value){ echo " selected ";}?>><? echo $value;?></option>
    <? } ?>
    </select> 月</span>
    <span class="selectDay">
    <? $var = "reserve_sub_birth_dd[$loop]";?>
    <select name="<? echo $var;?>" id="reserve_sub_birth_dd_<? echo $loop;?>">
    <option value="">選択</option>
    <? foreach($arr_birth_dd as $key=>$value) { ?>
    <option value="<? echo $value;?>" <? if($birth_dd==$value){ echo " selected ";}?>><? echo $value;?></option>
    <? } ?>
    </select> 日</span><label id="err_reserve_sub_birth_<? echo $loop;?>"></label>
</dd>
<dt>撮影メニュー</dt>
<dd>
<ul class="radioUl" id="cate_menu_<? echo $loop;?>">
<? $var = "cate_menu_id[$loop]"; ?>
<select name="<? echo $var;?>" id="cate_menu_id_<? echo $loop;?>">
    <option value="">選択</option>
    <? foreach($arr_cate_menu_id as $key=>$value) { ?>
    <option value="<? echo $key;?>" <? if($cate_menu_id==$key && $sex!=""){ echo " selected ";}?>><? echo $value;?></option>
    <? } ?>
</select>
</ul>
</dd>
</dl>
</div>
</td>
</tr>
<?
}
?>


<tr>
<th>スタジオご利用</th>
<td>
<? $var = "visited_count"; ?>
<input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" class="shortForm"> 回目
</td>
</tr>
<tr>
<th>来店人数</th>
<td>
大人 
<? $var = "visit_adult"; ?>
<input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" class="shortForm"> 人　
子ども 
<? $var = "visit_child"; ?>
<input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="text" class="shortForm"> 人
</td>
</tr>
<tr>
<th>撮影メニュー&プラン</th>
<td>
<label id="txt_plan_text"><? echo $arr_plan_price_name[$plan_price_id];?></label>
<? $var = "plan_price_id";?>
<input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" type="hidden" >

<p class="tableLink inlineColorBox" href="#inline-content"><a href="">選択する</a>
<div class="modelBox">
<table class="modelTable" id="inline-content">

<?
foreach($arr_plan_price_id as $key_menu=>$value_menu) {
?>
<tr>
<th><? echo $arr_cate_menu_id[$key_menu];?></th>
<td>
<?
    foreach($arr_plan_price_id[$key_menu] as $key=>$value) {
        echo "<a href=\"#\" class=\"class_plan\" title=\"".$key."\">".$value."</a>";
    }
?>
</td>
</tr>
<?
}
?>
</table>
</div>

<tr>
<th>基本料金</th>
<td>
<label id="txt_plan_price"><? echo "¥".number_format($arr_plan_price[$plan_price_id]);?></label> ※メニューとプランから自動算出
<? $var = "plan_price";?>
<input name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $arr_plan_price[$plan_price_id];?>" type="hidden" >
</td>
</tr>
<tr>
<th>オプション</th>
<td>
<div class="option">
<?
$all_option_price = 0;
//オプション
$sql = " select reserve_option_name, reserve_option_price FROM reserve_option ";
$sql .= " where reserve_id='".$reserve_id."' order by reserve_option_id" ; 
$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
?>

    <input name="reserve_option_name[]" type="text" placeholder="オプション商品" value="<? echo $db_result[$db_loop]["reserve_option_name"];?>">
    <input name="reserve_option_price[]" type="text" placeholder="オプション料金" value="<? echo $db_result[$db_loop]["reserve_option_price"];?>" class="option_price">
<?
        $all_option_price += $db_result[$db_loop]["reserve_option_price"];
    }
}
?>
    <input name="reserve_option_name[]" type="text" placeholder="オプション商品" value="">
    <input name="reserve_option_price[]" type="text" placeholder="オプション料金" value="" class="option_price">
</div>
<input type="button" class="tableLink" id="addForm" value="+ オプション追加">
</td>
</tr>
<tr>
<th>合計</th>
<td>
    <? $var ="reserve_price";?>
    <input name="<? echo $var;?>" id="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
    <label id="txt_reserve_price"><? if($$var!="") {echo "¥".number_format($$var);}?> ※自動算出</label>
</td>
</tr>
<tr>
<th>支払い方法</th>
<td>
<?
    $var = "pay_method";
    $arr_pay_method[] = "現金";
    $arr_pay_method[] = "クレジットカード";
?>
<? foreach($arr_pay_method as $key=>$value) { ?>
<label><input name="<? echo $var;?>" type="radio" value="<? echo $value;?>" <? if($$var==$value && $$var!=""){ echo " checked ";}?>><? echo $value;?></label>
<? } ?>
</td>
</tr>
<tr>
<th>備考</th>
<td>
<? $var = "reserve_comment"; ?>
<textarea name="<? echo $var;?>" id="<? echo $var;?>"><? echo $$var;?></textarea>
</td>
</tr>
<tr>
<th>画像</th>
<td>
<? 
$var = "img_1";
?>
<input type="file" name="<?=$var;?>" size="50" accept="image/*"> 
<input type="hidden" name="text_<?=$var;?>" value="<?=$$var;?>"><br>
<?
if ($$var != "")
{
    //echo "<a href='".global_ssl."/".global_reserve_dir.$admin_id."/".$$var."' target='_blank'>".global_ssl."/".global_reserve_dir.$admin_id."/".$$var."</a><br />";
    echo "<a href='/".global_reserve_dir.$reserve_id."/".$$var."' target='_blank'><img src='/".global_reserve_dir.$reserve_id."/".$$var."?d=".date(his)."' width=250 border=0></a>";
    echo "<br /><a href=\"#\" onClick='fnImgDel(\"".$reserve_id."\", \"".$$var."\", \"".$var."\");'>上のファイル削除</a>";
}
?>
</td>
</tr>
</tbody>
</table>



</section>
</form>

</article>


</body>
</html>
