<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
    $common_image = new CommonImage(); //画像
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>画像削除</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>
<body>
<?
    //ショップチェック
    $common_connect -> Fn_admin_check();
    
    $reserve_id = $_GET['reserve_id'];
    $img = $_GET['img'];
    $img_name = $_GET['img_name'];
    
    //DBへ保存
    if ($reserve_id != "")
    {
        unlink($_SERVER['DOCUMENT_ROOT']."/".global_reserve_dir.$reserve_id."/".$img);
        
        //商品情報削除
        $dbup = "update reserve set $img_name = '' where reserve_id = '$reserve_id'";
        $db_result = $common_dao->db_update($dbup);
    }
    
    $common_connect -> Fn_javascript_move("ファイルを削除しました。", "detail_form.php?reserve_id=".$reserve_id);

?>
</body>
</html>