<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }


    if(strlen($yyyymmdd)!=8)
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }

    $yyyy = date("Y",strtotime($yyyymmdd));
    $mm = date("m",strtotime($yyyymmdd));
    $dd = date("d",strtotime($yyyymmdd));
    //日付チェック
    if(!checkdate($mm, $dd, $yyyy))
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }

    //コース名
    $arr_plan = array();
    $sql = "select p_".$dd." as plan, cate_course_name, cate_course_id ";
    $sql .= " from schedule s inner join ";
    $sql .= " cate_course c on s.p_".$dd."=c.cate_course_id  ";
    $sql .= " where yyyymm='".$yyyy.$mm."'  ";// flag_open=1 and
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $cate_course_id = $db_result[0]["cate_course_id"];
        $cate_course_name = $db_result[0]["cate_course_name"];
    }

    //プラン
    $sql = " select plan_price_id, m.cate_menu_id, plan_price_name, plan_price, m.cate_menu_name ";
    $sql .= " FROM plan_price p inner join cate_menu m on m.cate_menu_id=p.cate_menu_id ";
    $sql .= " where m.cate_course_id='".$cate_course_id."' ";//and m.flag_open=1 and p.flag_open=1 ";
    $sql .= " order by p.view_level ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_plan_price_id[$db_result[$db_loop]["cate_menu_id"]][$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["plan_price_name"];
            $arr_plan_price[$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["plan_price"];
            $arr_plan_price_name[$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["cate_menu_name"]."　".$db_result[$db_loop]["plan_price_name"];
        }
    }

    //プランコース
    $arr_plan_course = array();
    $sql = "select cate_course_time_from, cate_course_time_to ";
    $sql .= " from cate_course_time ";
    $sql .= " where cate_course_time_id='".$cate_course_time_id."'  ";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $db_loop=0;
        $course_time = substr($db_result[$db_loop]["cate_course_time_from"], 0, 5)." 〜 ".substr($db_result[$db_loop]["cate_course_time_to"], 0, 5);
    }

    //予約データ
    $sql = "SELECT reserve_id, status FROM reserve " ;
    $sql .= " where reserve_day='".$yyyy."-".$mm."-".$dd."' ";
    $sql .= " and cate_course_time_id='".$cate_course_time_id."'  ";
    $sql .= " and flag_open=1  ";

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop_reserve=0 ; $db_loop_reserve < count($db_result) ; $db_loop_reserve++)
        {
            $arr_reserve_id[] = $db_result[$db_loop_reserve]["reserve_id"];
        }
    }
    
?>
<script type="text/javascript">
    $(function(){
      $('#reserve_detail_save').click(function(){
        var err_check_count = 0;
        var arr = [];
        <? foreach($arr_reserve_id as $key=>$value) { ?>
        if($("select[id=status_<? echo $value;?>").children(':selected').val()<=4){
            if(arr.indexOf($("select[id=status_<? echo $value;?>").children(':selected').val())>=0)
            {
                err_check_count = 1;
            }
            arr.push($("select[id=status_<? echo $value;?>").children(':selected').val());
        }
        <? } ?>

        if(err_check_count>0)
        {
            alert("重複して登録されています。");
            return false;
        }
        else
        {
            $('#form_regist').submit();
            return true;
        }
      });

    })

        function btn_search($str) {
            if($('#s_'+$str).val()=="")
            {
                alert("検索する文字を入力してください。");
                return false;
            }
            else
            {
                location.href  = "search.php?keyword="+$('#s_'+$str).val()+"&status="+$str+"&yyyymmdd="+$('#yyyymmdd').val()+"&cate_course_time_id="+$('#cate_course_time_id').val();
                return true;
            }
        }
    
</script>
<article>

<section class="table01">
<table>
<thead>
<tr>
<th colspan="3"><? echo $yyyy;?>/<? echo $mm;?>/<? echo $dd;?> <? echo $cate_course_name;?> [<? echo $course_time;?>]</th>
</tr>
</thead>
</table>


<form action="./detail_reserve_save.php" method="POST" name="form_write" id="form_regist">
<? $var = "yyyymmdd";?>
<input name="<? echo $var;?>" id="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
<? $var = "cate_course_time_id";?>
<input name="<? echo $var;?>" id="<? echo $var;?>" type="hidden" value="<? echo $$var;?>">
<?
$arr_loop = array('100', '1', '2', '3', '4');
foreach($arr_loop as $key=>$loop) {
    //予約データ
    $arr_db_field = array("reserve_id", "member_id", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "plan_price_id", "pay_method", "reserve_count", "reserve_price", "visited_count", "visit_adult", "visit_child", "img_1", "reserve_comment", "status", "regi_date");
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " 1 FROM reserve " ;
    $sql .= " where reserve_day='".$yyyy."-".$mm."-".$dd."' ";
    $sql .= " and cate_course_time_id='".$cate_course_time_id."'  ";
    $sql .= " and status='".$loop."'  ";
    $sql .= " and flag_open=1  ";

    $db_result_reserve = $common_dao->db_query_bind($sql);
    if($db_result_reserve)
    {
        for($db_loop_reserve=0 ; $db_loop_reserve < count($db_result_reserve) ; $db_loop_reserve++)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result_reserve[$db_loop_reserve][$val];
            }


            //会員情報
            $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "tel", "member_email", "post_num", "address_1", "address_2", "address_3");
            $sql = "SELECT ";
            foreach($arr_db_field as $val)
            {
                $sql .= $val.", ";
            }
            $sql .= " 1 FROM member " ;
            $sql .= " where member_id='".$member_id."'  ";

            $db_result = $common_dao->db_query_bind($sql);
            if($db_result)
            {
                $db_loop=0;
                foreach($arr_db_field as $val)
                {
                    $$val = $db_result[$db_loop][$val];
                }
            }

            $sum_price = 0;
?>
<table>
<tbody>
<tr>
<th rowspan="50" width="120">
<?
if($check != "modify")
{
    echo $global_reserve_admin_view[$loop];
}
else
{
?>
    <select name="status[<? echo $reserve_id;?>]" id="status_<? echo $reserve_id;?>">
        <? foreach($global_reserve_admin_view as $key => $value) { ?>
        <option value="<? echo $key;?>" <? if($key == $loop) { echo " selected";}?>><? echo $value;?></option>
        <? } ?>
    </select>
<?
}
?>
<p class="editBtn"><a href="./detail_form.php?reserve_id=<? echo $reserve_id;?>"><span class="tooltip">編集</span></a></p>
</th>
<th width="200">代表者</th>
<td data-href="/management/customer/detail.php?member_id=<? echo $member_id;?>"><? echo $member_name_1." ".$member_name_2;?></td>
</tr>
<tr>
<th>TEL</th>
<td><? echo $tel;?></td>
</tr>
<tr>
<th>予約日</th>
<td><? echo str_replace("-", "/", $regi_date);?></td>
</tr>
<tr>
<th>主役の人数</th>
<td><? echo $reserve_count;?>人</td>
</tr>
<? 
            $arr_cate_menu_id = array();
            $arr_cate_menu_name = array();
            //主役
            $arr_db_field = array("reserve_sub_id", "reserve_id", "reserve_sub_name", "reserve_sub_kana", "sex", "reserve_sub_birth", "cate_menu_id", "cate_menu_name");
            $sql = "SELECT ";
            foreach($arr_db_field as $val)
            {
                $sql .= $val.", ";
            }
            $sql .= " 1 FROM reserve_sub " ;
            $sql .= " where reserve_id='".$reserve_id."'  ";

            $db_result = $common_dao->db_query_bind($sql);
            if($db_result)
            {
                for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
                {
                    foreach($arr_db_field as $val)
                    {
                        $$val = $db_result[$db_loop][$val];
                    }

                    $arr_cate_menu_id[$db_loop] = $cate_menu_id;
                    $arr_cate_menu_name[$db_loop] = $cate_menu_name;
?>
    <tr>
    <th>主役<? echo ($db_loop+1);?></th>
    <td>

    <? echo $reserve_sub_name;?> <? echo $reserve_sub_kana;?>（<? echo $sex;?>）<? echo floor((date("Ymd")-date("Ymd", strtotime($reserve_sub_birth)))/10000);?>才 誕生日<? echo $reserve_sub_birth;?><br>
    <? echo $cate_menu_name;?></td>
    </tr>
<?
                }
            }
?>
<tr>
<th>来店人数</th>
<td>大人<? echo $visit_adult;?>人　子ども<? echo $visit_child;?>人</td>
</tr>
<tr>
<th>スタジオご利用</th>
<td><? echo $visited_count;?>回目</td>
</tr>
<tr>
<th>撮影メニュー</th>
<td><? echo $arr_cate_menu_name[0];?></td>
</tr>
<?
            $plan_price_name = "";
            $plan_price = "";
            if($plan_price_id!="")
            {
                $sql = "SELECT plan_price_name, plan_price FROM plan_price WHERE plan_price_id='".$plan_price_id."'";
                $db_result = $common_dao->db_query_bind($sql);
                if($db_result)
                {
                    $plan_price_name = $db_result[0]["plan_price_name"];
                    $plan_price = $db_result[0]["plan_price"];
                }
            }
?>
<tr>
<th>撮影プラン</th>
<td><? echo $arr_plan_price_name[$plan_price_id];?>　<? //echo $plan_price_name;?></td>
</tr>
<tr>
<th>基本料金</th>
<td>
<? if($plan_price!=0) { echo "¥".number_format($plan_price); $sum_price += $plan_price; }?>
</td>
</tr>
<tr>
<th>オプション</th>
<td>
<?
            $sql = "SELECT reserve_option_name, reserve_option_price FROM reserve_option " ;
            $sql .= " where reserve_id='".$reserve_id."' order by reserve_option_id ";

            $db_result = $common_dao->db_query_bind($sql);
            if($db_result)
            {
                for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
                {
                    echo $db_result[$db_loop]["reserve_option_name"]."：¥".number_format($db_result[$db_loop]["reserve_option_price"])."<br />";
                }
            }
?>
</td>
</tr>
<tr>
<th>合計</th>
<td><? echo "¥".number_format($reserve_price); ?></td>
</tr>
<tr>
<th>支払い方法</th>
<td><? echo $pay_method;?></td>
</tr>
<tr>
<th>備考</th>
<td><? echo $reserve_comment;?></td>
</tr>
<tr>
<th>画像</th>
<td>
<?
$var = "img_1";
if ($$var != "")
{
    //echo "<a href='".global_ssl."/".global_reserve_dir.$admin_id."/".$$var."' target='_blank'>".global_ssl."/".global_reserve_dir.$admin_id."/".$$var."</a><br />";
    echo "<a href='/".global_reserve_dir.$reserve_id."/".$$var."' target='_blank'><img src='/".global_reserve_dir.$reserve_id."/".$$var."?d=".date(his)."' width=250 border=0></a>";
}
?>
</td>
</tr>
</tbody>
</table>
<?
        } //for
    }
    else
    {
        if($check != "modify")
        {
?>
<table>
<tr>
<th width="120"><? echo $global_reserve_admin_view[$loop];?></th>
<th width="200">代表者</th>
<td><input id="s_<? echo $loop;?>" type="text" placeholder="姓 or 名 or ふりがなで検索"><p class="tableLink"><input type="button" value="+ 検索して追加" onclick="btn_search(<? echo $loop;?>)"></td>
</tr>
</table>
<?
        }
    } //if
} //for
?>


<?
//撮影完了とキャンセル
$arr_db_field = array("reserve_id", "member_id", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "plan_price_id", "pay_method", "reserve_count", "visited_count", "visit_adult", "visit_child", "img_1", "reserve_comment", "status", "regi_date");

$sql = "SELECT ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM reserve " ;
$sql .= " where reserve_day='".$yyyy."-".$mm."-".$dd."' ";
$sql .= " and cate_course_time_id='".$cate_course_time_id."'  ";
$sql .= " and status='99' "; //99：キャンセル
$sql .= " and flag_open=1  ";

$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    for($db_loop_reserve=0 ; $db_loop_reserve < count($db_result) ; $db_loop_reserve++)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop_reserve][$val];
        }


        //会員情報
        $arr_db_field_member = array("member_name_1", "member_name_2");
        $sql_member = "SELECT ";
        foreach($arr_db_field_member as $val)
        {
            $sql_member .= $val.", ";
        }
        $sql_member .= " 1 FROM member " ;
        $sql_member .= " where member_id='".$member_id."'  ";

        $db_result_member = $common_dao->db_query_bind($sql_member);
        if($db_result_member)
        {
            $db_loop_member=0;
            foreach($arr_db_field_member as $val)
            {
                $$val = $db_result_member[$db_loop_member][$val];
            }
        }
?>
<table>
<tr>
<th width="120" rowspan="2">
<?
if($check != "modify")
{
    echo $global_reserve_admin_view[$status];
}
else
{
?>
    <select name="status[<? echo $reserve_id;?>]" id="status_<? echo $reserve_id;?>">
        <? foreach($global_reserve_admin_view as $key => $value) { ?>
        <option value="<? echo $key;?>" <? if($key == $status) { echo " selected";}?>><? echo $value;?></option>
        <? } ?>
    </select>
<?
}
?>

<p class="editBtn"><a href="./detail_form.php?reserve_id=<? echo $reserve_id;?>"><span class="tooltip">編集</span></a></p>
</th>
<th width="200">代表者</th>
<td data-href="/management/customer/detail.php?member_id=<? echo $member_id;?>"><? echo $member_name_1." ".$member_name_2;?></td>
</tr>
<tr>
<th>備考</th>
<td><? echo nl2br($reserve_comment);?></td>
</tr>
</table>
<?
    }
}
?>
</form>

</section>

</article>


</body>
</html>
