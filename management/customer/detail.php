<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }


    if ($member_id != "")
    {
        //リスト表示
        $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "login_id", "login_pw", "post_num", "address_1", "address_2", "address_3", "tel", "member_email", "member_img", "flag_mailling", "flag_open");
        
        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM member where member_id='".$member_id."' " ;
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[0][$val];
            }
        }
    }
?>
<script type="text/javascript">
    $(function(){
      $('#customer_edit').click(function(){
        location.href="./detail_formnew.php?member_id=<? echo $member_id;?>";
      });
    })
    
</script>
<article>


<section class="table01">
<table>
<thead>
<tr>
<th colspan="2" class="tLeft"><? echo $member_name_1;?> <? echo $member_name_2;?><span class="hurigana"><? echo $member_name_kana;?></span></th>
</tr>
</thead>
<tbody>
<tr>
<th width="30%">ユーザー名</th>
<td><? echo $login_id;?></td>
</tr>
<tr>
<th width="30%">氏名</th>
<td><? echo $member_name_1." ".$member_name_2;?></td>
</tr>
<tr>
<th>パスワード</th>
<td><? echo $login_pw;?></td>
</tr>
<tr>
<th>郵便番号</th>
<td><? echo $post_num;?></td>
</tr>
<tr>
<th>都道府県</th>
<td><? echo $address_1;?></td>
</tr>
<tr>
<th>市町村・番地</th>
<td><? echo $address_2;?></td>
</tr>
<tr>
<th>建物名など</th>
<td><? echo $address_3;?></td>
</tr>
<tr>
<th>TEL</th>
<td><? echo $tel;?></td>
</tr>
<tr>
<th>メールアドレス</th>
<td><? echo $member_email;?></td>
</tr>
<tr>
<th>メルマガ</th>
<td><? if($flag_mailling==1) {echo "希望する";}elseif($flag_mailling==0) {echo "希望しない";}?></td>
</tr>
<tr>
<th>来店回数</th>
<td>
<?
    //予約データ
    $sql = "SELECT count(reserve_id) as reserve_count FROM reserve " ;
    $sql .= " where member_id='".$member_id."' ";
    $sql .= " and flag_open=1 and status=100 ";

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        if($db_result[0]["reserve_count"]!="")
        {
            echo $db_result[0]["reserve_count"]."回";
        }
    }
?>
</td>
</tr>

<tr>
<th>画像</th>
<td>
<? if($member_img!="") { ?>
<img src='<? echo "/".global_member_dir.$member_id."/".$member_img; ?>'>
<? } ?>
</td>
</tr>

</tbody>
</table>
</section>



<?
//予約データ
$arr_db_field = array("reserve_id", "member_id", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "plan_price_id", "plan_price_name", "reserve_count", "visited_count", "visit_adult", "visit_child", "pay_method", "reserve_comment", "cate_menu_id", "cate_menu_name", "reserve_price", "img_1", "status", "regi_date");

$sql = "SELECT ";
foreach($arr_db_field as $val)
{
    $sql .= $val.", ";
}
$sql .= " 1 FROM reserve " ;
$sql .= " where flag_open='1' ";
$sql .= " and member_id='".$member_id."' ";
$sql .= " and (reserve_day>='".date("Y-m-d")."' or (reserve_day<='".date("Y-m-d")."' and (status=100 or status=1))) ";
$sql .= " order by reserve_day, cate_course_time_from ";

$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        foreach($arr_db_field as $val)
        {
            $$val = $db_result[$db_loop][$val];
        }
        $course_time = substr($db_result[$db_loop]["cate_course_time_from"], 0, 5)." 〜 ".substr($db_result[$db_loop]["cate_course_time_to"], 0, 5);
        //撮影完了してる
        if($status==100)
        {
?>
<section class="table03">
<table>
<tr>
<th class="img" rowspan="15"><p>
<? if($img_1!="") { ?>
<img src='<? echo "/".global_reserve_dir.$reserve_id."/".$img_1; ?>'>
<? } ?>
</p><? echo $cate_menu_name;?><? if($plan_price_name!="") { echo "（".$plan_price_name."）";}?></th>
<th>撮影日時</th>
<td data-href="/management/reserve/detail_form.php?reserve_id=<? echo $reserve_id;?>"><? echo str_replace("-", "/", $reserve_day);?> <? echo $cate_course_name;?> <? echo $course_time;?></td>
</tr>
<tr>
<th>主役</th>
<td><? echo $reserve_count;?>人<br>
<?
//主役
$arr_db_field_sub = array("reserve_sub_id", "reserve_id", "reserve_sub_name", "reserve_sub_kana", "sex", "reserve_sub_birth", "cate_menu_id", "cate_menu_name");
$sql_sub = "SELECT ";
foreach($arr_db_field_sub as $val_sub)
{
    $sql_sub .= $val_sub.", ";
}
$sql_sub .= " 1 FROM reserve_sub " ;
$sql_sub .= " where reserve_id='".$reserve_id."'  ";

$db_result_sub = $common_dao->db_query_bind($sql_sub);
if($db_result_sub)
{
    for($db_loop_sub=0 ; $db_loop_sub < count($db_result_sub) ; $db_loop_sub++)
    {
        foreach($arr_db_field_sub as $val_sub)
        {
            $$val_sub = $db_result_sub[$db_loop_sub][$val_sub];
        }

        //echo $reserve_sub_name;?> <? echo $reserve_sub_kana;?>（<? echo $sex;?>）<? echo floor((date("Ymd")-date("Ymd", strtotime($reserve_sub_birth)))/10000);?>才 誕生日<? echo str_replace("-", "/", $reserve_sub_birth);?>　<? echo $cate_menu_name;?><br>
<?
    }
}
?>
</tr>
<tr>
<th>スタジオご利用</th>
<td><? echo $visited_count;?>回目</td>
</tr>
<?
    //プラン
    $plan_price = 0;
    $sql_plan = " select plan_price_id, m.cate_menu_id, plan_price_name, plan_price, m.cate_menu_name ";
    $sql_plan .= " FROM plan_price p inner join cate_menu m on m.cate_menu_id=p.cate_menu_id ";
    $sql_plan .= " where m.cate_course_id=1 and m.flag_open=1 and p.flag_open=1 ";
    $sql_plan .= " and plan_price_id='".$plan_price_id."' ";
    $db_result_plan = $common_dao->db_query_bind($sql_plan);
    if($db_result_plan)
    {
        $plan_price = $db_result_plan[0]["plan_price"];
    }
?>
<tr>
<th>基本料金</th>
<td>¥<? echo number_format($plan_price);?></td>
</tr>
<?
$all_option_price = 0;
//オプション
$sql_option = " select reserve_option_name, reserve_option_price FROM reserve_option ";
$sql_option .= " where reserve_id='".$reserve_id."' order by reserve_option_id" ; 
$db_result_option = $common_dao->db_query_bind($sql_option);
if($db_result_option)
{
    for($db_loop_option=0 ; $db_loop_option < count($db_result_option) ; $db_loop_option++)
    {
        $all_option_price += $db_result_option[$db_loop_option]["reserve_option_price"];
    }
}
?>
<tr>
<th>オプションA</th>
<td>¥<? echo number_format($all_option_price);?></td>
</tr>
<tr>
<th>合計</th>
<td>¥<? echo number_format($reserve_price)?></td>
</tr>
<tr>
<th>支払い方法</th>
<td><? echo nl2br($pay_method);?></td>
</tr>
<tr>
<th>備考</th>
<td><? echo nl2br($reserve_comment);?></td>
</tr>
</table>
</section>
<?
        }
        else
        {
?>
<section class="table03">
<table>
<tr>
<th class="img reserve" rowspan="5"><? echo $cate_menu_name;?></th>
<th>撮影日時</th>
<td data-href="/management/reserve/detail_form.php?reserve_id=<? echo $reserve_id;?>"><? echo str_replace("-", "/", $reserve_day);?> <? echo $cate_course_name;?> <? echo $course_time;?>（<? echo $global_reserve_admin_view[$status];?>）</td>
</tr>
<tr>
<th>予約日</th>
<td><? echo str_replace("-", "/", $regi_date);?></td>
</tr>
<tr>
<th>主役</th>
<td><? echo $reserve_count;?>人<br>
<?
//主役
$arr_db_field_sub = array("reserve_sub_id", "reserve_id", "reserve_sub_name", "reserve_sub_kana", "sex", "reserve_sub_birth", "cate_menu_id", "cate_menu_name");
$sql_sub = "SELECT ";
foreach($arr_db_field_sub as $val_sub)
{
    $sql_sub .= $val_sub.", ";
}
$sql_sub .= " 1 FROM reserve_sub " ;
$sql_sub .= " where reserve_id='".$reserve_id."'  ";

$db_result_sub = $common_dao->db_query_bind($sql_sub);
if($db_result_sub)
{
    for($db_loop_sub=0 ; $db_loop_sub < count($db_result_sub) ; $db_loop_sub++)
    {
        foreach($arr_db_field_sub as $val_sub)
        {
            $$val_sub = $db_result_sub[$db_loop_sub][$val_sub];
        }

        //echo $reserve_sub_name;?> <? echo $reserve_sub_kana;?>（<? echo $sex;?>）<? echo floor((date("Ymd")-date("Ymd", strtotime($reserve_sub_birth)))/10000);?>才 誕生日<? echo str_replace("-", "/", $reserve_sub_birth);?><br>
<?
    }
}
?>
</tr>
<tr>
<th>スタジオご利用</th>
<td><? echo $visited_count;?>回目</td>
</tr>
<tr>
<th>備考</th>
<td><? echo nl2br($reserve_comment);?></td>
</tr>
</table>
</section>
<?
        }//$status!=100
    }
}

?>




</article>


</body>
</html>
