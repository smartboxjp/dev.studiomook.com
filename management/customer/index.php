<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
?>
<article>


<section class="table04">
<form action="./" method="GET" name="form_search">
<table>
<tr>
<th>顧客検索</th>
<td width="400">
    <?php $var="keyword";?>
    <input name="<? echo $var;?>" type="text" placeholder="姓 or 名 or ふりがなを入力して検索" value="<? echo $$var;?>">
</td>
<td width="120"><input type="submit" value="検索"></td>
</tr>
</table>
</form>
</section>


<?php
    $view_count=100;   // List count
    $offset=0;
    $where = " ";
    $keyword = preg_replace("/( |　|   )/", "", $keyword );

    if($keyword!="")
    {
        $where .= " and (member_name_1 collate utf8_unicode_ci like '%".$keyword."%' or member_name_2 collate utf8_unicode_ci like '%".$keyword."%' or member_name_kana collate utf8_unicode_ci like '%".$keyword."%' ) ";
    }

    if(!$page)
    {
        $page=1;
    }
    Else
    {
        $offset=$view_count*($page-1);
    }
    
    //合計
    $sql_count = "SELECT count(member_id) as all_count FROM member where 1 ".$where ;
    
    $db_result_count = $common_dao->db_query_bind($sql_count);
    if($db_result_count)
    {
        $all_count = $db_result_count[0]["all_count"];
    }
    
    //リスト表示
    $arr_db_field = array("member_id", "member_name_1", "member_name_2", "member_name_kana", "login_id", "login_pw");
    $arr_db_field = array_merge($arr_db_field, array("tel", "member_email", "flag_mailling", "flag_open"));
    
    $sql = "SELECT ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " (SELECT reserve_day FROM reserve ";
    $sql .= " where member.member_id=reserve.member_id and flag_open=1 and status=100 ";
    $sql .= " order by reserve_day desc limit 0, 1) as reserve_day ";
    $sql .= " FROM member ";
    $sql .= " where 1 ".$where ;
    
    if($order_name != "")
    {
        $sql .= " order by ".$order_name." ".$order;
    }
    else
    {
        $sql .= " order by member_id desc";
    }
    $sql .= " limit $offset,$view_count";
?>
<section class="table01">
<table>
<thead>
<tr>
    <th>氏名<a href="?order_name=member_name_1&order=desc" class="desc">&#9650;</a><a href="?order_name=member_name_1" class="asc">&#9660;</a></th>
    <th>TEL<a href="?order_name=tel&order=desc" class="desc">&#9650;</a><a href="?order_name=tel" class="asc">&#9660;</a></th>
    <th>メール<a href="?order_name=member_email&order=desc" class="desc">&#9650;</a><a href="?order_name=member_email" class="asc">&#9660;</a></th>
    <th>最終来店日<a href="?order_name=reserve_day&order=desc" class="desc">&#9650;</a><a href="?order_name=reserve_day" class="asc">&#9660;</a></th>
</tr>
</thead>
<tbody>
    <?
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        $inner_count = count($db_result);
    
        for($db_loop=0 ; $db_loop < $inner_count ; $db_loop++)
        {
          foreach($arr_db_field as $val)
          {
            $$val = $db_result[$db_loop][$val];
          }
          $reserve_day = $db_result[$db_loop]["reserve_day"];　
    ?>
    <tr data-href="detail.php?member_id=<? echo $member_id;?>">
        <th><? echo $member_name_1." ".$member_name_2;?></th>
        <td><? echo $tel;?></td>
        <td><? echo $member_email;?></td>
        <td><? echo str_replace("-", "/", $reserve_day);?></td>
    </tr>
    <?php
        }
    }
    ?>

</tbody>
</table>

</section>

<?php $common_connect -> Fn_paging_10_list($view_count, $all_count); ?>

</article>


</body>
</html>
