<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }


    if ($member_id != "")
    {
        //リスト表示
        $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "login_id", "login_pw", "post_num", "address_1", "address_2", "address_3", "tel", "member_email", "member_img", "flag_mailling", "flag_open");
        
        $sql = "SELECT ";
        foreach($arr_db_field as $val)
        {
            $sql .= $val.", ";
        }
        $sql .= " 1 FROM member where member_id='".$member_id."' " ;
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[0][$val];
            }
        }
    }
?>
<script type="text/javascript">
    $(function() {
        $('#form_confirm').click(function() {
            err_default = "";
            err_check_count = 0;
            err_check = false;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";


            err_check_count += check_input("member_name_1");
            //err_check_count += check_input("member_name_2");
            err_check_count += check_input("member_name_kana");
            
            err_check_count += check_input_id("login_id");
            err_check_count += check_input_password("login_pw");
            //err_check_count += check_input("address");
            //err_check_count += check_input_tel("tel");
            //err_check_count += check_input_email("member_email");
            
            if(err_check_count)
            {
                //alert("入力に不備があります");
                return false;
            }
            else
            {
                $('#form_regist').submit();
                return true;
            }
            
            
        });
        

        
        function check_input($str) 
        {
            $("#err_"+$str).html(err_default);
            $("#"+$str).css(background,bgcolor_default);
            $("#"+$str).removeClass("error").removeClass("reauired");
            
            if($('#'+$str).val().replace(/　/g," ").match(/^\s+$/))
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                $("#"+$str).focus();
                
                return 1;
            }
            else if($('#'+$str).val()=="")
            {
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_"+$str).html(err);
                $("#"+$str).css(background,bgcolor_err);
                $("#"+$str).focus();
                
                return 1;
            }
            return 0;
        }
        
        //メールチェック
        function check_input_email($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            
            if(checkIsEmail($('#'+$str_1).val()) == false)
            {
                err ="<span class='error'>メールアドレスは半角英数字でご入力ください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                $("#"+$str_1).focus();
                
                return 1;
            }
            
            return 0;
        }
        //メールチェック
        function check_input_tel($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            
            if(checkIsTel($('#'+$str_1).val()) == false)
            {
                err ="<span class='error'>電話番号は半角英数字でご入力ください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                $("#"+$str_1).focus();
                
                return 1;
            }
            
            return 0;
        }
        
        //メールチェック
        function check_input_password($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            
            if(checkIsPassword($('#'+$str_1).val()) == false)
            {
                err ="<span class='error'>パスワードは6文字以上の半角英数字「!#$%&@()*+,.」でご入力ください。</span>";
                $("#err_"+$str_1).html(err);
                $("#"+$str_1).css(background,bgcolor_err);
                $("#"+$str_1).focus();
                
                return 1;
            }
            
            return 0;
        }

        function check_input_id($str_1) 
        {
            $("#err_"+$str_1).html(err_default);
            $("#"+$str_1).css(background,bgcolor_default);
            
            if($('#'+$str_1).val()!="")
            {
                if(checkIsID($('#'+$str_1).val()) == false)
                {
                    err ="<span class='error'>IDは4文字以上の半角英数字「-_」でご入力ください。</span>";
                    $("#err_"+$str_1).html(err);
                    $("#"+$str_1).css(background,bgcolor_err);
                    $("#"+$str_1).focus();
                    
                    return 1;
                }
            }
            
            return 0;
        }
        
        //メールチェック
        function checkIsEmail(value) {
            if (value.match(/.+@.+\..+/) == null) {
                return false;
            }
            return true;
        }
        
        function checkIsTel(value) {
            if (value.match(/^[0-9-]{8,}$/) == null) {
                return false;
            }
            return true;
        }
        
        function checkIsID(value) 
        {
            if (value.match(/^[0-9a-z_-]{4,}$/) == null) {
                return false;
            }
            return true;
        }

        function checkIsPassword(value) 
        {
            if (value.match(/^[0-9a-zA-Z!#$%&@()*+,./_-]{6,}$/) == null) {
                return false;
            }
            return true;
        }

        
    });
    
//-->
</script>

<script language="javascript"> 
    function fnChangeSel(i, j) { 
        var result = confirm('削除しますか？'); 
        if(result){ 
            document.location.href = './detail_formnew_del.php?member_id='+i+'&member_sub_id='+j;
        } 
    }
</script>

<script type="text/javascript">
    $(function(){
      $('#reserve_detail_save').click(function(){
        //var result = confirm('保存しますか？'); 
        //if(result){ 
            $('#form_regist').submit();
        //}
      });
    })
    
    function fnImgDel(i, j, k) { 
        var result = confirm('削除しますか？'); 
        if(result){
            document.location.href = './img_one_del.php?member_id='+i+'&img='+j+'&img_name='+k;
        } 
    }
</script>
<script src="/common/js/ajaxzip3.js" charset="UTF-8"></script>

<article>




<form action="./detail_formnew_save.php" method="POST" name="form_write" id="form_regist" enctype="multipart/form-data">
<? $var = "member_id"; ?>
<input type="hidden" name="<? echo $var;?>" value="<? echo $$var;?>">

<section class="table01">
<table>
<thead>
<tr>
<th colspan="2" class="tLeft">
    <? $var = "member_name_1"; ?>
    代表者姓 <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>">　
    <? $var = "member_name_2"; ?>
    代表者名 <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>">　
    <? $var = "member_name_kana"; ?>
    ふりがな <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>">　
</th>
</tr>
</thead>
<tbody>
<tr>
<th width="30%">ユーザー名</th>
<td>
    <? $var = "login_id"; ?>
    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" placeholder="4文字以上の半角英数字「-_」でご入力ください">
</td>
</tr>
<tr>
<th>パスワード</th>
<td>
    <?
        $var = "login_pw";
        if($$var=="") { $$var = $common_connect->Fn_random_data(10);}
    ?>
    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>郵便番号</th>
<td>
    <? $var = "post_num"; ?>
    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>" maxlength="8" onKeyUp="AjaxZip3.zip2addr(this,'','address_1','address_2');">
</td>
</tr>
<tr>
<th>都道府県</th>
<td>
    <? $var = "address_1"; ?>
    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>市町村・番地</th>
<td>
    <? $var = "address_2"; ?>
    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>建物名など</th>
<td>
    <? $var = "address_3"; ?>
    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>TEL</th>
<td>
    <? $var = "tel"; ?>
    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>メールアドレス</th>
<td>
    <? $var = "member_email"; ?>
    <input type="text" name="<? echo $var;?>" id="<? echo $var;?>" value="<? echo $$var;?>">
</td>
</tr>
<tr>
<th>メルマガ</th>
<td>
    <? $var = "flag_mailling"; ?>
    <label><input type="radio" name="<? echo $var;?>" value="1" <? if($$var==1 && $$var!="") { echo " checked ";}?>> 希望する</label>
    <label><input type="radio" name="<? echo $var;?>" value="0" <? if($$var==0 && $$var!="") { echo " checked ";}?>> 希望しない</label>
</td>
</tr>
<tr>
<th>来店回数</th>
<td>
<?
    //予約データ
    $sql = "SELECT count(reserve_id) as reserve_count FROM reserve " ;
    $sql .= " where member_id='".$member_id."' ";
    $sql .= " and flag_open=1  ";

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        if($db_result[0]["reserve_count"]!="")
        {
            echo $db_result[0]["reserve_count"]."回";
        }
    }
?>
</td>
</tr>
<tr>
<th>画像</th>
<td>
<? 
$var = "member_img";
?>
<input type="file" name="<?=$var;?>" size="50" accept="image/*"> 
<input type="hidden" name="text_<?=$var;?>" value="<?=$$var;?>"><br>
<?
if ($$var != "")
{
    //echo "<a href='".global_ssl."/".global_reserve_dir.$admin_id."/".$$var."' target='_blank'>".global_ssl."/".global_reserve_dir.$admin_id."/".$$var."</a><br />";
    echo "<a href='/".global_member_dir.$member_id."/".$$var."' target='_blank'><img src='/".global_member_dir.$member_id."/".$$var."?d=".date(his)."' width=250 border=0></a>";
    echo "<br /><a href=\"#\" onClick='fnImgDel(\"".$member_id."\", \"".$$var."\", \"".$var."\");'>上のファイル削除</a>";
}
?>
</td>
</tr>
</tbody>
</table>
</section>



</form>

</article>


</body>
</html>
