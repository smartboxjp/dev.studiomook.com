<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonImage.php";
    $common_image = new CommonImage(); //画像

    //管理者チェック
    $common_connect -> Fn_admin_check();
    
    foreach($_POST as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>登録</title>
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />
</head>

<body>
<?php   
    if($member_name_1 == "" || $login_pw == "")
    {
        $common_connect -> Fn_javascript_back("正しく入力して下さい。");
    }

    $post_num = mb_convert_kana($post_num, 'kvrn', "UTF-8");
    $post_num = str_replace("-", "", $post_num);
    
    $datetime = date("Y/m/d H:i:s");
    $lastday = date('t', strtotime($s_yyyy.'-'.$s_mm.'-01'));
    if($login_id!="") {$flag_open=1;} else {$flag_open=0;}

    //array
    $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "login_id", "login_pw", "post_num", "address_1", "address_2", "address_3", "tel", "member_email", "flag_mailling", "flag_open");
    if($member_id=="")
    {
        $db_insert = "insert into member ( ";
        $db_insert .= " member_id, ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val.", ";
        }
        $db_insert .= " regi_date, up_date ";
        $db_insert .= " ) values ( ";
        $db_insert .= " '', ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= " '".$$val."', ";
        }
        $db_insert .= " '$datetime', '$datetime')";
    }
    else
    {

        $db_insert = "update member set ";
        foreach($arr_db_field as $val)
        {
            $db_insert .= $val."='".$$val."', ";
        }
        $db_insert .= " up_date='".$datetime."' ";
        $db_insert .= " where member_id='".$member_id."'";
    }
    $db_result = $common_dao->db_update($db_insert);

    if ($member_id == "")
    {
        //自動生成されてるID出力
        $sql = "select last_insert_id() as last_id";  
        $db_result = $common_dao->db_query_bind($sql);
        if($db_result)
        {
            $member_id = $common_connect->h($db_result[0]["last_id"]);
        }
    }

    //Folder生成
    $save_dir = $global_path.global_member_dir.$member_id."/";

    //Folder生成
    $common_image -> create_folder ($save_dir);
    
    $new_end_name="_1";
    $var = "member_img";
    $text_var = "text_".$var;
    $fname_new_name[1] = $common_image -> img_save($var, $common_connect, $save_dir, $new_end_name, $member_id, $$text_var, "300");//300
    
    $dbup = "update member set ";
    $dbup .= " member_img='".$fname_new_name[1]."',";
    $dbup .= " up_date='$datetime' where member_id='".$member_id."'";
    
    $db_result = $common_dao->db_update($dbup);

    
    $common_connect-> Fn_redirect("./detail.php?member_id=".$member_id);
?>
</body>
</html>