<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連


    //管理者チェック
    $common_connect -> Fn_admin_check();

    header("Content-Type: application/vnd.ms-excel;"); 
    header("Content-Disposition: attachment; filename=reserve_".date("YmdHi").".csv"); 
    header("Content-Transfer-Encoding: text/plain"); 
    header("Pragma: no-cache"); 
    header("Expires: 0"); 


    $enter = "\r";
    $csv_list = "";
    $csv_list .= "ユーザー管理ID,ユーザーID,登録日,最終来店日,氏名,郵便番号,都道府県,";
    $csv_list .= "市町村・番地,建物名など,TEL,メールアドレス,メルマガ,来店回数,";
    for($loop=1 ; $loop<=10 ; $loop++)
    {
        $csv_list .= "撮影".$loop."の日付,撮影".$loop."のコース/メニュー/プラン,撮影".$loop."金額,";
    }
    $csv_list .= $enter;

    //リスト表示
    $arr_db_field = array("member_name_1", "member_name_2", "member_name_kana", "login_id", "login_pw", "post_num", "address_1", "address_2", "address_3");
    $arr_db_field = array_merge($arr_db_field, array("tel", "member_email", "flag_mailling", "flag_open"));
    
    $sql = "SELECT member.member_id, member.regi_date, ";
    foreach($arr_db_field as $val)
    {
        $sql .= $val.", ";
    }
    $sql .= " (SELECT reserve_day FROM reserve ";
    $sql .= " where member.member_id=reserve.member_id and flag_open=1 and status=100 ";
    $sql .= " order by reserve_day desc limit 0, 1) as reserve_day ";
    $sql .= " FROM member ";
    $sql .= " where 1 ".$where ;
    $sql .= " order by member.member_id desc";

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            foreach($arr_db_field as $val)
            {
                $$val = $db_result[$db_loop][$val];
            }
            $member_id = $db_result[$db_loop]["member_id"];
            $regi_date = $db_result[$db_loop]["regi_date"];
            $reserve_day = $db_result[$db_loop]["reserve_day"];
            if($flag_mailling==1) {$flag_mailling="希望する";}elseif($flag_mailling==0) {$flag_mailling="希望しない";}

            //予約データ
            $result_count = 0;
            $sql_count = "SELECT count(reserve_id) as reserve_count FROM reserve " ;
            $sql_count .= " where member_id='".$member_id."' ";
            $sql_count .= " and flag_open=1  ";

            $db_result_count = $common_dao->db_query_bind($sql_count);
            if($db_result_count)
            {
                $result_count = $db_result_count[0]["reserve_count"];
            }

            $csv_list .= "\"".$member_id."\",\"".$login_id."\",\"".$regi_date."\",\"".$reserve_day."\",\"".$member_name_1." ".$member_name_2."\",\"".$post_num."\",\"".$address_1."\",";
            $csv_list .= "\"".$address_2."\",\"".$address_3."\",\"".$tel."\",\"".$member_email."\",\"".$flag_mailling."\",\"".$result_count."\",";



            //予約データ
            $arr_db_field_reserve = array("reserve_id", "reserve_day", "cate_course_id", "cate_course_name", "cate_course_time_id", "cate_course_time_from", "cate_course_time_to", "plan_price_id", "plan_price_name", "reserve_count", "visited_count", "visit_adult", "visit_child", "pay_method", "reserve_comment", "cate_menu_name", "reserve_price", "img_1", "status", "regi_date");

            $sql_reserve = "SELECT ";
            foreach($arr_db_field_reserve as $val)
            {
                $sql_reserve .= $val.", ";
            }
            $sql_reserve .= " 1 FROM reserve " ;
            $sql_reserve .= " where flag_open='1' ";
            $sql_reserve .= " and member_id='".$member_id."' ";
            $sql_reserve .= " order by reserve_day desc";

            $db_result_reserve = $common_dao->db_query_bind($sql_reserve);
            if($db_result_reserve)
            {
                for($db_loop_reserve=0 ; $db_loop_reserve < count($db_result_reserve) ; $db_loop_reserve++)
                {
                    foreach($arr_db_field_reserve as $val)
                    {
                        $$val = $db_result_reserve[$db_loop_reserve][$val];
                    }
                    $csv_list .= "\"".$reserve_day."\",\"".$cate_course_name."／".$cate_menu_name."／".$plan_price_name."\",\"".$reserve_price."\",";
                    
                }
            }

            $csv_list .= $enter;
        }
    }
    



    //echo mb_convert_encoding($csv_list, 'SJIS', 'UTF-8');
    echo pack('C*',0xEF,0xBB,0xBF).$csv_list; //BOM付きのCSV
?>