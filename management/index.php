<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<script type="text/javascript">
    $(function() {
        $('#form_confirm').click(function() {
            err_default = "";
            err_check = false;
            bgcolor_default = "#FFFFFF";
            bgcolor_err = "#FFCCCC";
            background = "background-color";

            <? $str = "admin_id";?>
            $("#err_<?=$str;?>").html(err_default);
            $("#<?=$str;?>").css(background,bgcolor_default);
            
            if($('#<?=$str;?>').val()=="")
            {
                err_check = true;
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_<?=$str;?>").html(err);
                $("#<?=$str;?>").css(background,bgcolor_err);
            }

            <? $str = "admin_pw";?>
            $("#err_<?=$str;?>").html(err_default);
            $("#<?=$str;?>").css(background,bgcolor_default);
            
            if($('#<?=$str;?>').val()=="")
            {
                err_check = true;
                err ="<span class='error'>正しく入力してください。</span>";
                $("#err_<?=$str;?>").html(err);
                $("#<?=$str;?>").css(background,bgcolor_err);
            }
            
            
            if(err_check)
            {
                alert("入力に不備があります");
                return false;
            }
            else
            {
                $('#form_confirm').submit();
                return true;
            }
            
            
        });
        
    });
//-->
</script>
<article>


<section class="table06">
<form name="login_check" method="post" action="/management/admin_login/login_check.php">
<table>
<tr>
<th>ログイン</th>
</tr>
<tr>
<td>
    <input type="text" name="admin_id" id="admin_id" placeholder="ユーザー名">
    <label id="err_admin_id"></label>
</td>
</tr>
<tr>
<td>
    <input type="password" name="admin_pw" id="admin_pw" placeholder="パスワード">
    <label id="err_admin_pw"></label>
</td>
</tr>
<tr>
<td><input type="submit" name="button" id="form_confirm" value="ログイン"></td>
</tr>
</table>
</form>
</section>

</article>


</body>
</html>
