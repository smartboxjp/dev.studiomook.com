<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();

    //コース
    $sql = " select cate_course_id, cate_course_name FROM cate_course where flag_open=1 order by view_level" ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_id[$db_result[$db_loop]["cate_course_id"]] = $db_result[$db_loop]["cate_course_name"];
        }
    }

    //プランの時間
    $sql = " select cate_course_id, cate_course_time_id, cate_course_time_from, cate_course_time_to FROM cate_course_time where flag_open=1 order by view_level" ; 
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_time_from[$db_result[$db_loop]["cate_course_id"]][$db_result[$db_loop]["cate_course_time_id"]] = substr($db_result[$db_loop]["cate_course_time_from"], 0, 5);
        }
    }

    $sql = "select cate_course_id, cate_course_name, ";
    $sql .= " (select count(cate_course_time_id) from cate_course_time where cate_course_time.cate_course_id=cate_course.cate_course_id
 and flag_open=1) as cate_course_time_count ";
    $sql .= " FROM cate_course where flag_open=1 order by view_level";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_cate_course_time_id[$db_result[$db_loop]["cate_course_id"]] = $db_result[$db_loop]["cate_course_time_count"];
        }
    }

    //30分以内の仮予約削除(予約初期化)
    $del_time = date("Y-m-d H:i", strtotime('-31 minute'));
    $db_del = "Delete from reserve where flag_open=0 and regi_date<'".$del_time."' ";
    $common_dao->db_update($db_del);
?>

<?php
    $add_day = 1;
    $yyyymmdd = date("Y-m-01");
    $today_ymd = date("Ymd");
    //$today_ymd = "20170129";

    $addday_ymd = date("Ym01", strtotime($today_ymd." ".(30+$add_day)." day"));

    $date1=strtotime($today_ymd);
    $date2=strtotime($addday_ymd);
    $month1=date("Y",$date1)*12+date("m",$date1." 00:00");
    $month2=date("Y",$date2)*12+date("m",$date2." 00:00");
    $diff = $month2 - $month1;

    $max_month = $diff+1;
?>
<article>

<?
//最新公開枠表示
$check_sun = 0;
$check_mon = 0;
$check_day = 0;
for ($i=0 ; $i < $max_month ; $i++) {
    
    $sql  = "SELECT '".$yyyymmdd."' + INTERVAL $i MONTH as yyyymm";
    $result_month = $common_dao->db_query_bind ($sql);
    $yyyy = date("Y", strtotime($result_month[0]["yyyymm"]));
    $mm = date("m", strtotime($result_month[0]["yyyymm"]));
    
    //最初の曜日
    $firstTime = strtotime($yyyy . "-" . $mm . "-01"); 
    $firstWeek = date("w", $firstTime);
    if($firstWeek==0) { $firstWeek=7; } //■月曜日から始まる
    
    //最後の日
    for ($ld = 28; checkdate($mm,$ld,$yyyy); $ld ++); 
    $lastDay = $ld - 1;
    
    //最後の曜日
    $lastTime = strtotime($yyyy . "-" . $mm . "-" . $lastDay); 
    $lastWeek = date("w", $lastTime); 
    if($lastWeek==0) { $lastWeek=7; } //■月曜日から始まる
                
    //arrへ保存
    $arrSCH = array(array(),array(),array(),array(),array(),array(),array()); 
    
    //該当する最初の曜日になる前にはブランク
    for($j = 1; $j < $firstWeek; $j ++) {
        $arrSCH[$j][] = ""; 
    }
    
    $maxRow = 0; 
    for($schDay = 1; $schDay <= $lastDay; $schDay ++) { 
        $checkday = mktime(0, 0, 0, $mm, $schDay, $yyyy); 
        $week = date("w", $checkday);
        if($week==0) { $week=7; } //■月曜日から始まる
        $arrSCH[$week][] = $schDay; 
    
        if(count($arrSCH[$week]) > $maxRow) $maxRow = count($arrSCH[$week]); 
    } 


    //予約データ
    $arr_reserve = array();
    $sql = "SELECT day(reserve_day) as reserve_day, cate_course_time_id, status ";
    $sql .= " FROM  reserve ";
    $sql .= " WHERE flag_open =1 ";
    $sql .= " and left(reserve_day, 7)='".$yyyy."-".$mm."' ";

    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
        {
            $arr_reserve[$db_result[$db_loop]["reserve_day"]][$db_result[$db_loop]["cate_course_time_id"]][$db_result[$db_loop]["status"]]++;
        }
    }

    //日毎のプラン
    //初期化
    $p = array();
    $sql = "select  ";
    for($loop=1 ; $loop<=$lastDay ; $loop++) {
        $sql .= "p_".substr($loop+100, 1)." , ";
    }
    $sql .= " up_date from schedule ";
    $sql .= " where yyyymm='".$yyyy.substr($mm+100, 1)."'";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($loop=1 ; $loop<=$lastDay ; $loop++) {
            $p[substr($loop+100, 1)] = $db_result[0]["p_".substr($loop+100, 1)];
        }
    }
?>
<section class="table05">
<table>
<thead>
<tr>
<th class="tit" colspan="7"><?php echo $yyyy;?>年<?php echo $mm;?>月</th>
</tr>
<tr>
<th>月</th>
<th>火</th>
<th>水</th>
<th>木</th>
<th>金</th>
<th>土</th>
<th>日</th>
</tr>
</thead>
<tbody>
            
<?
for($row = 0; $row < $maxRow; $row ++) { 
    echo "<tr>"; 
    for($col = 1; $col < 8; $col ++) { 
        $day = $arrSCH[$col][$row]; 
        $dd = substr(($day+100),1);

        $cal_class = "";
        $check_link = false;
        if(checkdate($mm, $dd, $yyyy))
        {
            if(date("Ymd",strtotime($yyyy . "-" . $mm . "-" . $day))<=date("Ymd", strtotime($today_ymd." ".$add_day." day")))
            {
                $cal_class .= "past ";
            }
            else
            {
                $check_day++;
                $check_link = true;
            }
            
            if($check_day>28 && $check_sun>4)
            {
                $cal_class .= "notyet ";
                $check_link = false;
            }


            if($col == 1)
            {
                if(date("Ymd",strtotime($yyyy . "-" . $mm . "-" . $day))>date("Ymd", strtotime($today_ymd." ".$add_day." day")))
                {
                    $check_mon++;
                }
            }
            if($check_day>21 && $check_mon==4)
            {
                $cal_class .= "open ";
            }
        }


        if($col == 7)
        {
            $cal_class .= "sun ";
            if(date("Ymd",strtotime($yyyy . "-" . $mm . "-" . $day))>date("Ymd", strtotime($today_ymd." ".$add_day." day")))
            {
                $check_sun++;
            }
        }
        elseif($col == 6)
        {
            $cal_class .= "sat ";
        }
        if($arr_cate_course_time_id[$p[$dd]]<1)
        {
            $cal_class .= "none ";
        }

        if(date("Ymd") == $yyyy . $mm . $day)
        {
            $cal_class .= "today ";
        }


        $check_loginbox = false;
        echo "<td ";

        echo " class=\"".$cal_class."\">";

        if($day)
        {
        ?>
            <p>
            <span class="day"><? echo $day;?></span>
        <?


            if($arr_cate_course_time_id[$p[$dd]]>0)
            {
        ?>
            <span class="plan"><? echo $arr_cate_course_id[$p[$dd]];?></span>

            <table>
            <tbody>
            <?
            foreach ($arr_cate_course_time_from[$p[$dd]] as $key => $value) {
            ?>
            <tr>
            <th><? echo $value;?></th>
            <td>
            <?
                $reserved_count = 0;
                $reserved_count = $arr_reserve[$day][$key][1]+$arr_reserve[$day][$key][100];
            ?>
            <span <? if($reserved_count=="0"){ echo "class=\"vacancy\""; } ?>><? echo $reserved_count;?></span>
            +<? echo $arr_reserve[$day][$key][2]+$arr_reserve[$day][$key][3]+$arr_reserve[$day][$key][4]?>
            </td>
            </tr>
            <?
            }
            ?>
            </tbody>
            </table>
        <?
            }
            else
            {
        ?>
            <span class="plan">撮影なし</span>
        <?
                //notyetが含まれた場合
                if(strpos($cal_class,'notyet') !== false){
        ?>
<table>
<tbody>
<tr>
<th>予約受付前 </th>
</tr>
</tbody>
</table>
        <?
                }
            }
        ?>
            </p>
        <?
        }
        
        echo "</td>"; 
    } 
    echo "</tr>"; 
} 
?>




</tbody>
</table>
<?
}
?>



</section>

</article>


</body>
</html>
