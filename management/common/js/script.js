/*テーブルをリンクにする*/
jQuery(function($) {
  //data-hrefの属性を持つtrを選択しclassにclickableを付加
  $('tr[data-href]').addClass('clickable')
    //クリックイベント
    .click(function(e) {
      //e.targetはクリックした要素自体、それがa要素以外であれば
      if(!$(e.target).is('a')){
        //その要素の先祖要素で一番近いtrの
        //data-href属性の値に書かれているURLに遷移する
        window.location = $(e.target).closest('tr').data('href');}
  });
});/*テーブルをリンクにする*/
jQuery(function($) {
  //data-hrefの属性を持つtrを選択しclassにclickableを付加
  $('td[data-href]').addClass('clickable')
    //クリックイベント
    .click(function(e) {
      //e.targetはクリックした要素自体、それがa要素以外であれば
      if(!$(e.target).is('a')){
        //その要素の先祖要素で一番近いtdの
        //data-href属性の値に書かれているURLに遷移する
        window.location = $(e.target).closest('td').data('href');}
  });
});

/*ツールチップ*/
$(function() {
	$('.reserved').hover(function() {
		sethover = setTimeout(function(){
			$('.reserved .tooltip').fadeIn(300);
		},500);
	}, function() {
			$('.reserved .tooltip').fadeOut(300);
		clearTimeout(sethover);
	});
});
$(function() {
	$('.waiting').hover(function() {
		sethover = setTimeout(function(){
			$('.waiting .tooltip').fadeIn(300);
		},500);
	}, function() {
			$('.waiting .tooltip').fadeOut(300);
		clearTimeout(sethover);
	});
});
$(function() {
	$('.check').hover(function() {
		sethover = setTimeout(function(){
			$('.check .tooltip').fadeIn(300);
		},500);
	}, function() {
			$('.check .tooltip').fadeOut(300);
		clearTimeout(sethover);
	});
});
$(function() {
	$('.edit').hover(function() {
		sethover = setTimeout(function(){
			$('.edit .tooltip').fadeIn(300);
		},500);
	}, function() {
			$('.edit .tooltip').fadeOut(300);
		clearTimeout(sethover);
	});
});
$(function() {
	$('.plus').hover(function() {
		sethover = setTimeout(function(){
			$('.plus .tooltip').fadeIn(300);
		},500);
	}, function() {
			$('.plus .tooltip').fadeOut(300);
		clearTimeout(sethover);
	});
});
$(function() {
	$('.save').hover(function() {
		sethover = setTimeout(function(){
			$('.save .tooltip').fadeIn(300);
		},500);
	}, function() {
			$('.save .tooltip').fadeOut(300);
		clearTimeout(sethover);
	});
});
$(function() {
	$('.csv').hover(function() {
		sethover = setTimeout(function(){
			$('.csv .tooltip').fadeIn(300);
		},500);
	}, function() {
			$('.csv .tooltip').fadeOut(300);
		clearTimeout(sethover);
	});
});
$(function() {
	$('.logout').hover(function() {
		sethover = setTimeout(function(){
			$('.logout .tooltip').fadeIn(300);
		},500);
	}, function() {
			$('.logout .tooltip').fadeOut(300);
		clearTimeout(sethover);
	});
});

/*カラーボックス*/

$(function() {
    $(".inlineColorBox").colorbox({
    inline:true,
    maxWidth:"90%",
    maxHeight:"90%",
    opacity: 0.7
  });
});