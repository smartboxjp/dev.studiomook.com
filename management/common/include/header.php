<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/common.php"); ?>

<!DOCTYPE HTML>
<html>
<head>
<meta charset="UTF-8">
<meta name="robots" content="noindex,nofollow">
<title>[Mr.MOOK管理画面]</title>

<link rel="stylesheet" href="/management/common/css/management.css">
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="/management/common/js/script.js"></script>
<script src="/management/common/js/jquery.easing.1.3.js"></script>
<script src="/management/common/js/colorbox/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="/management/common/js/colorbox/colorbox.css">
<!--▼favicon-->
<link href="https://fonts.googleapis.com/css?family=Crimson+Text" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
<link rel="apple-touch-icon" sizes="180x180" href="/common/img/share/favicon/apple-touch-icon.png">
<link rel="icon" type="image/png" href="/common/img/share/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/common/img/share/favicon/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/common/img/share/favicon/manifest.json">
<link rel="mask-icon" href="/common/img/share/favicon/safari-pinned-tab.svg" color="#26afaf">
<meta name="theme-color" content="#ffffff">
<!--▲favicon-->

</head>

<body>
<header<?php if($savePage){ echo ' class="savePage"';}?>>

<?php if($_SESSION['admin_id']!=""){ ?>
<?php if(!$savePage){ ?>
<ul class="statusList">
<li class="reserved"><a href="/management/reserve/"><span>
<?
/*
例えば、今日1/22だとしたら。
予約確定が、1/22までの予約確定数
キャンセル待ちが、1/22までのキャンセル待ち数
処理予定が、1/21以前の予約確定数。（撮影完了になっていない数）
*/
$date_time = date("Y-m-d");
$sql = " SELECT count(reserve_id) as reserve_count FROM reserve ";
$sql .= " where flag_open=1 and  reserve_day>='".$date_time."' and status=1 ";
$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    echo $db_result[0]["reserve_count"];
}
?>
</span></a><p class="tooltip">予約確定</p></li>
<li class="waiting"><a href="/management/reserve/"><span>
<?
$date_time = date("Y-m-d");
$sql = " SELECT count(reserve_id) as reserve_count FROM reserve ";
$sql .= " where flag_open=1 and  reserve_day>='".$date_time."' and (status=2 or status=3 or status=4) ";
$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    echo $db_result[0]["reserve_count"];
}
?>
</span></a><p class="tooltip">キャンセル待ち</p></li>
<li class="check"><a href="/management/reserve/"><span>
<?
$date_time = date("Y-m-d");
$sql = " SELECT count(reserve_id) as reserve_count FROM reserve ";
$sql .= " where flag_open=1 and  reserve_day<'".$date_time."' and status=1 ";
$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    echo $db_result[0]["reserve_count"];
}
?>
</span></a><p class="tooltip">処理予定</p></li>
</ul>
<?php } ?>

<ul class="operateList">
<?php if(!$savePage){ ?>
<li class="logout"><a href="/management/admin_login/logout.php"><span class="tooltip">ログアウト</span></a></li>
    <?php if($_GET["check"] != "modify") { ?>
        <?php if($category_file == reserve) { ?>
<li class="csv"><a href="/management/reserve/reserve_csv.php"><span class="tooltip">csvダウンロード</span></a></li>
        <?php } elseif($category_file == customer) { ?>
<li class="csv"><a href="/management/customer/customer_csv.php"><span class="tooltip">csvダウンロード</span></a></li>
        <?php } ?>
    <?php } ?>
<?php if($category_file == customer && $page_file == detail) { ?>
<li class="edit"><a href="#" id="customer_edit"><span class="tooltip">編集</span></a></li>
<?php }elseif($category_file == customer && $page_file == "index") { ?>
<li class="plus"><a href="./detail_formnew.php"><span class="tooltip">新規入力</span></a></li>
<?php }elseif($category_file == reserve && $page_file == detail) { ?>
    <?php if($_GET["check"] != "modify") { ?>
<li class="edit"><a href="./detail.php?check=modify&<? echo $_SERVER["QUERY_STRING"];?>"><span class="tooltip">編集</span></a></li>
    <?php } else { ?>
<li class="save"><a href="#" id="reserve_detail_save"><span class="tooltip">保存</span></a></li>
    <?php } ?>
<?php }elseif($category_file == schedule && $page_file == index) { ?>
<li class="edit"><a href="#" id="schedule_edit"><span class="tooltip">編集</span></a></li>
<?php } ?>
<?php }else{ 
	if($category_file == schedule && $page_file == index_form) { ?>
<li class="save"><a href="#" id="schedule_save"><span class="tooltip">保存</span></a></li>
<?php }else{ ?>
<li class="save"><a href="#" id="reserve_detail_save"><span class="tooltip">保存</span></a></li>
<?php } ?><?php } ?>
</ul>
<?php } //if($_SESSION['admin_id'] ?>


</header>
<nav>
<p></p>
<ul>
<li class="dashboard<?php if($category_file == dashboard){ echo " act";}?>"><a href="/management/dashboard/">ダッシュボード</a></li>
<li class="customer<?php if($category_file == customer){ echo " act";}?>"><a href="/management/customer/">顧客管理</a></li>
<li class="reserve<?php if($category_file == reserve){ echo " act";}?>"><a href="/management/reserve/">予約管理</a></li>
<li class="schedule<?php if($category_file == schedule){ echo " act";}?>"><a href="/management/schedule/">スケジュール管理</a></li>
<li class="data<?php if($category_file == data){ echo " act";}?>"><a href="/management/data/">データ集計</a></li>
</ul>
</nav>

