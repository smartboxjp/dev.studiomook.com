<?php
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
    $common_connect = new CommonConnect();
    require_once $_SERVER['DOCUMENT_ROOT']."/app_include/CommonDao.php";
    $common_dao = new CommonDao(); //DB関連
?>
<?php require_once($_SERVER["DOCUMENT_ROOT"]. $DOCUMENT_ROOT."/management/common/include/header.php"); ?>

<?php
    //管理者チェック
    $common_connect -> Fn_admin_check();
    foreach($_GET as $key => $value)
    { 
        $$key = $common_connect->h($value);
    }

    if($s_yyyy=="" || $s_mm=="")
    {
        $yyyymm = date("Ym");
        $s_yyyy = date("Y");
        $s_mm = date("m");
    }
    else
    {
        $yyyymm = $s_yyyy.$s_mm;
    }

    //最後の日
    for ($ld = 28; checkdate($s_mm,$ld,$s_yyyy); $ld ++); 
    $lastDay = $ld - 1;

    $p = array();
    $sql = "select  ";
    for($loop=1 ; $loop<=$lastDay ; $loop++) {
        $sql .= "p_".substr($loop+100, 1)." , ";
    }
    $sql .= " up_date from schedule ";
    $sql .= " where yyyymm='".$s_yyyy.substr($s_mm+100, 1)."'";
    $db_result = $common_dao->db_query_bind($sql);
    if($db_result)
    {
        for($loop=1 ; $loop<=$lastDay ; $loop++) {
            $p[$db_result[0]["p_".substr($loop+100, 1)]]+=1;
        }
    }
?>



<article>

<section class="table04">

<?
//コース
$sql = " select cate_course_id, cate_course_name FROM cate_course where cate_course_id!=5 order by view_level" ; //flag_open=1 and 
$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        $arr_cate_course_id[$db_result[$db_loop]["cate_course_id"]] = $db_result[$db_loop]["cate_course_name"];
    }
}

//メニュー
$sql = " select cate_course_id, cate_menu_id, cate_menu_name FROM cate_menu order by view_level" ; //where flag_open=1 
$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        $arr_cate_menu_name[$db_result[$db_loop]["cate_menu_id"]] = $db_result[$db_loop]["cate_menu_name"];
        $arr_cate_menu_course_id[$db_result[$db_loop]["cate_menu_id"]] = $db_result[$db_loop]["cate_course_id"]; 
    }
}

//プラン
$row_price_all = 0;
$sql = " select plan_price_id, cate_menu_id, plan_price_name FROM plan_price order by view_level" ; //where flag_open=1 
$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        $arr_plan_price[$db_result[$db_loop]["cate_menu_id"]][$db_loop] = $db_result[$db_loop]["plan_price_name"];
        $arr_menu_count[$db_result[$db_loop]["cate_menu_id"]]++;
    }
}

?>




<form action="./index.php" method="GET" name="form_search">
<table>
<tr>
<th>表示月</th>
<td width="120"><div class="customSelect">

<? $var = "s_yyyy";?>
<select name="<? echo $var;?>">
    <? for($loop=date(Y) ; $loop<date(Y)+3 ; $loop++) { ?>
    <option value="<? echo $loop;?>" <? if($$var == $loop) { echo " selected";}?>><? echo $loop;?>年</option>
    <? } ?>
</select>
</div></td>
<td width="120"><div class="customSelect">

<? $var = "s_mm";?>
<select name="<? echo $var;?>">
    <? for($loop=1 ; $loop<13 ; $loop++) { ?>
    <option value="<? echo substr($loop+100, 1);?>" <? if(intval($$var) == substr($loop+100, 1)) { echo " selected=true ";}?>><? echo $loop;?>月</option>
    <? } ?>
</select>
</div></td>
<td width="120"><input type="submit" value="表示"></td>
</tr>
</table>
</form>
</section>

<?
//予約データ
$sql = "SELECT cate_course_id, count(reserve_count) as sum_reserve_count, sum(reserve_price) as sum_reserve_price  FROM reserve ";
$sql .= " where left(reserve_day, 7)='".$s_yyyy."-".$s_mm."' and flag_open=1 and status=100 GROUP BY cate_course_id";
$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        $arr_sum_reserve_count[$db_result[$db_loop]["cate_course_id"]] = $db_result[$db_loop]["sum_reserve_count"];
        $arr_sum_reserve_price[$db_result[$db_loop]["cate_course_id"]] = $db_result[$db_loop]["sum_reserve_price"];
    }
}
?>
<section class="table01">
<table>
<thead>
<tr>
<th width="30%">コース</th>
<th>開催日</th>
<th>撮影人数</th>
<th>売上</th>
<th>客単価</th>
</tr>
</thead>
<tbody>
<?
$sum_schedule_count = 0;
$sum_reserve_count = 0;
$sum_reserve_price = 0;
foreach($arr_cate_course_id as $key=>$value) {
?>
<tr>
<th><? echo $value;?></th>
<td><? echo $p[$key]; $sum_schedule_count += $p[$key];?>日</td>
<td><? echo number_format($arr_sum_reserve_count[$key]); $sum_reserve_count += $arr_sum_reserve_count[$key];?>人</td>
<td>¥<? echo number_format($arr_sum_reserve_price[$key]); $sum_reserve_price += $arr_sum_reserve_price[$key];?></td>
<td>¥<? echo number_format($arr_sum_reserve_price[$key]/$arr_sum_reserve_count[$key]);?></td>
</tr>
<?
}
?>
<tr class="sum">
<th>合計</th>
<td><? echo $sum_schedule_count;?>日</td>
<td><? echo number_format($sum_reserve_count);?>人</td>
<td>¥<? echo number_format($sum_reserve_price);?></td>
<td>¥<? echo number_format($sum_reserve_price/$sum_reserve_count);?></td>
</tr>
</tbody>
</table>
</section>



<?
$arr_sum_reserve_count = array();
$arr_sum_reserve_price = array();
//予約データ
$sql = "SELECT plan_price_id, count(reserve_count) as sum_reserve_count, sum(reserve_price) as sum_reserve_price, ";
$sql .= " sum((SELECT SUM( reserve_option_price )  ";
$sql .= " FROM reserve_option ";
$sql .= " WHERE reserve.reserve_id = reserve_option.reserve_id ";
$sql .= " )) AS reserve_option_price ";
$sql .= " FROM reserve ";
$sql .= " where left(reserve_day, 7)='".$s_yyyy."-".$s_mm."' and flag_open=1 and status=100";
$sql .= " group by plan_price_id ";

$db_result = $common_dao->db_query_bind($sql);
if($db_result)
{
    for($db_loop=0 ; $db_loop < count($db_result) ; $db_loop++)
    {
        $arr_sum_reserve_count[$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["sum_reserve_count"];
        $arr_sum_reserve_price[$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["sum_reserve_price"];
        $arr_sum_reserve_option_price[$db_result[$db_loop]["plan_price_id"]] = $db_result[$db_loop]["reserve_option_price"];
    }
}

foreach ($arr_cate_course_id as $key_course => $value_course) {
?>
<section class="table01">
<table>
<thead>
<tr>
<th>コース</th>
<th width="15%">メニュー</th>
<th width="15%">プラン</th>
<th width="12%">単価</th>
<th width="10%">人数</th>
<th width="10%">セット売上</th>
<th width="10%">オプション売上</th>
<th width="10%">合計売上</th>
</tr>
</thead>
<tbody>
<tr>


<?
    $total_plan_reserve_count = 0;
    $total_set = 0;
    $total_option = 0;
    $total_price = 0;

    //プラン
    $sql_plan = " select plan_price_id, p.cate_menu_id, plan_price_name, plan_price ";
    $sql_plan .= " FROM plan_price p inner join cate_menu m on p.cate_menu_id=m.cate_menu_id ";
    $sql_plan .= " where cate_course_id='".$key_course."'  order by m.view_level, p.view_level" ; //and p.flag_open=1 and m.flag_open=1
    $db_result_plan = $common_dao->db_query_bind($sql_plan);

    if($db_result_plan)
    {
?>
<th rowspan="<? echo count($db_result_plan);?>"><? echo $value_course; ?></th>
<?
        $before_menu = ""; $after_menu = "";
        for ($db_loop_price=0; $db_loop_price < count($db_result_plan) ; $db_loop_price++) {
            $before_menu = $arr_cate_menu_name[$db_result_plan[$db_loop_price]["cate_menu_id"]];
            if($before_menu!=$after_menu) {
?>
<th rowspan="<? echo $arr_menu_count[$db_result_plan[$db_loop_price]["cate_menu_id"]];?>"><? echo $arr_cate_menu_name[$db_result_plan[$db_loop_price]["cate_menu_id"]]; ?></th>
<?
            }
            $after_menu = $arr_cate_menu_name[$db_result_plan[$db_loop_price]["cate_menu_id"]];

            $plan_price_id = $db_result_plan[$db_loop_price]["plan_price_id"];
            $plan_reserve_count = $arr_sum_reserve_count[$plan_price_id];
            $plan_price = $db_result_plan[$db_loop_price]["plan_price"];
?>
<th><? echo $db_result_plan[$db_loop_price]["plan_price_name"];?></th>
<td>¥<? echo number_format($plan_price);?></td>
<td><? if($plan_reserve_count!="") {echo $plan_reserve_count."人"; $total_plan_reserve_count+=$plan_reserve_count;}?></td>
<td><? if($plan_reserve_count!="") {echo "¥".number_format($plan_reserve_count*$plan_price); $total_set += $plan_reserve_count*$plan_price;}?></td>
<td><? if($arr_sum_reserve_option_price[$plan_price_id]!="") {echo "¥".number_format($arr_sum_reserve_option_price[$plan_price_id]); $total_option+=$arr_sum_reserve_option_price[$plan_price_id];}?></td>
<td><? if($arr_sum_reserve_price[$plan_price_id]!="") {echo "¥".number_format(($plan_reserve_count*$plan_price)+$arr_sum_reserve_option_price[$plan_price_id]); $total_price+=$arr_sum_reserve_price[$plan_price_id];}?></td>
</tr>
<?
        }
    }
?>

<tr class="sum">
<th colspan="3">合計</th>
<td></td>
<td><? echo number_format($total_plan_reserve_count);?>人</td>
<td>¥<? echo number_format($total_set);?></td>
<td>¥<? echo number_format($total_option);?></td>
<td>¥<? echo number_format($total_price);?></td>
</tr>

</tbody>
</table>
</section>
<?
}//foreach
?>

</article>


</body>
</html>
